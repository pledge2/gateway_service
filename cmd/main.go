package main

import (
	"context"
	"fmt"
	"gateway_service/internal/bootstrap"

	"gateway_service/api/docs"
	"gateway_service/internal/config"
	"gateway_service/internal/pkg/logger"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/sethvargo/go-envconfig"
	"os"
	"os/signal"
)

func main() {
	quitSignal := make(chan os.Signal, 1)
	signal.Notify(quitSignal, os.Interrupt)
	_ = godotenv.Load()

	var cfg config.Config
	if err := envconfig.ProcessWith(context.TODO(), &cfg, envconfig.OsLookuper()); err != nil {
		panic(fmt.Sprintf("envconfig.Process: %s", err))
	}

	//docs.SwaggerInfo.Host = cfg.ServerIP + cfg.HTTPPort
	//docs.SwaggerInfo.Schemes = []string{"http"}
	docs.SwaggerInfo.Description = "Gateway Service"
	docs.SwaggerInfo.Host = cfg.ServerHost
	docs.SwaggerInfo.BasePath = ""
	docs.SwaggerInfo.Schemes = []string{"https", "http"}

	log := logger.New(cfg.LogLevel, "Gateway Service")

	ctx, cancel := context.WithCancel(context.Background())

	app := bootstrap.New(cfg, log, ctx)

	go func() {
		OSCall := <-quitSignal
		log.Info(fmt.Sprintf("system call:%+v", OSCall))
		cancel()
	}()

	app.Run(ctx)

	log.Info("REST Server Gracefully Shut Down")
}
