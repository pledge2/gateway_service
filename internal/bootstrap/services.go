package bootstrap

import (
	"gateway_service/internal/config"
	"gateway_service/internal/drivers/service/analog_service/analog"
	"gateway_service/internal/drivers/service/analog_service/product"
	minio_integration "gateway_service/internal/drivers/service/archive_service/minio"
	nutanix_integration "gateway_service/internal/drivers/service/archive_service/nutanix"
	"gateway_service/internal/drivers/service/assessment_service/assessment"
	"gateway_service/internal/drivers/service/assessment_service/status"
	"gateway_service/internal/drivers/service/identification_service/app_roles"
	"gateway_service/internal/drivers/service/identification_service/authorization"
	"gateway_service/internal/drivers/service/identification_service/employees"
	"gateway_service/internal/drivers/service/identification_service/roles"
	"gateway_service/internal/drivers/service/identification_service/users"
	transport_discount "gateway_service/internal/drivers/service/reference_service/discount"
	transport_fuel "gateway_service/internal/drivers/service/reference_service/fuel"
	fuel_control "gateway_service/internal/drivers/service/reference_service/fuelControl"
	transport_model "gateway_service/internal/drivers/service/reference_service/model"
	transport_name "gateway_service/internal/drivers/service/reference_service/name"
	transport_position "gateway_service/internal/drivers/service/reference_service/position"
	transport_special "gateway_service/internal/drivers/service/reference_service/special"
	transport_type "gateway_service/internal/drivers/service/reference_service/type"
	transport_validity "gateway_service/internal/drivers/service/reference_service/validity"
	"gateway_service/internal/drivers/service/report"
	"gateway_service/internal/pkg/logger"
)

type clients struct {
	// Analog Service Integration
	analogI  analog.Client
	productI product.Client
	// Archive Service Integration
	minioI   minio_integration.Client
	nutanixI nutanix_integration.Client
	// Identification Service Integration
	authI    authorization.Client
	empI     employees.Client
	roleI    roles.Client
	appRoleI app_roles.Client
	usersI   users.Client
	// Assessment Service Integration
	assessmentI       assessment.Client
	assessmentStatusI status.Client
	// Reference Service Integration
	typeI        transport_type.Client
	modelI       transport_model.Client
	nameI        transport_name.Client
	positionI    transport_position.Client
	validityI    transport_validity.Client
	fuelI        transport_fuel.Client
	discountI    transport_discount.Client
	fuelControlI fuel_control.Client
	specialI     transport_special.Client
	// Report Service Integration
	reportI report.Client
}

func initClients(cfg config.Config, log logger.Logger) clients {
	// Analog Service Integration
	analogClient := analog.New(cfg, log)
	productClient := product.New(cfg, log)
	// Archive Service Integration
	minioClient := minio_integration.New(cfg, log)
	nutanixClient := nutanix_integration.New(cfg, log)
	// Identification Service Integration
	authorizationClient := authorization.New(cfg, log)
	employeesClient := employees.New(cfg, log)
	rolesClient := roles.New(cfg, log)
	appRolesClient := app_roles.New(cfg, log)
	usersClient := users.New(cfg, log)
	// Assessment Service Integration
	assessmentClient := assessment.New(cfg, log)
	assessmentStatusClient := status.New(cfg, log)
	// Reference Service Integration
	transportTypeClient := transport_type.New(cfg, log)
	transportModelClient := transport_model.New(cfg, log)
	transportNameClient := transport_name.New(cfg, log)
	transportPositionClient := transport_position.New(cfg, log)
	transportValidityClient := transport_validity.New(cfg, log)
	transportFuelClient := transport_fuel.New(cfg, log)
	transportDiscountClient := transport_discount.New(cfg, log)
	fuelControlClient := fuel_control.New(cfg, log)
	transportSpecialClient := transport_special.New(cfg, log)
	// Report Service Integration
	reportClient := report.New(cfg, log)
	return clients{
		// Analog Service Integration
		analogI:  *analogClient,
		productI: *productClient,
		// Archive Service Integration
		minioI:   *minioClient,
		nutanixI: *nutanixClient,
		// Identification Service Integration
		authI:    *authorizationClient,
		empI:     *employeesClient,
		roleI:    *rolesClient,
		appRoleI: *appRolesClient,
		usersI:   *usersClient,
		// Assessment Service Integration
		assessmentI:       *assessmentClient,
		assessmentStatusI: *assessmentStatusClient,
		// Reference Service Integration
		typeI:        *transportTypeClient,
		modelI:       *transportModelClient,
		nameI:        *transportNameClient,
		positionI:    *transportPositionClient,
		validityI:    *transportValidityClient,
		fuelI:        *transportFuelClient,
		discountI:    *transportDiscountClient,
		specialI:     *transportSpecialClient,
		fuelControlI: *fuelControlClient,
		// Report Service Integration
		reportI: *reportClient,
	}
}
