package bootstrap

import (
	"context"
	"fmt"
	"gateway_service/internal/config"
	"gateway_service/internal/pkg/cors"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/rest"
	"net/http"

	"time"

	"github.com/gin-gonic/gin"
)

const gracefulDeadline = 5 * time.Second

type App struct {
	http     *http.Server
	cfg      config.Config
	log      logger.Logger
	ctx      context.Context
	teardown []func()
}

func New(cfg config.Config, log logger.Logger, ctx context.Context) *App {
	teardown := make([]func(), 0)

	app := App{
		cfg:      cfg,
		log:      log,
		teardown: teardown,
		ctx:      ctx,
	}

	app.initConnections()

	client := initClients(app.cfg, app.log)
	useCase := initUseCases(app.cfg, client, app.log)

	router := gin.Default()
	router.Use(cors.CORSMiddleware())

	server := rest.New(router,
		app.cfg,
		app.log,
		&useCase.analogUC,
		&useCase.productUC,
		&useCase.minioUC,
		&useCase.nutanixUC,
		&useCase.assessmentUC,
		&useCase.assessmentStatusUC,
		&useCase.appRolesUC,
		&useCase.authorizationUC,
		&useCase.employeesUC,
		&useCase.roleUC,
		&useCase.usersUC,
		&useCase.jwtUC,
		&useCase.typeUC,
		&useCase.modelUC,
		&useCase.nameUC,
		&useCase.positionUC,
		&useCase.fuelUC,
		&useCase.validityUC,
		&useCase.discountUC,
		&useCase.specialUC,
		&useCase.referenceUC,
		&useCase.reportUC,
		&useCase.fuelControlUC,
	)

	app.http = &http.Server{
		Addr:        cfg.HTTPPort,
		Handler:     server,
		ReadTimeout: 10 * time.Second,
	}

	return &app
}

func (app *App) initConnections() {

	app.teardown = append(app.teardown, func() {
		app.log.Info("HTTP is shutting down")
		ctxShutDown, cancel := context.WithTimeout(app.ctx, gracefulDeadline)
		defer cancel()
		if err := app.http.Shutdown(ctxShutDown); err != nil {
			app.log.Error(fmt.Sprintf("server Shutdown Failed:%s", err))
			if err == http.ErrServerClosed {
				err = nil //nolint:ineffassign
			}
			return
		}

		app.log.Info("HTTP is shut down")
	})

}

func (app *App) Run(ctx context.Context) {

	go func() {

		app.log.Info("REST Server started at port " + app.cfg.HTTPPort)
		if err := app.http.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			app.log.Fatal(fmt.Sprintf("Failed To Run REST Server: %s\n", err.Error()))
		}
	}()

	<-ctx.Done()
	for i := range app.teardown {
		app.teardown[i]()
	}
}
