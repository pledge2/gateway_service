package bootstrap

import (
	"gateway_service/internal/config"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/usecase/analog_service/analog"
	"gateway_service/internal/usecase/analog_service/product"
	minio_usecase "gateway_service/internal/usecase/archive_service/minio"
	nutanix_usecase "gateway_service/internal/usecase/archive_service/nutanix"
	"gateway_service/internal/usecase/assessment_service/assessment"
	"gateway_service/internal/usecase/assessment_service/assessment_status"
	"gateway_service/internal/usecase/identification_service/app_roles"
	"gateway_service/internal/usecase/identification_service/authorization"
	"gateway_service/internal/usecase/identification_service/employees"
	"gateway_service/internal/usecase/identification_service/roles"
	"gateway_service/internal/usecase/identification_service/users"
	"gateway_service/internal/usecase/jwt"
	reference_usecase "gateway_service/internal/usecase/reference_service"
	"gateway_service/internal/usecase/reference_service/discount"
	"gateway_service/internal/usecase/reference_service/fuel"
	fuel_control "gateway_service/internal/usecase/reference_service/fuelControl"
	"gateway_service/internal/usecase/reference_service/model"
	"gateway_service/internal/usecase/reference_service/name"
	"gateway_service/internal/usecase/reference_service/position"
	"gateway_service/internal/usecase/reference_service/special"
	type_usecase "gateway_service/internal/usecase/reference_service/type"
	"gateway_service/internal/usecase/reference_service/validity"
	"gateway_service/internal/usecase/report"
)

type useCases struct {
	// Analog Service UseCase
	analogUC  analog.UseCase
	productUC product.UseCase
	// Archive Service UseCase
	minioUC   minio_usecase.UseCase
	nutanixUC nutanix_usecase.UseCase
	// Assessment Service UseCase
	assessmentUC       assessment.UseCase
	assessmentStatusUC assessment_status.UseCase
	// Identification Service UseCase
	appRolesUC      app_roles.UseCase
	authorizationUC authorization.UseCase
	employeesUC     employees.UseCase
	roleUC          roles.UseCase
	usersUC         users.UseCase
	// JWT
	jwtUC jwt.TokenUseCase
	// Reference Service UseCase
	typeUC        type_usecase.UseCase
	modelUC       model.UseCase
	nameUC        name.UseCase
	positionUC    position.UseCase
	fuelUC        fuel.UseCase
	validityUC    validity.UseCase
	discountUC    discount.UseCase
	specialUC     special.UseCase
	fuelControlUC fuel_control.UseCase

	referenceUC reference_usecase.UseCase
	// Report Service UseCase
	reportUC report.UseCase
}

func initUseCases(cfg config.Config, client clients, log logger.Logger) useCases {
	return useCases{
		// Analog Service UseCase
		analogUC:  *analog.New(&client.analogI, &client.usersI, log),
		productUC: *product.New(&client.productI, log),
		// Archive Service UseCase
		minioUC:   *minio_usecase.New(&client.minioI, log),
		nutanixUC: *nutanix_usecase.New(&client.nutanixI, log),
		// Assessment Service UseCase
		assessmentUC:       *assessment.New(&client.assessmentI, &client.usersI, log),
		assessmentStatusUC: *assessment_status.New(&client.assessmentStatusI, log),
		appRolesUC:         *app_roles.New(&client.appRoleI, log),
		authorizationUC:    *authorization.New(&client.authI, log),
		employeesUC:        *employees.New(&client.empI, log),
		roleUC:             *roles.New(&client.roleI, log),
		usersUC:            *users.New(&client.usersI, log),
		// JWT
		jwtUC: *jwt.New(cfg.AccessTokenExpireTime, cfg.RefreshTokenExpireTime, cfg.TokenSecretKey, &client.usersI, &client.roleI, log),
		// Reference Service UseCase
		typeUC:        *type_usecase.New(&client.typeI, log),
		modelUC:       *model.New(&client.modelI, log),
		nameUC:        *name.New(&client.nameI, log),
		positionUC:    *position.New(&client.positionI, log),
		fuelUC:        *fuel.New(&client.fuelI, log),
		validityUC:    *validity.New(&client.validityI, log),
		discountUC:    *discount.New(&client.discountI, log),
		specialUC:     *special.New(&client.specialI, log),
		fuelControlUC: *fuel_control.New(&client.fuelControlI, log),
		referenceUC: *reference_usecase.New(&client.modelI, &client.nameI, &client.positionI,
			&client.fuelI, &client.validityI, log),
		// Report Service UseCase
		reportUC: *report.New(&client.reportI, log),
	}
}
