package config

import "time"

type Config struct {
	Environment           string `env:"ENVIRONMENT"`
	LogLevel              string `env:"LOG_LEVEL"`
	ServerIP              string `env:"SERVER_IP"`
	HTTPPort              string `env:"HTTP_PORT"`
	ServiceName           string `env:"SERVICE_NAME"`
	IdentificationService string `env:"IDENTIFICATION_SERVICE"`
	ReferenceService      string `env:"REFERENCE_SERVICE"`
	AssessmentService     string `env:"ASSESSMENT_SERVICE"`
	AnalogService         string `env:"ANALOG_SERVICE"`
	ArchiveService        string `env:"ARCHIVE_SERVICE"`
	ReportService         string `env:"REPORT_SERVICE"`
	ServerHost            string `env:"SERVER_HOST"`

	AccessTokenExpireTime  time.Duration `env:"ACCESS_TOKEN_EXPIRE_TIME"`
	RefreshTokenExpireTime time.Duration `env:"REFRESH_TOKEN_EXPIRE_TIME"`
	TokenSecretKey         string        `env:"TOKEN_SECRET_KEY"`
}
