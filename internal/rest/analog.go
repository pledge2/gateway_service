package rest

import (
	"fmt"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"
	"strconv"

	"github.com/gin-gonic/gin"
)

type analogStore struct {
	PID            string        `json:"p_id"`
	UID            string        `json:"u_id"`
	Type           string        `json:"type"`
	Model          string        `json:"model"`
	Name           string        `json:"name"`
	Position       string        `json:"position"`
	Speedometer    int           `json:"speedometer"`
	Year           int           `json:"year"`
	Fuel           string        `json:"fuel"`
	Price          int           `json:"price"`
	Validity       int           `json:"validity"`
	SellPrice      int           `json:"sell_price"`
	AllPhotosName  allPhotosName `json:"photos"`
	SourceName     string        `json:"source_name"`
	Comment        string        `json:"comment"`
	SourceFileName string        `json:"source_file_name"`
	Active         bool          `json:"active"`
	NameKey        string        `json:"name_key"`
}

type allPhotosName struct {
	PhotosName []photosName `json:"photo"`
}

type photoName struct {
	PhotoName string `json:"photo_name"`
}

type photosName struct {
	photoName
	source
}
type source struct {
	LAT   string `json:"lat"`
	LON   string `json:"lon"`
	Title string `json:"title"`
	Order int    `json:"order"`
}

type idResponse struct {
	ID string `json:"id"`
}

// analogStore godoc
// @Router /analog/store [POST]
// @Summary Create a new Analog
// @Description API Create Analog
// @Tags Admin Page Analog
// @Produce json
// @Param token header string true "token"
// @Param request body analogStore true "Analog details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) analogStore() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   analogStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return

		}

		userID, isExist := c.Get("user_id")
		if !isExist {
			Return(c, result, errs.ErrUnauthorized)
			return
		}

		if !utils.IsTrailer(data.NameKey) {
			if data.Fuel == "" && data.Speedometer == 0 {
				Return(c, result, errs.ErrValidation)
				return
			}
		} else {
			data.Fuel = ""
			data.Speedometer = 0
		}

		data.UID = fmt.Sprint(userID)

		req := domain.Analog{
			PID:            data.PID,
			UID:            data.UID,
			Type:           data.Type,
			Model:          data.Model,
			Name:           data.Name,
			Position:       data.Position,
			Speedometer:    data.Speedometer,
			Year:           data.Year,
			Fuel:           data.Fuel,
			Price:          data.Price,
			Validity:       data.Validity,
			SellPrice:      data.SellPrice,
			SourceName:     data.SourceName,
			Comment:        data.Comment,
			SourceFileName: data.SourceFileName,
			Active:         data.Active,
			NameKey:        data.NameKey,
		}

		for _, val := range data.AllPhotosName.PhotosName {
			domainSource := domain.PhotosName{
				Source: domain.Source{
					Title: val.source.Title,
					Order: val.source.Order,
					LAT:   val.source.LAT,
					LON:   val.source.LON,
				},
				PhotoName: domain.PhotoName{
					PhotoName: val.photoName.PhotoName,
				},
			}
			req.Photos.PhotosName = append(req.Photos.PhotosName, domainSource)
		}

		id, err := s.analogUC.StoreAnalog(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

type analog struct {
	ID            string       `json:"id"`
	Type          string       `json:"type"`
	Model         string       `json:"model"`
	Name          string       `json:"name"`
	Position      string       `json:"position"`
	Speedometer   int          `json:"speedometer"`
	Year          int          `json:"year"`
	Fuel          string       `json:"fuel"`
	SellPrice     int          `json:"sell_price"`
	Price         int          `json:"price"`
	Validity      int          `json:"validity"`
	Active        bool         `json:"active"`
	Comment       string       `json:"comment"`
	SourceFileUrl string       `json:"source_file_url"`
	SourceFile    string       `json:"source_file"`
	AllPhotosUrl  AllPhotosUrl `json:"photos"`
	Employee      string       `json:"employee"`
	CreatedAt     string       `json:"created_at"`
	UpdatedAt     string       `json:"updated_at"`
}

type AllPhotosUrl struct {
	PhotosUrl []photosUrl `json:"photo"`
}

type photosUrl struct {
	photoUrl
	source
}
type photoUrl struct {
	PhotoUrl string `json:"photo_url"`
}

// analogFindOne godoc
// @Router /analog/find/one [GET]
// @Summary Find Analog
// @Description API Find Analog
// @Tags Admin Page Analog
// @Produce json
// @Param token header string true "token"
// @Param id query string true "Analog -> id"
// @Success 201 {object} Response{data=analog}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) analogFindOne() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result analog

		ID := c.Query("id")

		if ID == "" {
			Return(c, result, errs.ErrAnalogIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrAnalogIDFormat)
			return
		}

		data, err := s.analogUC.AnalogFindOne(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = analog{
			ID:            data.ID,
			Type:          data.Type,
			Model:         data.Model,
			Name:          data.Name,
			Position:      data.Position,
			Speedometer:   data.Speedometer,
			Year:          data.Year,
			Fuel:          data.Fuel,
			SellPrice:     data.SellPrice,
			Price:         data.Price,
			Validity:      data.Validity,
			Active:        data.Active,
			Comment:       data.Comment,
			SourceFileUrl: data.SourceFileUrl,
			SourceFile:    data.SourceFile,
			Employee:      data.Employee,
			CreatedAt:     data.CreatedAt,
			UpdatedAt:     data.CreatedAt,
		}

		for _, val := range data.AllPhotosUrl.PhotosUrl {
			result.AllPhotosUrl.PhotosUrl = append(result.AllPhotosUrl.PhotosUrl, photosUrl{
				photoUrl: photoUrl{
					PhotoUrl: val.PhotoUrl.PhotoUrl,
				},
				source: source{
					Title: val.Source.Title,
					Order: val.Source.Order,
					LAT:   val.Source.LAT,
					LON:   val.Source.LAT,
				},
			})
		}
		Return(c, result, nil)
	}
}

type analogView struct {
	ID            string       `json:"id"`
	Type          string       `json:"type"`
	Model         string       `json:"model"`
	Name          string       `json:"name"`
	Position      string       `json:"position"`
	Speedometer   int          `json:"speedometer"`
	Year          int          `json:"year"`
	Fuel          string       `json:"fuel"`
	SellPrice     int          `json:"sell_price"`
	Price         int          `json:"price"`
	Validity      string       `json:"validity"`
	Active        bool         `json:"active"`
	Comment       string       `json:"comment"`
	SourceFileUrl string       `json:"source_file_url"`
	SourceFile    string       `json:"source_file"`
	AllPhotosUrl  AllPhotosUrl `json:"photos"`
	Employee      string       `json:"employee"`
	CreatedAt     string       `json:"created_at"`
	UpdatedAt     string       `json:"updated_at"`
}

// analogFindView godoc
// @Router /analog/find/view [GET]
// @Summary Find Analog
// @Description API Find Analog
// @Tags Admin Page Analog
// @Produce json
// @Param token header string true "token"
// @Param id query string true "Analog -> id"
// @Success 201 {object} Response{data=analogView}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) analogFindView() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result analogView

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrAnalogIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrAnalogIDFormat)
			return
		}

		data, err := s.analogUC.AnalogFindView(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = analogView{
			ID:            data.ID,
			Type:          data.Type,
			Model:         data.Model,
			Name:          data.Name,
			Position:      data.Position,
			Speedometer:   data.Speedometer,
			Year:          data.Year,
			Fuel:          data.Fuel,
			SellPrice:     data.SellPrice,
			Price:         data.Price,
			Validity:      data.Condition,
			Active:        data.Active,
			Comment:       data.Comment,
			SourceFileUrl: data.SourceFileUrl,
			SourceFile:    data.SourceFile,
			Employee:      data.Employee,
			CreatedAt:     data.CreatedAt,
			UpdatedAt:     data.CreatedAt,
		}

		for _, val := range data.AllPhotosUrl.PhotosUrl {
			result.AllPhotosUrl.PhotosUrl = append(result.AllPhotosUrl.PhotosUrl, photosUrl{
				photoUrl: photoUrl{
					PhotoUrl: val.PhotoUrl.PhotoUrl,
				},
				source: source{
					Title: val.Source.Title,
					Order: val.Source.Order,
					LAT:   val.Source.LAT,
					LON:   val.Source.LAT,
				},
			})
		}

		Return(c, result, nil)
	}
}

type analogListResponse struct {
	TotalPages int          `json:"total_pages"`
	AnalogList []analogList `json:"analog_list"`
}

type analogList struct {
	Index       int    `json:"index"`
	ID          string `json:"id"`
	Model       string `json:"model"`
	Name        string `json:"name"`
	Position    string `json:"position"`
	Speedometer int    `json:"speedometer"`
	Year        int    `json:"year"`
	Fuel        string `json:"fuel"`
	SellPrice   int    `json:"sell_price"`
	Active      bool   `json:"active"`
	CreatedAt   string `json:"created_at"`
}

// findList godoc
// @Router /analog/find/list [GET]
// @Summary Find all Analogs
// @Description API to Find all Analogs
// @Tags Admin Page Analog
// @Produce json
// @Param token header string true "token"
// @Param product_id query string true "Product id"
// @Param type_id query string true "Type id"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Success 201 {object} Response{data=analogListResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findList() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result analogListResponse

		productID := c.Query("product_id")
		if productID == "" {
			Return(c, result, errs.ErrProductIDEmpty)
			return
		}

		if !utils.IsValidUUID(productID) {
			Return(c, result, errs.ErrProductIDEmpty)
			return
		}

		typeID := c.Query("type_id")
		if typeID == "" {
			Return(c, result, errs.ErrTypeIDEmpty)
			return
		}

		if !utils.IsValidUUID(typeID) {
			Return(c, result, errs.ErrTypeIDFormat)
			return
		}

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		if intPage < 1 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		data, err := s.analogUC.AnalogFindList(c, productID, typeID, intLimit, intPage)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = analogListResponse{
			TotalPages: data.TotalPages,
		}

		for _, list := range data.AnalogList {
			analog := analogList{
				Index:       list.Index,
				ID:          list.ID,
				Model:       list.Model,
				Name:        list.Name,
				Position:    list.Position,
				Speedometer: list.Speedometer,
				Year:        list.Year,
				Fuel:        list.Fuel,
				SellPrice:   list.SellPrice,
				Active:      list.Active,
				CreatedAt:   list.CreatedAt,
			}
			result.AnalogList = append(result.AnalogList, analog)
		}

		Return(c, result, nil)
	}
}

type analogStatus struct {
	ID     string `json:"id" binding:"required"`
	Active bool   `json:"active"`
}

type analogUpdateStatus struct {
	Analog []analogStatus `json:"data"`
}

// analogAdminData godoc
// @Router /analog/update/status [PUT]
// @Summary Update Analog Status
// @Description API to Update Analog Status
// @Tags Admin Page Analog
// @Accept json
// @Produce json
// @Param token header string true "Token"
// @Param req body analogUpdateStatus true "Analog Status"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data    analogUpdateStatus
			request domain.UpdateAnalogStatus
			result  Result
		)

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		for _, val := range data.Analog {
			info := domain.Analog{
				ID:     val.ID,
				Active: val.Active,
			}
			request.Analog = append(request.Analog, info)
		}

		response, err := s.analogUC.AnalogUpdateStatus(c, request)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = Result{
			Result: response.Result,
		}

		Return(c, result, nil)
	}
}

type analogUpdate struct {
	UID            string        `json:"u_id"`
	PID            string        `json:"p_id"`
	Type           string        `json:"type"`
	Model          string        `json:"model"`
	Name           string        `json:"name"`
	Position       string        `json:"position"`
	Speedometer    int           `json:"speedometer"`
	Year           int           `json:"year"`
	Fuel           string        `json:"fuel"`
	Price          int           `json:"price"`
	Validity       int           `json:"validity"`
	SellPrice      int           `json:"sell_price"`
	AllPhotosName  allPhotosName `json:"photos"`
	SourceName     string        `json:"source_name"`
	Comment        string        `json:"comment"`
	SourceFileName string        `json:"source_file_name"`
	Active         bool          `json:"active"`
}

// analogUpdate godoc
// @Router /analog/update [PUT]
// @Summary Update Analog
// @Description API to Update Analog
// @Tags Admin Page Analog
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body analogUpdate true "Analog url"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) analogUpdate() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data analogUpdate
			//arrSource domain.AllPhotosName
			result Result
		)

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrAnalogIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrAnalogIDEmpty)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		user_id, isExist := c.Get("user_id")
		if !isExist {
			Return(c, result, errs.ErrUnauthorized)
			return
		}

		data.UID = fmt.Sprint(user_id)

		req := domain.Analog{
			PID:            data.PID,
			UID:            data.UID,
			Type:           data.Type,
			Model:          data.Model,
			Name:           data.Name,
			Position:       data.Position,
			Speedometer:    data.Speedometer,
			Year:           data.Year,
			Fuel:           data.Fuel,
			Price:          data.Price,
			Validity:       data.Validity,
			SellPrice:      data.SellPrice,
			SourceName:     data.SourceName,
			Comment:        data.Comment,
			SourceFileName: data.SourceFileName,
			Active:         data.Active,
		}

		for _, val := range data.AllPhotosName.PhotosName {
			domainSource := domain.PhotosName{
				Source: domain.Source{
					Title: val.source.Title,
					Order: val.source.Order,
					LAT:   val.source.LAT,
					LON:   val.source.LON,
				},
				PhotoName: domain.PhotoName{
					PhotoName: val.photoName.PhotoName,
				},
			}
			req.Photos.PhotosName = append(req.Photos.PhotosName, domainSource)
		}

		resp, err := s.analogUC.AnalogUpdate(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// analogDelete godoc
// @Router /analog/delete [DELETE]
// @Summary Delete  Analog
// @Description API Delete  Analog
// @Tags Admin Page Analog
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) analogDelete() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrAnalogIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrAnalogIDFormat)
			return
		}

		resp, err := s.analogUC.AnalogDelete(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// searchAnalog godoc
// @Router /analog/search [GET]
// @Summary Search Analog
// @Description API Search Analog
// @Tags Admin Page Analog
// @Produce json
// @Param token header string true "token"
// @Param id query string false "id"
// @Param type query string false "type"
// @Param model query string false "model"
// @Param name query string false "name"
// @Param position query string false "position"
// @Param fromYear query string false "fromYear"
// @Param toYear query string false "toYear"
// @Param fuel query string false "fuel"
// @Param fromSpeedometer query string false "fromSpeedometer"
// @Param toSpeedometer query string false "toSpeedometer"
// @Param validity query string false "validity"
// @Param fromPrice query string false "fromPrice"
// @Param toPrice query string false "toPrice"
// @Param active query string false "status"
// @Param fromDate query string false "fromDate"
// @Param toDate query string false "toDate"
// @Param user query string false "user"
// @Param limit query string true "limit"
// @Param page query string true "page"
// @Success 201 {object} Response{data=analogListResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) searchAnalog() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result analogListResponse

		analogID := c.Query("id")
		typeID := c.Query("type")
		modelID := c.Query("model")
		nameID := c.Query("name")
		positionID := c.Query("position")
		fromYear := c.Query("fromYear")
		toYear := c.Query("toYear")
		fuel := c.Query("fuel")
		fromSpeedometer := c.Query("fromSpeedometer")
		toSpeedometer := c.Query("toSpeedometer")
		validity := c.Query("validity")
		fromPrice := c.Query("fromPrice")
		toPrice := c.Query("toPrice")
		active := c.Query("active")
		fromDate := c.Query("fromDate")
		toDate := c.Query("toDate")
		user := c.Query("user")

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		data, err := s.analogUC.AnalogSearch(c, analogID, typeID, modelID, nameID, positionID, fromYear, toYear, fuel, fromSpeedometer, toSpeedometer, validity, fromPrice, toPrice, active, fromDate, toDate, user, limit, page)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = analogListResponse{
			TotalPages: data.TotalPages,
		}

		for _, list := range data.AnalogList {
			analog := analogList{
				Index:       list.Index,
				ID:          list.ID,
				Model:       list.Model,
				Name:        list.Name,
				Position:    list.Position,
				Speedometer: list.Speedometer,
				Year:        list.Year,
				Fuel:        list.Fuel,
				SellPrice:   list.SellPrice,
				Active:      list.Active,
				CreatedAt:   list.CreatedAt,
			}
			result.AnalogList = append(result.AnalogList, analog)
		}

		Return(c, result, nil)
	}
}
