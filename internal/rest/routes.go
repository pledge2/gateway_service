package rest

import (
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func (s *Server) Router() {
	// Identification Service
	identification := s.router.Group("/")
	identification.POST("sign/in", s.signIn())
	identification.POST("sign/up", s.signUp())
	identification.POST("check/code", s.checkCode())
	identification.POST("resend/code", s.reSendCode())
	identification.POST("check/cbid", s.checkCbid())
	identification.POST("change/password", s.changePassword())

	// User Role
	role := s.router.Group("/role", s.middleware())
	role.POST("/store", s.createRole())
	role.GET("/find/one", s.roleFindOne())
	role.GET("/find/all", s.roleFindAll())
	role.PUT("/update", s.updateRole())
	role.PUT("/update/status", s.updateRoleStatus())
	role.DELETE("/delete", s.deleteRole())

	//User List
	user := s.router.Group("/user")
	user.GET("/find/list", s.userFindList())
	user.GET("/info", s.middleware(), s.getUserInfo())
	user.PUT("/update/email", s.middleware(), s.updateUserEmail())
	user.DELETE("/delete", s.middleware(), s.deleteUserData())
	user.DELETE("/block", s.middleware(), s.blockUser())

	s.router.GET("/users/list", s.middleware(), s.allUsersList())

	employee := s.router.Group("/emp")
	employee.GET("/find/one", s.middleware(), s.findEmployee())
	employee.GET("/find/all", s.middleware(), s.findAllEmployees())
	employee.PUT("/update", s.middleware(), s.updateEmpData())

	//Bank Employee
	adminRoles := s.router.Group("")
	adminRoles.GET("/get/positions/postcode", s.middleware(), s.getRoleByPositionCode())
	adminRoles.GET("/search/positions/postcode", s.middleware(), s.searchRolesByPostCode())
	adminRoles.GET("/get/positions/cbid", s.middleware(), s.getRoleByCBID())
	adminRoles.GET("/search/positions/cbid", s.middleware(), s.searchRolesByCbID())
	adminRoles.POST("/add/position/postcode", s.middleware(), s.addPositionPostCode())
	adminRoles.DELETE("/delete/position/postcode", s.middleware(), s.deletePositionPostCode())
	adminRoles.POST("/add/position/cbid", s.middleware(), s.addPositionCBID())
	adminRoles.DELETE("/delete/position/cbid", s.middleware(), s.deletePositionCBID())

	// Reference Service
	// Type
	typee := s.router.Group("/type", s.middleware())
	typee.POST("/store", s.typeStore())
	typee.GET("/find/one", s.typeFindOne())
	typee.GET("/find/all", s.findAllType())
	typee.GET("/pagination", s.typeFindPagination())
	typee.PUT("/update", s.typeUpdate())
	typee.PUT("/update/photo", s.updateTypeUrl())
	typee.PUT("/update/status", s.updateTypeStatus())
	typee.DELETE("/delete", s.typeDelete())

	// Model
	model := s.router.Group("/model", s.middleware())
	model.POST("/store", s.modelStore())
	model.GET("/find/one", s.modelFindOne())
	model.GET("/find/all", s.findAllModel())
	model.GET("/pagination", s.modelFindPagination())
	model.GET("/find/all/type", s.findAllModelByTypeID())
	model.GET("/filter", s.modelFilter())
	model.PUT("/update", s.modelUpdate())
	model.PUT("/update/photo", s.updateModelUrl())
	model.PUT("/update/status", s.updateModelStatus())
	model.DELETE("/delete", s.modelDelete())

	// Name
	name := s.router.Group("/name", s.middleware())
	name.POST("/store", s.nameStore())
	name.GET("/find/one", s.nameFindOne())
	name.GET("/find/all", s.findAllName())
	name.GET("/pagination", s.nameFindPagination())
	name.GET("/find/all/model", s.findAllNameByModelID())
	name.GET("/filter", s.nameFilter())
	name.PUT("/update", s.nameUpdate())
	name.PUT("/update/photo", s.updateNameUrl())
	name.PUT("/update/status", s.updateNameStatus())
	name.DELETE("/delete", s.nameDelete())

	// Position
	position := s.router.Group("/position", s.middleware())
	position.POST("/store", s.positionStore())
	position.GET("/find/one", s.positionFindOne())
	position.GET("/find/all", s.findAllPosition())
	position.GET("/pagination", s.positionFindPagination())
	position.GET("/find/all/name", s.findAllPositionByNameID())
	position.GET("/filter", s.positionFilter())
	position.PUT("/update", s.positionUpdate())
	position.PUT("/update/status", s.updatePositionStatus())
	position.DELETE("/delete", s.positionDelete())

	// Fuel
	fuel := s.router.Group("/fuel", s.middleware())
	fuel.POST("/store", s.fuelStore())
	fuel.GET("/find/one", s.fuelFindOne())
	fuel.GET("/find/all", s.findAllFuel())
	fuel.PUT("/update", s.fuelUpdate())
	fuel.PUT("/update/status", s.updateFuelStatus())
	fuel.DELETE("/delete", s.fuelDelete())

	// Validity
	validity := s.router.Group("/validity", s.middleware())
	validity.POST("/store", s.validityStore())
	validity.GET("/find/one", s.validityFindOne())
	validity.GET("/find/all", s.findAllValidity())
	validity.PUT("/update", s.validityUpdate())
	validity.PUT("/update/status", s.updateValidityStatus())
	validity.DELETE("/delete", s.validityDelete())

	// Discounts
	discount := s.router.Group("/discount", s.middleware())
	discount.GET("/all", s.getDiscounts())
	discount.PUT("/update", s.updateDiscount())
	discount.POST("/store", s.storeDiscount())
	discount.DELETE("/delete", s.deleteDiscount())

	// FuelControls
	fuelControl := s.router.Group("/fuel/control", s.middleware())
	fuelControl.GET("/all", s.getFuels())
	fuelControl.PUT("/update", s.updateFuelControl())
	fuelControl.POST("/store", s.storeFuelControl())
	fuelControl.DELETE("/delete", s.deleteFuelControl())

	special := s.router.Group("special", s.middleware())
	special.POST("/store", s.storeSpecial())
	special.GET("/find/all", s.findSpecials())
	special.PUT("/update", s.updateSpecial())
	special.DELETE("/delete", s.deleteSpecial())

	// Reference All Methods
	s.router.GET("/reference/all", s.middleware(), s.getAllMethods())

	// Minio
	minio := s.router.Group("/minio", s.middleware())
	minio.POST("/upload/file", s.uploadFile())
	minio.POST("/upload/photo", s.uploadPhoto())

	// Nutanix
	nutanix := s.router.Group("nutanix", s.middleware())
	nutanix.POST("/upload/file", s.uploadFileToNutanix())
	nutanix.POST("/upload/photo", s.uploadPhotoToNutanix())

	// Assessment Service
	// Fast
	assessment := s.router.Group("/assessment", s.middleware())
	assessment.POST("/fast", s.fastAssessment())
	assessment.POST("/official", s.officialAssessment())
	// assessment.GET("/find/all", s.findAllAssessment())
	assessment.GET("/find/one", s.findAssessment())
	assessment.GET("/find/status", s.findAssessmentByStatus())
	assessment.GET("/count/status", s.countStatusAssessment())
	assessment.POST("/change/status", s.changeAssessmentByStatus())
	assessment.DELETE("/delete", s.deleteAssessment())

	assessment.GET("/analogs/list", s.analogsList())

	// Outsource Integration
	s.router.GET("/assessment/data", s.getAssessmentData())

	s.router.POST("/status/store", s.middleware(), s.statusStore())
	s.router.GET("/status/find/all", s.middleware(), s.findAllStatus())

	// Official
	// assessment.PUT("/store/gov", s.storeGovInfo())
	assessment.POST("/store/main", s.storeMainInfo())
	assessment.GET("/get/photos", s.getPhotos())

	// GovInfo
	gov := s.router.Group("/gov", s.middleware())
	gov.POST("/vehicle/carinfo", s.govCarInfo())
	gov.PUT("/update", s.updateGovData())

	// Analog Service
	analog := s.router.Group("/analog", s.middleware())
	analog.GET("/search", s.searchAnalog())
	analog.POST("/store", s.analogStore())
	analog.GET("/find/one", s.analogFindOne())
	analog.GET("/find/view", s.analogFindView())
	analog.GET("/find/list", s.findList())
	analog.PUT("/update", s.analogUpdate())
	analog.PUT("/update/status", s.updateStatus())
	analog.DELETE("/delete", s.analogDelete())

	// Product
	product := s.router.Group("/product", s.middleware())
	product.POST("/store", s.productStore())
	product.GET("/find/one", s.productFindOne())
	product.GET("/find/all", s.findAllProduct())
	product.PUT("/update/photo", s.updateProductPhotoUrl())
	product.PUT("/update/status", s.updateProductStatus())

	s.router.POST("/generate/report", s.assessmentReport())
	s.router.GET("/iabs/get/all", s.getEmployeeDataIABS())

	s.router.POST("/refresh", s.refresh())
	s.router.POST("/extract/claims", s.extractClaims())
	s.router.POST("/generate/token", s.generateToken())

	// Nutanix Change folder methods
	s.router.POST("/type/change/folder", s.copyTypePhotos())
	s.router.POST("/model/change/folder", s.copyModelPhotos())
	s.router.POST("/name/change/folder", s.copyNamePhotos())
	s.router.POST("/analog/change/photos/folder", s.copyAnalogPhotos())
	s.router.POST("/analog/change/pdf/folder", s.copyAnalogPdf())
	s.router.POST("/assessment/change/photo/folder", s.moveAssessmentPhotos())
	s.router.POST("/product/change/photos/folder", s.moveProductPhotos())

	s.router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
