package rest

import (
	"gateway_service/internal/errs"

	"github.com/gin-gonic/gin"
)

type tokenRequest struct {
	Token string `json:"token" example:"valid token"`
}

type tokenResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

// refresh godoc
// @Router /refresh [POST]
// @Summary Activate Access Token By Refresh
// @Description Activate Access Token
// @Tags Both Device
// @Produce json
// @Param request body tokenRequest true "Refresh Token"
// @Success 201 {object} Response{data=tokenResponse}
// @Failure 400 {object} Response
// @Failure 500 {object} Response
func (s *Server) refresh() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   tokenRequest
			result tokenResponse
		)
		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, nil, errs.ErrValidation)
			return
		}

		info, err := s.jwtUC.ExtractClaims(data.Token)
		if err != nil {
			Return(c, result, errs.ErrRefreshTokenExpired)
			return
		}

		access, refresh, err := s.jwtUC.GenerateToken(info)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = tokenResponse{
			AccessToken:  access,
			RefreshToken: refresh,
		}

		Return(c, result, nil)
	}
}

// extractClaims godoc
// @Router /extract/claims [POST]
// @Summary Extract Claims
// @Description Extract Claims
// @Tags Backend
// @Produce json
// @Param request body tokenRequest true "Token"
// @Success 201 {object} Response{data}
// @Failure 400 {object} Response
// @Failure 500 {object} Response
func (s *Server) extractClaims() gin.HandlerFunc {
	return func(c *gin.Context) {
		var data tokenRequest
		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, nil, errs.ErrValidation)
			return
		}

		result, err := s.jwtUC.ExtractClaims(data.Token)
		if err != nil {
			Return(c, result, err)
			return
		}

		Return(c, result, nil)
	}
}

type generateToken struct {
	Claims map[string]interface{} `json:"claims"`
}

// generateToken godoc
// @Router /generate/token [POST]
// @Summary Extract Claims
// @Description Extract Claims
// @Tags Backend
// @Produce json
// @Param request body generateToken true "Token"
// @Success 201 {object} Response{data}
// @Failure 400 {object} Response
// @Failure 500 {object} Response
func (s *Server) generateToken() gin.HandlerFunc {
	return func(c *gin.Context) {
		var data generateToken
		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, nil, errs.ErrValidation)
			return
		}

		claims := data.Claims

		access, refresh, err := s.jwtUC.GenerateToken(claims)
		if err != nil {
			Return(c, nil, err)
			return
		}

		result := tokenResponse{
			AccessToken:  access,
			RefreshToken: refresh,
		}

		Return(c, result, nil)
	}
}
