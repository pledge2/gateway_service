package rest

import "github.com/gin-gonic/gin"

// copyTypePhotos godoc swagger
// @Router /type/change/folder [POST]
// @Summary  Change Transport Type photos folder in Nutanix
// @Description Change Transport Type photos folder in Nutanix
// @Tags Nutanix Temp
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data=Result}
// @Failure 500 {object} Response{data=Result}
func (s *Server) copyTypePhotos() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result Result
		)

		resp, err := s.typeUC.ChangeTypeFolder(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = Result{
			Result: resp.Result,
		}

		Return(c, result, nil)
	}
}

// copyModelPhotos godoc swagger
// @Router /model/change/folder [POST]
// @Summary  Change Transport Model photos folder in Nutanix
// @Description Change Transport Model photos folder in Nutanix
// @Tags Nutanix Temp
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data=Result}
// @Failure 500 {object} Response{data=Result}
func (s *Server) copyModelPhotos() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result Result
		)

		resp, err := s.modelUC.ChangeModelFolder(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = Result{
			Result: resp.Result,
		}

		Return(c, result, nil)
	}
}

// copyNamePhotos godoc swagger
// @Router /name/change/folder [POST]
// @Summary  Change Transport Name photos folder in Nutanix
// @Description Change Transport Name photos folder in Nutanix
// @Tags Nutanix Temp
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data=Result}
// @Failure 500 {object} Response{data=Result}
func (s *Server) copyNamePhotos() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result Result
		)

		resp, err := s.nameUC.ChangeNameFolder(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = Result{
			Result: resp.Result,
		}

		Return(c, result, nil)
	}
}

// copyAnalogPhotos godoc swagger
// @Router /analog/change/photos/folder [POST]
// @Summary Change Analog Photos folder
// @Description Change Analog Photos folder
// @Tags Nutanix Temp
// @Accept json
// @Produce json
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data=Result}
// @Failure 500 {object} Response{data=Result}
func (s *Server) copyAnalogPhotos() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result Result
		)

		resp, err := s.analogUC.ChangeAnalogPhotosFolder(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = Result{
			Result: resp.Result,
		}

		Return(c, result, nil)
	}
}

// copyAnalogPdf godoc swagger
// @Router /analog/change/pdf/folder [POST]
// @Summary Change Analog PDF folder
// @Description Change Analog PDF folder
// @Tags Nutanix Temp
// @Accept json
// @Produce json
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data=Result}
// @Failure 500 {object} Response{data=Result}
func (s *Server) copyAnalogPdf() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result Result
		)

		resp, err := s.analogUC.ChangeAnalogPdfFolder(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = Result{
			Result: resp.Result,
		}

		Return(c, result, nil)
	}
}

type assessmentPhoto struct {
	Result []string `json:"result"`
}

// moveAssessmentPhotos godoc swagger
// @Router /assessment/change/photo/folder [POST]
// @Summary Change Assessment PDF folder
// @Description Change Assessment PDF folder
// @Tags Nutanix Temp
// @Accept json
// @Produce json
// @Success 200 {object} Response{data}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) moveAssessmentPhotos() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result assessmentPhoto
		)

		resp, err := s.analogUC.ChangeAssessmentPhotoFolder(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		Return(c, assessmentPhoto{Result: resp.Result}, nil)
	}
}

// moveProductPhotos godoc swagger
// @Router /product/change/photos/folder [POST]
// @Summary  Change folder
// @Description Change folder
// @Tags Nutanix Temp
// @Accept json
// @Produce json
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data=Result}
// @Failure 500 {object} Response{data=Result}
func (s *Server) moveProductPhotos() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		resp, err := s.productUC.ChangeProductPhotosFolder(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}
