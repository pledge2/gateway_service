package rest

import (
	"fmt"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type roleStore struct {
	Role   string `json:"role"`
	Code   int    `json:"code"`
	Direct int    `json:"direct"`
	Active bool   `json:"active"`
}

// createRole godoc
// @Router /role/store [POST]
// @Summary Create new User
// @Description API Create new User
// @Tags Admin Page UserRole
// @Produce json
// @Param token header string true "token"
// @Param request body roleStore true "Create New Role"
// @Success 201 {object} Response{data}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) createRole() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   roleStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.UserRole{
			Role:   data.Role,
			Code:   data.Code,
			Direct: data.Direct,
			Active: data.Active,
		}

		id, err := s.roleUC.CreateRole(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

// roleFindOne godoc
// @Router /role/find/one [GET]
// @Summary Find One User role
// @Description API Find One User role
// @Tags Admin Page UserRole
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=models.UserRoleResp}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) roleFindOne() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result roleStore
		)
		ID := c.Query("id")

		if ID == "" {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrRoleIDFormat)
			return
		}

		data, err := s.roleUC.FindOneRole(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = roleStore{
			Role:   data.Role,
			Code:   data.Code,
			Direct: data.Direct,
			Active: data.Active,
		}

		Return(c, result, nil)
	}
}

type userRoleResp struct {
	ID       string    `json:"id"`
	Index    int       `json:"index"`
	Role     string    `json:"role"`
	Code     int       `json:"code"`
	Active   bool      `json:"active"`
	PostCode int       `json:"post_code"`
	CBID     int       `json:"cbid"`
	CreateAt time.Time `json:"created_at"`
	UpdateAt time.Time `json:"updated_at"`
}

// roleFindAll godoc
// @Router /role/find/all [GET]
// @Summary Find All User roles
// @Description API Find All User roles
// @Tags Admin Page UserRole
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]userRoleResp}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) roleFindAll() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []userRoleResp

		role, isExist := c.Get("role")
		if !isExist {
			Return(c, result, errs.ErrRoleCodeEmpty)
			return
		}

		roleCode, err := strconv.Atoi(fmt.Sprint(role))
		if err != nil {
			Return(c, result, errs.ErrRoleCodeFormat)
			return
		}

		data, err := s.roleUC.RoleFindAll(c, roleCode)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			rData := userRoleResp{
				ID:       val.ID,
				Index:    val.Index,
				Role:     val.Role,
				Code:     val.Code,
				Active:   val.Active,
				PostCode: val.RoleCount.PostCode,
				CBID:     val.RoleCount.CBID,
				CreateAt: val.CreateAt,
				UpdateAt: val.UpdateAt,
			}
			result = append(result, rData)
		}

		Return(c, result, nil)
	}
}

type updateUserRole struct {
	Role   string `json:"role"`
	Code   int    `json:"code"`
	Active bool   `json:"active"`
}

// updateRole godoc
// @Router /role/update [PUT]
// @Summary Update User Role
// @Description API Update to User Role
// @Tags Admin Page UserRole
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Param request body updateUserRole true "User details"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateRole() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   updateUserRole
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrRoleIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.UserRole{
			Role:   data.Role,
			Code:   data.Code,
			Active: data.Active,
		}

		resp, err := s.roleUC.UpdateRole(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type updateUserStatus struct {
	Active bool `json:"active"`
}

// updateRoleStatus godoc
// @Router /role/update/status [PUT]
// @Summary Update User Status
// @Description API to User Status
// @Tags Admin Page UserRole
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Param request body updateUserStatus true "User status"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateRoleStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   updateUserStatus
			result Result
		)

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrRoleIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.UserRole{
			Active: data.Active,
		}

		resp, err := s.roleUC.UpdateRoleStatus(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// deleteRole godoc
// @Router /role/delete [DELETE]
// @Summary Delete User Role
// @Description API Delete to User Role
// @Tags Admin Page UserRole
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) deleteRole() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result Result
		)

		ID := c.Query("id")

		if ID == "" {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrRoleIDFormat)
			return
		}

		resp, err := s.roleUC.DeleteRole(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}
