package rest

import (
	"fmt"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"
	"strconv"

	"github.com/gin-gonic/gin"
)

type userCbids struct {
	ID   string `json:"id"`
	CBID int    `json:"cbid"`
}

// userFindList godoc
// @Router /user/find/list [GET]
// @Summary Find All User list
// @Description API Find All User list
// @Tags Admin Page User
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]userCbids}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) userFindList() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []userCbids

		data, err := s.usersUC.FindUserList(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			uData := userCbids{
				ID:   val.ID,
				CBID: val.CBID,
			}
			result = append(result, uData)
		}

		Return(c, result, nil)
	}
}

type postCodeList struct {
	Index        int    `json:"index"`
	PostCode     string `json:"post_code"`
	DepName      string `json:"dep_name"`
	PositionName string `json:"position_name"`
	Direction    string `json:"direction"`
}

// getRoleByPositionCode godoc
// @Router /get/positions/postcode [GET]
// @Summary Get role by position code
// @Description API to Get role by position code
// @Tags Admin Page User
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=[]postCodeList}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getRoleByPositionCode() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []postCodeList

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrRoleIDFormat)
			return
		}

		data, err := s.appRolesUC.GetAppRolesByPostCode(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			uData := postCodeList{
				Index:        val.Index,
				PostCode:     val.PostCode,
				DepName:      val.DepName,
				PositionName: val.PositionName,
				Direction:    val.Direction,
			}
			result = append(result, uData)
		}

		Return(c, result, err)
	}
}

type empPostCode struct {
	DepName  string `json:"dep_name"`
	PostName string `json:"post_name"`
}

// searchRolesByPostCode godoc
// @Router /search/positions/postcode [GET]
// @Summary Get EmpData using postCode
// @Description API to Get EmpData using postCode
// @Tags Admin Page User
// @Produce json
// @Param token header string true "token"
// @Param postcode query string true "postcode"
// @Success 201 {object} Response{data=empPostCode}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) searchRolesByPostCode() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result empPostCode

		postcode := c.Query("postcode")
		if postcode == "" {
			Return(c, nil, errs.ErrPostCodeEmpty)
			return
		}

		data, err := s.appRolesUC.SearchRolesByPostCode(c, postcode)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = empPostCode{
			DepName:  data.DepName,
			PostName: data.PositionName,
		}

		Return(c, result, nil)
	}
}

type cbidList struct {
	Index     int    `json:"index"`
	CBID      int    `json:"cbid"`
	BranchID  int    `json:"branch_id"`
	Name      string `json:"name"`
	Direction string `json:"direction"`
	ExpiredAt string `json:"expired_at"`
}

// getRoleByCBID godoc
// @Router /get/positions/cbid [GET]
// @Summary Get role by position code
// @Description API to Get role by position code
// @Tags Admin Page User
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=[]cbidList}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getRoleByCBID() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []cbidList

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}

		data, err := s.appRolesUC.GetAppRolesByCbID(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			uData := cbidList{
				Index:     val.Index,
				CBID:      val.CBID,
				BranchID:  val.BranchID,
				Name:      val.Name,
				Direction: val.Direction,
				ExpiredAt: val.ExpiredAt,
			}
			result = append(result, uData)
		}

		Return(c, result, nil)
	}
}

type empCBID struct {
	CBID     int    `json:"cbid"`
	BranchID int    `json:"branch_id"`
	Name     string `json:"name"`
}

// searchRolesByCbID godoc
// @Router /search/positions/cbid [GET]
// @Summary Get empData using cbID
// @Description API to Get empData using cbID
// @Tags Admin Page User
// @Produce json
// @Param token header string true "token"
// @Param cbid query string true "cbid"
// @Success 201 {object} Response{data=empCBID}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) searchRolesByCbID() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result empCBID

		cbid := c.Query("cbid")
		if cbid == "" {
			Return(c, result, errs.ErrCbidEmpty)
			return
		}

		intCBID, err := strconv.Atoi(cbid)
		if err != nil {
			Return(c, result, errs.ErrCbidFormat)
			return
		}

		data, err := s.appRolesUC.SearchEmpByCbID(c, intCBID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = empCBID{
			CBID:     data.CBID,
			BranchID: data.BranchID,
			Name:     data.Name,
		}

		Return(c, result, nil)
	}
}

type postcodeStore struct {
	PostCode string `json:"post_code"`
	Direct   int    `json:"direct"`
}

// addPositionPostCode godoc
// @Router /add/position/postcode [POST]
// @Summary Add new postcode for position
// @Description API to Add new postcode for position
// @Tags Admin Page User
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Param request body postcodeStore true "Postcode details"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) addPositionPostCode() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   postcodeStore
			result Result
		)

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, nil, errs.ErrCbidEmpty)
			return
		}

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.PostcodeList{
			PostCode: data.PostCode,
			Direct:   data.Direct,
		}

		resp, err := s.appRolesUC.AddPostcodeToRole(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// deletePositionPostCode godoc
// @Router /delete/position/postcode [DELETE]
// @Summary Delete new postcode for position
// @Description API to Delete new postcode for position
// @Tags Admin Page User
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Param postcode query string true "postcode"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) deletePositionPostCode() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result Result
		)

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrRoleIDFormat)
			return
		}

		postcode := c.Query("postcode")
		if postcode == "" {
			Return(c, result, errs.ErrPostCodeEmpty)
			return
		}

		req := domain.PostcodeList{
			PostCode: postcode,
		}

		data, err := s.appRolesUC.DeletePostcodeFromRole(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = data.Result

		Return(c, result, nil)
	}
}

type positionCBID struct {
	CBID      int    `json:"cbid"`
	Direct    int    `json:"direct"`
	ExpiredAt string `json:"expired_at"`
}

// addPositionCBID godoc
// @Router /add/position/cbid [POST]
// @Summary Add new cbid for position
// @Description API to Add new cbid for position
// @Tags Admin Page User
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Param request body positionCBID true "Cbid details"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) addPositionCBID() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   positionCBID
			result Result
		)

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}
		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrRoleIDFormat)
			return
		}

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.CBIDList{
			CBID:      data.CBID,
			Direct:    data.Direct,
			ExpiredAt: data.ExpiredAt,
		}

		resp, err := s.appRolesUC.AddCbidToRole(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// deletePositionCBID godoc
// @Router /delete/position/cbid [DELETE]
// @Summary Delete new cbid for position
// @Description API to Delete new cbid for position
// @Tags Admin Page User
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Param cbid query string true "cbid"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) deletePositionCBID() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result Result
		)

		ID := c.Query("id")

		if ID == "" {
			Return(c, result, errs.ErrRoleIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrRoleIDFormat)
			return
		}

		cbid := c.Query("cbid")

		if cbid == "" {
			Return(c, result, errs.ErrCbidNotFound)
			return
		}

		intcbid, err := strconv.Atoi(cbid)
		if err != nil {
			Return(c, result, errs.ErrCbidFormat)
			return
		}

		req := domain.CBIDList{
			CBID: intcbid,
		}

		data, err := s.appRolesUC.DeleteCbidFromRole(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = data.Result

		Return(c, result, nil)
	}
}

type userInfo struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	CBID     int    `json:"cbid"`
	Direct   int    `json:"direct"`
	Role     int    `json:"role"`
	Position string `json:"position"`
	BranchID int    `json:"branch_id"`
}

// getUserInfo godoc
// @Router /user/info [GET]
// @Summary Get userInfo by cbid
// @Description API to Get userInfo by cbid
// @Tags Admin Page User
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=userInfo}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getUserInfo() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			intCBID int
			result  userInfo
		)

		cbid, isExist := c.Get("cbid")
		if isExist {
			Cbid, err := strconv.Atoi(fmt.Sprint(cbid))
			if err != nil {
				Return(c, result, errs.ErrCbidFormat)
				return
			}

			intCBID = Cbid

		} else {
			Return(c, result, errs.ErrCbidNotFound)
			return
		}

		data, err := s.usersUC.UserInfo(c, intCBID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = userInfo{
			ID:       data.ID,
			Name:     data.Name,
			CBID:     data.CBID,
			Direct:   data.Direct,
			Role:     data.Role,
			Position: data.Position,
			BranchID: data.BranchID,
		}

		Return(c, result, nil)
	}
}

type usersList struct {
	Index         int      `json:"index"`
	ID            string   `json:"id"`
	CBID          int      `json:"cbid"`
	FIO           string   `json:"fio"`
	Email         string   `json:"email"`
	Devices       []string `json:"devices"`
	DevicesStatus []string `json:"devices_status"`
	Role          string   `json:"role"`
	Filial        int      `json:"filial"`
	CreatedAt     string   `json:"created_at"`
}

type usersPagination struct {
	UsersList  []usersList
	TotalPages int `json:"total_pages"`
}

// allUsersList godoc
// @Router /users/list [GET]
// @Summary Find all Users With Pagination
// @Description API to Find all Users With Pagination
// @Tags Admin Page User
// @Produce json
// @Param token header string true "token"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Success 201 {object} Response{data=usersPagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) allUsersList() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result usersPagination
		)

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		info, err := s.usersUC.AllUsersList(c, intLimit, intPage)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.TotalPages = info.TotalPages

		for _, val := range info.UsersList {
			uData := usersList{
				Index:         val.Index,
				ID:            val.ID,
				CBID:          val.CBID,
				FIO:           val.FIO,
				Email:         val.Email,
				Devices:       val.Devices,
				DevicesStatus: val.DevicesStatus,
				Role:          val.Role,
				Filial:        val.BranchID,
				CreatedAt:     val.CreatedAt,
			}
			result.UsersList = append(result.UsersList, uData)
		}

		Return(c, result, nil)
	}
}

// getEmployeeDataIABS godoc
// @Router /iabs/get/all [GET]
// @Summary Get All Employees Data
// @Description API to Get All Employees Data
// @Tags IABS
// @Produce json
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getEmployeeDataIABS() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		data, err := s.empUC.GetEmpDataIABS(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = data.Result

		Return(c, result, nil)
	}
}

type updateUserEmail struct {
	Email string `json:"email"`
}

// updateUserEmail godoc
// @Router /user/update/email [PUT]
// @Summary Update User Email
// @Description API to Update Employee Email
// @Tags Admin Page User
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Param request body updateUserEmail true "User details"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateUserEmail() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   updateUserEmail
			result Result
		)

		id := c.Query("id")
		if id == "" {
			Return(c, result, errs.ErrEmpIDEmpty)
			return
		}

		if !utils.IsValidUUID(id) {
			Return(c, result, errs.ErrEmpIDFormat)
			return
		}

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, err)
			return
		}

		req := domain.AppUsers{
			ID:    id,
			Email: data.Email,
		}

		resp, err := s.usersUC.UpdateUserEmail(c, id, req)
		if err != nil {
			Return(c, resp, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// deleteUserData godoc
// @Router /user/delete [DELETE]
// @Summary Delete User data
// @Description API to Delete Employee Data
// @Tags Admin Page User
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data.Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) deleteUserData() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		id := c.Query("id")
		if id == "" {
			Return(c, result, errs.ErrEmpIDEmpty)
			return
		}

		if !utils.IsValidUUID(id) {
			Return(c, result, errs.ErrEmpIDFormat)
			return
		}

		resp, err := s.usersUC.DeleteUser(c, id)
		if err != nil {
			Return(c, resp, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// blockUser godoc
// @Router /user/block [DELETE]
// @Summary Block User
// @Description API to Block User
// @Tags Admin Page User
// @Produce json
// @Param token header string true "token"
// @Param cbid query string true "cbid"
// @Success 201 {object} Response{data.Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) blockUser() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		cbid := c.Query("cbid")
		if cbid == "" {
			Return(c, result, errs.ErrCbidEmpty)
			return
		}

		intCBID, err := strconv.Atoi(cbid)
		if err != nil {
			Return(c, result, errs.ErrCbidFormat)
			return
		}

		if intCBID < 1 {
			Return(c, result, errs.ErrCbidFormat)
			return
		}

		resp, err := s.usersUC.BlockUser(c, intCBID)
		if err != nil {
			Return(c, resp, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}
