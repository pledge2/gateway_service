package rest

import (
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type empInfo struct {
	ID          string    `json:"id"`
	Index       int       `json:"index"`
	BranchID    int       `json:"branchId"`
	CategoryEmp string    `json:"categoryEmp"`
	CBID        int       `json:"cbId"`
	DepCode     string    `json:"depCode"`
	DepName     string    `json:"depName"`
	EmpState    string    `json:"empState"`
	LocalCode   string    `json:"localCode"`
	MailAddress string    `json:"mailAddress"`
	Name        string    `json:"name"`
	PhoneMobil  string    `json:"phoneMobil"`
	PostCode    string    `json:"postCode"`
	PostName    string    `json:"postName"`
	State       string    `json:"state"`
	UserState   string    `json:"userState"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

// findEmployee godoc
// @Router /emp/find/one [GET]
// @Summary Find Employee
// @Description API to Find Employee
// @Tags Admin Page Employee
// @Produce json
// @Param token header string true "token"
// @Param cbid query string true "CBID"
// @Success 201 {object} Response{data=empInfo}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findEmployee() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result empInfo

		cbid := c.Query("cbid")
		if cbid == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intCBID, err := strconv.Atoi(cbid)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intCBID < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		resp, err := s.empUC.FindEmployee(c, intCBID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = empInfo{
			ID:          resp.ID,
			Index:       resp.Index,
			BranchID:    resp.BranchID,
			CategoryEmp: resp.CategoryEmp,
			CBID:        resp.CBID,
			DepCode:     resp.DepCode,
			DepName:     resp.DepName,
			EmpState:    resp.EmpState,
			LocalCode:   resp.LocalCode,
			MailAddress: resp.MailAddress,
			Name:        resp.Name,
			PhoneMobil:  resp.PhoneMobil,
			PostCode:    resp.PostCode,
			PostName:    resp.PostName,
			State:       resp.State,
			UserState:   resp.UserState,
			CreatedAt:   resp.CreatedAt,
			UpdatedAt:   resp.UpdatedAt,
		}

		Return(c, result, nil)
	}
}

type employeesPagination struct {
	EmpInfo    []empInfo `json:"employees"`
	TotalPages int       `json:"total_pages"`
}

// findAllEmployees godoc
// @Router /emp/find/all [GET]
// @Summary Find all Employees
// @Description API to Find all Employees
// @Tags Admin Page Employee
// @Produce json
// @Param token header string true "token"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Success 201 {object} Response{data=employeesPagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllEmployees() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result employeesPagination

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intLimit <= 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		resp, err := s.empUC.FindAllEmployees(c, intLimit, intPage)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.TotalPages = resp.TotalPages

		for _, val := range resp.EmpInfo {
			eData := empInfo{
				ID:          val.ID,
				Index:       val.Index,
				BranchID:    val.BranchID,
				CategoryEmp: val.CategoryEmp,
				CBID:        val.CBID,
				DepCode:     val.DepCode,
				DepName:     val.DepName,
				EmpState:    val.EmpState,
				LocalCode:   val.LocalCode,
				MailAddress: val.MailAddress,
				Name:        val.Name,
				PhoneMobil:  val.PhoneMobil,
				PostCode:    val.PostCode,
				PostName:    val.PostName,
				State:       val.State,
				UserState:   val.UserState,
				CreatedAt:   val.CreatedAt,
				UpdatedAt:   val.UpdatedAt,
			}
			result.EmpInfo = append(result.EmpInfo, eData)
		}

		Return(c, result, nil)
	}
}

type updateEmpInfo struct {
	BranchID    int    `json:"branchId"`
	CategoryEmp string `json:"categoryEmp"`
	CBID        int    `json:"cbId"`
	DepCode     string `json:"depCode"`
	DepName     string `json:"depName"`
	EmpState    string `json:"empState"`
	LocalCode   string `json:"localCode"`
	MailAddress string `json:"mailAddress"`
	Name        string `json:"name"`
	PhoneMobil  string `json:"phoneMobil"`
	PostCode    string `json:"postCode"`
	PostName    string `json:"postName"`
	State       string `json:"state"`
	UserState   string `json:"userState"`
}

// findAllEmployees godoc
// @Router /emp/update [PUT]
// @Summary Update Employee Info
// @Description API to Update Employee Info
// @Tags Admin Page Employee
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Param request body updateEmpInfo true "Emp details"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateEmpData() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   updateEmpInfo
			result Result
		)

		id := c.Query("id")
		if id == "" {
			Return(c, result, errs.ErrEmpIDEmpty)
			return
		}

		if !utils.IsValidUUID(id) {
			Return(c, result, errs.ErrEmpIDFormat)
			return
		}

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, err)
			return
		}

		req := domain.EmpInfo{
			BranchID:    data.BranchID,
			CategoryEmp: data.CategoryEmp,
			CBID:        data.CBID,
			DepCode:     data.DepCode,
			DepName:     data.DepName,
			EmpState:    data.EmpState,
			LocalCode:   data.LocalCode,
			MailAddress: data.MailAddress,
			Name:        data.Name,
			PhoneMobil:  data.PhoneMobil,
			PostCode:    data.PostCode,
			PostName:    data.PostName,
			State:       data.State,
			UserState:   data.UserState,
		}

		resp, err := s.empUC.UpdateEmpData(c, id, req)
		if err != nil {
			Return(c, resp, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}
