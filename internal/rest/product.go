package rest

import (
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"

	"github.com/gin-gonic/gin"
)

type productStore struct {
	Product   string `json:"product" binding:"required"`
	PhotoName string `json:"photo_name"`
}

// productStore godoc
// @Router /product/store [POST]
// @Summary Create a new Product
// @Description API Create Product
// @Tags Admin Page
// @Produce json
// @Param token header string true "token"
// @Param request body productStore true "Product details"
// @Success 201 {object} Response{idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) productStore() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   productStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return

		}

		req := domain.StoreProduct{
			Product:   data.Product,
			PhotoName: data.PhotoName,
		}

		id, err := s.productUC.StoreProduct(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

type productResponse struct {
	ID        string `json:"id"`
	Product   string `json:"product"`
	PhotoUrl  string `json:"photo_url"`
	Active    bool   `json:"active"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

// productFindOne godoc
// @Router /product/find/one [GET]
// @Summary Find Product
// @Description API Find Product
// @Tags Admin Page
// @Produce json
// @Param token header string true "token"
// @Param id query string true "Product -> id"
// @Success 201 {object} Response{data=productResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) productFindOne() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result productResponse
		)

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrProductIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrProductIDFormat)
			return
		}

		data, err := s.productUC.FindOneProduct(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = productResponse{
			ID:        data.ID,
			Product:   data.Product,
			PhotoUrl:  data.PhotoUrl,
			Active:    data.Active,
			CreatedAt: data.CreatedAt,
			UpdatedAt: data.UpdatedAt,
		}

		Return(c, result, nil)
	}
}

// findAllProduct godoc
// @Router /product/find/all [GET]
// @Summary Find all Products
// @Description API to find all Products
// @Tags Both Device
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]productResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllProduct() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result []productResponse
		)

		data, err := s.productUC.FindAllProduct(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			info := productResponse{
				ID:        val.ID,
				Product:   val.Product,
				PhotoUrl:  val.PhotoUrl,
				Active:    val.Active,
				CreatedAt: val.CreatedAt,
			}
			result = append(result, info)
		}

		Return(c, result, nil)
	}
}

type productPhoto struct {
	PhotoName string `json:"photo_name" binding:"required"`
}

// updateProductPhotoUrl godoc
// @Router /product/update/photo [PUT]
// @Summary Update Product Photo Url
// @Description API to Update product Photo Url
// @Tags Web
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body productPhoto true "Type url"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateProductPhotoUrl() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   productPhoto
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrProductIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrProductIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.UpdateProductPhoto{
			PhotoName: data.PhotoName,
		}

		resp, err := s.productUC.UpdateProductPhoto(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type productUpdateStatus struct {
	Active bool `json:"active"`
}

// updateProductStatus godoc
// @Router /product/update/status [PUT]
// @Summary Update Product Status
// @Description API to Update Product Status
// @Tags Admin Page Analog
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body productUpdateStatus true "Product status"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateProductStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   productUpdateStatus
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrValidityIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrValidityIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.UpdateProductStatus{
			Active: data.Active,
		}

		resp, err := s.productUC.UpdateProductStatus(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}
