package rest

import (
	"fmt"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"
	"strconv"

	"github.com/gin-gonic/gin"
)

type assessmentReport struct {
	MarketPrice       int    `json:"market_price"`
	CreditPrice       int    `json:"credit_price"`
	TransportModel    string `json:"transport_model"`
	TransportName     string `json:"transport_name"`
	TransportPosition string `json:"transport_position"`
	TransportYear     int    `json:"transport_year"`
}

type officialAssessment struct {
	ID string `json:"id" binding:"required"`
}

// officialAssessment godoc
// @Router /assessment/official [POST]
// @Summary Create a new Assessment
// @Description API Create an Assessment
// @Tags Both Device
// @Produce json
// @Param token header string true "token"
// @Param request body officialAssessment true "Fast Assessment details"
// @Success 201 {object} Response{data=assessmentReport}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) officialAssessment() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			request officialAssessment
			result  assessmentReport
		)

		err := c.ShouldBindJSON(&request)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		if !utils.IsValidUUID(request.ID) {
			Return(c, result, errs.ErrAssessmentIDFormat)

			return
		}

		req := domain.AssessmentStore{
			ID: request.ID,
		}

		data, err := s.assessmentUC.OfficialAssessment(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = assessmentReport{
			MarketPrice:       data.MarketPrice,
			CreditPrice:       data.CreditPrice,
			TransportModel:    data.TransportModel,
			TransportName:     data.TransportName,
			TransportPosition: data.TransportPosition,
			TransportYear:     data.TransportYear,
		}

		Return(c, result, nil)
	}
}

type assessmentFast struct {
	Type        string `json:"type" binding:"required"`
	Model       string `json:"model" binding:"required"`
	Name        string `json:"name" binding:"required"`
	Year        int    `json:"year" binding:"required"`
	Validity    int    `json:"validity"`
	Position    string `json:"position" binding:"required"`
	Fuel        string `json:"fuel"`
	Speedometer int    `json:"speedometer"`
	ExtraParam  string `json:"extra_param"`
	NameKey     string `json:"name_key"`
}

// fastAssessment godo
// @Router /assessment/fast [POST]
// @Summary Create an new Assessment
// @Description API Create an Assessment
// @Tags Both Device
// @Produce json
// @Param token header string true "token"
// @Param request body assessmentFast true "Fast Assessment details"
// @Success 201 {object} Response{data=assessmentReport}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) fastAssessment() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   assessmentFast
			result assessmentReport
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		if !utils.IsTrailer(data.NameKey) {
			if data.Fuel == "" && data.Speedometer == 0 {
				Return(c, result, errs.ErrValidation)
				return
			}
		}

		req := domain.AssessmentStore{
			Transport: domain.Transport{
				Type:        data.Type,
				Model:       data.Model,
				Name:        data.Name,
				NameKey:     data.NameKey,
				Year:        data.Year,
				Speedometer: data.Speedometer,
				Validity:    data.Validity,
				Position:    data.Position,
				Fuel:        data.Fuel,
				ExtraParam:  data.ExtraParam,
			},
		}

		info, err := s.assessmentUC.FastAssessment(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = assessmentReport{
			MarketPrice:       info.MarketPrice,
			CreditPrice:       info.CreditPrice,
			TransportModel:    info.TransportModel,
			TransportName:     info.TransportName,
			TransportPosition: info.TransportPosition,
			TransportYear:     info.TransportYear,
		}

		Return(c, result, nil)
	}
}

type allPhotoUrl struct {
	PhotoUrl []photos `json:"photo"`
}

type photos struct {
	Title    string `json:"title"`
	Order    int    `json:"order"`
	PhotoUrl string `json:"photo_url"`
	LAT      string `json:"lat"`
	LON      string `json:"lon"`
}

type assessmentResponse struct {
	ID                 string      `json:"id"`
	CBID               int         `json:"cbid"`
	PID                string      `json:"p_id"`
	IIBInfoID          string      `json:"iib_info_id"`
	Status             int         `json:"status"`
	VehicleKuzov       string      `json:"vehicle_kuzov"`
	OwnerPinfl         string      `json:"owner_pinfl"`
	OwnerFullname      string      `json:"owner_fullname"`
	VehicleModel       string      `json:"vehicle_model"`
	VehicleColor       string      `json:"vehicle_color"`
	VehicleMadeYear    int         `json:"vehicle_year"`
	VehicleMotor       string      `json:"vehicle_motor"`
	VehicleFullWeight  int         `json:"vehicle_full_weight"`
	VehicleEmptyWeight int         `json:"vehicle_empty_weight"`
	VehiclePlateNumber string      `json:"vehicle_plate_number"`
	VehiclePower       int         `json:"vehicle_power"`
	PassportNumber     string      `json:"passport_number"`
	PassportSeria      string      `json:"passport_seria"`
	VinNumber          string      `json:"vin_number"`
	VehicleShassi      string      `json:"vehicle_shassi"`
	Type               string      `json:"type"`
	TypeKey            string      `json:"type_key"`
	Model              string      `json:"model"`
	Name               string      `json:"name"`
	NameKey            string      `json:"name_key"`
	Position           string      `json:"position"`
	Year               int         `json:"year"`
	Speedometer        int         `json:"speedometer"`
	Validity           int         `json:"validity"`
	Fuel               string      `json:"fuel"`
	Price              int         `json:"price"`
	SellPrice          int         `json:"sell_price"`
	MarketPrice        int         `json:"market_price"`
	CreditPrice        int         `json:"credit_price"`
	TransportModel     string      `json:"transport_model"`
	TransportName      string      `json:"transport_name"`
	TransportPosition  string      `json:"transport_position"`
	TransportYear      int         `json:"transport_year"`
	Photos             allPhotoUrl `json:"photos"`
	ExtraParam         string      `json:"extra_param"`
	Url                string      `json:"url"`
	IIBPermitInfo      string      `json:"iib_permit_info"`
}

// findAssessment godoc
// @Router /assessment/find/one [GET]
// @Summary Find an Assessment BY assessmentID
// @Description API Find Assessment
// @Tags Both Device
// @Produce json
// @Param token header string true "Access token"
// @Param id query string true "Assessment details"
// @Success 201 {object} Response{data=assessmentResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAssessment() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result assessmentResponse

		ID := c.Query("id")
		if ID == "" {
			Return(c, nil, errs.ErrAssessmentIDEmpty)
			return
		}
		data, err := s.assessmentUC.FindAssessment(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = assessmentResponse{
			ID:                 data.ID,
			CBID:               data.CBID,
			PID:                data.PID,
			IIBInfoID:          data.IIBInfoID,
			Status:             data.Status,
			VehicleKuzov:       data.GovUz.VehicleKuzov,
			OwnerPinfl:         data.GovUz.OwnerPinfl,
			OwnerFullname:      data.GovUz.OwnerFullname,
			VehicleModel:       data.GovUz.VehicleModel,
			VehicleColor:       data.GovUz.VehicleColor,
			VehicleMadeYear:    data.GovUz.VehicleMadeYear,
			VehicleMotor:       data.GovUz.VehicleMotor,
			VehicleFullWeight:  data.GovUz.VehicleFullWeight,
			VehicleEmptyWeight: data.GovUz.VehicleEmptyWeight,
			VehiclePlateNumber: data.GovUz.VehiclePlateNumber,
			VehiclePower:       data.GovUz.VehiclePower,
			PassportNumber:     data.GovUz.PassportNumber,
			PassportSeria:      data.GovUz.PassportSeria,
			VinNumber:          data.GovUz.VinNumber,
			VehicleShassi:      data.GovUz.VehicleShassi,
			Type:               data.Transport.Type,
			TypeKey:            data.Transport.TypeKey,
			Model:              data.Transport.Model,
			Name:               data.Transport.Name,
			NameKey:            data.Transport.NameKey,
			Position:           data.Transport.Position,
			Year:               data.Transport.Year,
			Speedometer:        data.Transport.Speedometer,
			Validity:           data.Transport.Validity,
			Fuel:               data.Transport.Fuel,
			SellPrice:          data.Transport.SellPrice,
			MarketPrice:        data.MarketPrice,
			CreditPrice:        data.CreditPrice,
			TransportModel:     data.TransportView.TransportModel,
			TransportName:      data.TransportView.TransportName,
			TransportPosition:  data.TransportView.TransportPosition,
			TransportYear:      data.TransportView.TransportYear,
			IIBPermitInfo:      data.IIBPermitInfo,
			ExtraParam:         data.Transport.ExtraParam,
			Url:                data.Url,
		}

		for _, photo := range data.AllPhotosUrl.PhotoSource {
			value := photos{
				Title:    photo.Title,
				Order:    photo.Order,
				PhotoUrl: photo.PhotoUrl,
				LAT:      photo.LAT,
				LON:      photo.LON,
			}
			result.Photos.PhotoUrl = append(result.Photos.PhotoUrl, value)
		}

		Return(c, result, nil)
	}
}

type assessmentPagination struct {
	Assessments []assessment `json:"assessments"`
	TotalPages  int          `json:"total_pages"`
	StatusName  string       `json:"status_name"`
}

type assessment struct {
	ID                 string      `json:"id"`
	CBID               int         `json:"cbid"`
	AID                string      `json:"a_id"`
	PID                string      `json:"p_id"`
	GID                string      `json:"g_id"`
	IIBInfoID          string      `json:"iib_info_id"`
	Status             int         `json:"status"`
	OwnerFullName      string      `json:"owner_fullname"`
	VehiclePlateNumber string      `json:"vehicle_plate_number"`
	VehicleKuzov       string      `json:"vehicle_kuzov"`
	PhotoUrl           allPhotoUrl `json:"photos"`
	CreateAt           string      `json:"created_at"`
	TransportName      string      `json:"transport_name"`
	CardID             string      `json:"card_id"`
	EmpName            string      `json:"emp_name"`
}

// findAssessmentByStatus godoc
// @Router /assessment/find/status [GET]
// @Summary Find an Assessment By Status
// @Description API Find Assessment By Status
// @Tags Both Device
// @Produce json
// @Param token header string true "Access token"
// @Param status query string true "Status"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Success 201 {object} Response{data=assessmentPagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAssessmentByStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			cbid, branchID, roleCode, direct int
			result                           assessmentPagination
		)

		value, isExist := c.Get("cbid")
		if isExist {
			intCbid, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				Return(c, result, errs.ErrCbidFormat)
				return
			}
			cbid = intCbid
		} else {
			Return(c, result, errs.ErrCbidEmpty)
			return
		}

		value, isExist = c.Get("role")
		if isExist {
			intRole, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				Return(c, result, errs.ErrRoleCodeFormat)
				return
			}
			roleCode = intRole
		} else {
			Return(c, result, errs.ErrRoleCodeEmpty)
			return
		}

		value, isExist = c.Get("branch_id")
		if isExist {
			intBranchID, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				Return(c, result, errs.ErrBranchIDFormat)
				return
			}
			branchID = intBranchID
		} else {
			Return(c, result, errs.ErrBranchIDEmpty)
			return
		}

		value, isExist = c.Get("direct")
		if isExist {
			intDirect, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				Return(c, result, errs.ErrDirectFormat)
				return
			}
			direct = intDirect
		} else {
			Return(c, result, errs.ErrDirectEmpty)
			return
		}

		statusParam := c.Query("status")
		if statusParam == "" {
			Return(c, result, errs.ErrStatusCodeEmpty)
			return
		}

		intStatus, err := strconv.Atoi(statusParam)
		if err != nil {
			Return(c, result, errs.ErrStatusCodeFormat)
			return
		}

		if intStatus < 0 {
			Return(c, result, errs.ErrStatusCodeFormat)
			return
		}

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}
		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}
		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		data, err := s.assessmentUC.FindAssessmentByStatus(c, intStatus, intLimit, intPage, cbid, roleCode, branchID, direct)
		if err != nil {
			Return(c, result, err)
			return
		}
		result = assessmentPagination{
			TotalPages: data.TotalPages,
			StatusName: data.StatusName,
		}

		for _, val := range data.Assessments {
			info := assessment{
				ID:                 val.ID,
				CBID:               val.CBID,
				AID:                val.AID,
				PID:                val.PID,
				GID:                val.GID,
				IIBInfoID:          val.IIBInfoID,
				Status:             val.Status,
				CardID:             val.CardID,
				EmpName:            val.EmpName,
				OwnerFullName:      val.GovUz.OwnerFullname,
				VehiclePlateNumber: val.GovUz.VehiclePlateNumber,
				VehicleKuzov:       val.GovUz.VehicleKuzov,
				CreateAt:           val.CreatedAt,
				TransportName:      val.TransportView.TransportName,
			}

			for _, photo := range val.AllPhotosUrl.PhotoSource {
				value := photos{
					Title:    photo.Title,
					Order:    photo.Order,
					PhotoUrl: photo.PhotoUrl,
					LAT:      photo.LAT,
					LON:      photo.LON,
				}
				info.PhotoUrl.PhotoUrl = append(info.PhotoUrl.PhotoUrl, value)
			}
			result.Assessments = append(result.Assessments, info)
		}

		Return(c, result, nil)
	}
}

type assessmentStatus struct {
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
	Count      int    `json:"count"`
}

// countStatusAssessment godoc
// @Router /assessment/count/status [GET]
// @Summary Count All Status
// @Description API Count All Status
// @Tags Both Device
// @Produce json
// @Param token header string true "Access token"
// @Success 201 {object} Response{data=[]assessmentStatus}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) countStatusAssessment() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			intCBID, intRoleCode, intBranchID, direct int
			result                                    []assessmentStatus
		)

		value, isExist := c.Get("cbid")
		if isExist {
			cbid, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				Return(c, result, errs.ErrCbidFormat)
				return
			}
			intCBID = cbid
		} else {
			Return(c, result, errs.ErrCbidNotFound)
			return
		}

		value, isExist = c.Get("role")
		if isExist {
			role, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				Return(c, result, errs.ErrRoleCodeFormat)
				return
			}
			intRoleCode = role
		} else {
			Return(c, result, errs.ErrRoleCodeEmpty)
			return
		}

		value, isExist = c.Get("branch_id")
		if isExist {
			branchID, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				Return(c, result, errs.ErrBranchIDFormat)
				return
			}
			intBranchID = branchID
		} else {
			Return(c, result, errs.ErrBranchIDEmpty)
			return
		}

		_, isExist = c.Get("device_num")
		if !isExist {
			Return(c, result, errs.ErrDeviceFormat)
			return
		}

		_, isExist = c.Get("device_type")
		if !isExist {
			Return(c, result, errs.ErrDeviceFormat)
			return
		}

		value, isExist = c.Get("direct")
		if isExist {
			intDirect, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				Return(c, result, errs.ErrDirectFormat)
				return
			}
			direct = intDirect
		} else {
			Return(c, result, errs.ErrDirectEmpty)
			return
		}

		data, err := s.assessmentUC.CountStatusAssessment(c, intCBID, intBranchID, intRoleCode, direct)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			value := assessmentStatus{
				StatusCode: val.StatusCode,
				StatusName: val.StatusName,
				Count:      val.StatusCount,
			}
			result = append(result, value)
		}

		Return(c, result, nil)
	}
}

type govUz struct {
	VehicleKuzov       string `json:"vehicle_kuzov"`
	OwnerPinfl         string `json:"owner_pinfl"`
	OwnerFullname      string `json:"owner_fullname"`
	VehicleModel       string `json:"vehicle_model"`
	VehicleColor       string `json:"vehicle_color"`
	VehicleMadeYear    int    `json:"vehicle_made_year"`
	VehicleMotor       string `json:"vehicle_motor"`
	VehicleFullWeight  int    `json:"vehicle_full_weight"`
	VehicleEmptyWeight int    `json:"vehicle_empty_weight"`
	VehiclePlateNumber string `json:"vehicle_plate_number"`
	PassportNumber     string `json:"passport_number"`
	PassportSeria      string `json:"passport_seria"`
	VinNumber          string `json:"vin_number"`
	VehiclePower       int    `json:"vehicle_power"`
	Seats              int    `json:"seats"`
	VehicleShassi      string `json:"vehicle_shassi"`
	IibPermitInfo      string `json:"iib_permit_info"`
}

type techNumberParams struct {
	AssessmentID   string `json:"assessment_id" binding:"required"`
	PassportNumber string `json:"passport_number" binding:"required"`
	PassportSeria  string `json:"passport_seria" binding:"required"`
	PlateNumber    string `json:"plate_number" binding:"required"`
}

// govCarInfo godoc
// @Router /gov/vehicle/carinfo [POST]
// @Summary Integration with GOV
// @Description Integration with Technical Passport
// @Tags Both Device
// @Produce json
// @Param token header string true "token"
// @Param request body techNumberParams true "Official Assessment details"
// @Success 201 {object} Response{data=govUz}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) govCarInfo() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   techNumberParams
			result govUz
		)

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		request := domain.TechNumberParams{
			AssessmentID:   data.AssessmentID,
			PassportNumber: data.PassportNumber,
			PassportSeria:  data.PassportSeria,
			PlateNumber:    data.PlateNumber,
		}

		info, err := s.assessmentUC.GovCarInfo(c, request)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = govUz{
			OwnerFullname:      info.OwnerFullname,
			OwnerPinfl:         info.OwnerPinfl,
			VehicleMadeYear:    info.VehicleMadeYear,
			VehiclePlateNumber: info.VehiclePlateNumber,
			VehicleModel:       info.VehicleModel,
			VehicleColor:       info.VehicleColor,
			VehicleKuzov:       info.VehicleKuzov,
			VehicleFullWeight:  info.VehicleFullWeight,
			VehicleEmptyWeight: info.VehicleEmptyWeight,
			VehicleMotor:       info.VehicleMotor,
			PassportNumber:     info.PassportNumber,
			PassportSeria:      info.PassportSeria,
			VinNumber:          info.VinNumber,
			VehiclePower:       info.VehiclePower,
			Seats:              info.Seats,
			VehicleShassi:      info.VehicleShassi,
			IibPermitInfo:      "",
		}

		Return(c, result, nil)
	}
}

type assessmentStore struct {
	CBID        int    `json:"cbid" example:"56833"`                                // user id
	EXTCBID     int    `json:"ext_cbid" example:"56833"`                            // user id
	Direct      int    `json:"direct" example:"1"`                                  // user id
	BranchID    int    `json:"branch_id" example:"21981"`                           // user id
	PID         string `json:"p_id" example:"a2a51ecf-d7c6-423b-b80b-0d7887f907f5"` // product id
	Type        string `json:"type" example:"e2dc1bbc-b464-4df5-811f-3329636a4ef0"`
	Model       string `json:"model" example:"890753db-47e1-4723-bb23-bcd9ea14c47e"`
	Name        string `json:"name" example:"296e966d-c0f9-42e0-932b-46d8c164cdb0"`
	Position    string `json:"position" example:"42d6106a-10cc-4417-a03a-0b5e2dc6a4aa"`
	Year        int    `json:"year" example:"2023"`
	Speedometer int    `json:"speedometer" example:"10000"`
	Validity    int    `json:"validity" example:"1"`
	Fuel        string `json:"fuel" example:"bfcdea15-3203-4a1d-9abe-36957241b75a"`
	ExtraParam  string `json:"extra_param"`
}

// storeMainInfo godoc
// @Router /assessment/store/main [POST]
// @Summary Create a new Assessment
// @Description API Create a new Assessment
// @Tags Both Device
// @Produce json
// @Param token header string true "token"
// @Param request body assessmentStore true "Fast Assessment details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) storeMainInfo() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   assessmentStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}
		branchID, isExist := c.Get("branch_id")
		if isExist {
			intBranchID, err := strconv.Atoi(fmt.Sprint(branchID))
			if err != nil {
				Return(c, result, errs.ErrBranchIDFormat)
				return
			}
			data.BranchID = intBranchID
		} else {
			Return(c, result, errs.ErrBranchIDEmpty)
			return
		}

		cbid, isExist := c.Get("cbid")
		if isExist {
			intCBID, err := strconv.Atoi(fmt.Sprint(cbid))
			if err != nil {
				Return(c, result, errs.ErrCbidFormat)
				return
			}
			data.CBID = intCBID
		} else {
			Return(c, result, errs.ErrCbidEmpty)
			return
		}

		value, isExist := c.Get("direct")
		if isExist {
			intDirect, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				Return(c, result, errs.ErrDirectFormat)
				return
			}
			data.Direct = intDirect
		} else {
			Return(c, result, errs.ErrDirectEmpty)
			return
		}

		request := domain.AssessmentStore{
			CBID:     data.CBID,
			EXTCBID:  data.EXTCBID,
			Direct:   data.Direct,
			BranchID: data.BranchID,
			PID:      data.PID,
			Transport: domain.Transport{
				Type:        data.Type,
				Model:       data.Model,
				Name:        data.Name,
				Position:    data.Position,
				Year:        data.Year,
				Speedometer: data.Speedometer,
				Validity:    data.Validity,
				Fuel:        data.Fuel,
				ExtraParam:  data.ExtraParam,
			},
		}

		id, err := s.assessmentUC.StoreMainInfo(c, request)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

// getPhotos godoc
// @Router /assessment/get/photos [GET]
// @Summary Create a new Assessment
// @Description API Create a new Assessment
// @Tags Web
// @Produce json
// @Param token header string true "Access token"
// @Param assessmentID query string true "Assessment ID"
// @Success 201 {object} Response{data=allPhotoUrl}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getPhotos() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result allPhotoUrl
		)

		assessmentID := c.Query("assessmentID")
		if assessmentID == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)
			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, nil, errs.ErrProductIDEmpty)
			return
		}

		data, err := s.assessmentUC.GetPhotos(c, assessmentID)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, photo := range data.PhotoSource {
			value := photos{
				Title:    photo.Title,
				Order:    photo.Order,
				PhotoUrl: photo.PhotoUrl,
				LAT:      photo.LAT,
				LON:      photo.LON,
			}
			result.PhotoUrl = append(result.PhotoUrl, value)
		}

		Return(c, result, nil)
	}
}

type changeStatus struct {
	AssessmentID string `json:"assessment_id" binding:"required"`
	StatusCode   int    `json:"status_code"`
	Comment      string `json:"comment"`
}

// changeAssessmentByStatus godoc
// @Router /assessment/change/status [POST]
// @Summary Change an Assessment By Status
// @Description API Change Assessment By Status
// @Tags Web
// @Produce json
// @Param token header string true "Access token"
// @Param request body changeStatus true "AssessmentStatus"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) changeAssessmentByStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   changeStatus
			result Result
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.AssessmentStatus{
			ID:         data.AssessmentID,
			StatusCode: data.StatusCode,
			Comment:    data.Comment,
		}

		resp, err := s.assessmentUC.ChangeAssessmentStatus(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type statusResponse struct {
	ID         string `json:"id"`
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
	Count      int    `json:"count"`
	CreatedAt  string `json:"created_at"`
}

// findAllStatus godoc
// @Router /status/find/all [GET]
// @Summary Find all Statuses
// @Description API to find all Statuses
// @Tags Web
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]statusResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			intRole int
			result  []statusResponse
		)

		value, isExist := c.Get("role")
		if isExist {
			role, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				Return(c, result, errs.ErrRoleCodeFormat)
				return
			}
			intRole = role
		} else {
			Return(c, result, errs.ErrRoleCodeEmpty)
			return
		}

		data, err := s.assessmentStatusUC.FindAllStatus(c, intRole)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			value := statusResponse{
				ID:         val.ID,
				StatusCode: val.StatusCode,
				StatusName: val.StatusName,
				Count:      val.StatusCount,
				CreatedAt:  val.CreatedAt,
			}
			result = append(result, value)
		}

		Return(c, result, nil)
	}
}

type statusStore struct {
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
}

// statusStore godoc
// @Router /status/store [POST]
// @Summary Create a new Status
// @Description API Create a Status
// @Tags Admin Page Assessment
// @Produce json
// @Param token header string true "token"
// @Param request body statusStore true "Status details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) statusStore() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   statusStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		request := domain.AssessmentStatus{
			StatusCode: data.StatusCode,
			StatusName: data.StatusName,
		}

		id, err := s.assessmentStatusUC.StoreStatus(c, request)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

type assessmentAnalogList struct {
	ID            string `json:"id"`
	Model         string `json:"model"`
	Name          string `json:"name"`
	Position      string `json:"position"`
	Speedometer   int    `json:"speedometer"`
	Year          int    `json:"year"`
	Fuel          string `json:"fuel"`
	SellPrice     int    `json:"sell_price"`
	Price         int    `json:"price"`
	Validity      string `json:"validity"`
	SourceFileUrl string `json:"source_file_url"`
	SourceFile    string `json:"source_file"`
}

// analogsList godoc
// @Router /assessment/analogs/list [GET]
// @Summary Get all Analogs list with assessmentID
// @Description API To Get all Analogs list with assessmentID
// @Tags Admin Page Assessment
// @Produce json
// @Param token header string true "token"
// @Param assessmentID query string true "assessmentID"
// @Success 201 {object} Response{data=[]assessmentAnalogList}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) analogsList() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result []assessmentAnalogList
		)

		assessmentID := c.Query("assessmentID")
		if assessmentID == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)
			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, result, errs.ErrAssessmentIDFormat)
			return
		}

		data, err := s.assessmentUC.AssessmentAnalogsLists(c, assessmentID)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			aData := assessmentAnalogList{
				ID:            val.ID,
				Model:         val.Model,
				Name:          val.Name,
				Position:      val.Position,
				Year:          val.Year,
				Speedometer:   val.Speedometer,
				SellPrice:     val.SellPrice,
				Price:         val.Price,
				Fuel:          val.Fuel,
				Validity:      val.Condition,
				SourceFile:    val.SourceFile,
				SourceFileUrl: val.SourceFileUrl,
			}
			result = append(result, aData)
		}

		Return(c, result, nil)
	}
}

type appTransportInfo struct {
	TransportModel       string `json:"transport_model"`
	TransportName        string `json:"transport_name"`
	TransportPosition    string `json:"transport_position"`
	TransportYear        int    `json:"transport_year"`
	TransportSpeedometer int    `json:"transport_speedometer"`
	TransportCondition   string `json:"transport_condition"`
	TransportFuel        string `json:"transport_fuel"`
	TransportPrice       int    `json:"market_price"`
	TransportSellPrice   int    `json:"credit_price"`
}

type govUzData struct {
	OwnerPinfl               string `json:"owner_pinfl"`
	OwnerFullname            string `json:"owner_fullname"`
	OwnerType                int    `json:"owner_type"`
	OwnerDateBirth           string `json:"owner_date_birth"`
	VehicleID                int    `json:"vehicle_id"`
	VehiclePlateNumber       string `json:"vehicle_plate_number"`
	VehicleModel             string `json:"vehicle_model"`
	VehicleColor             string `json:"vehicle_color"`
	VehicleRegistrationDate  string `json:"vehicle_registration_date"`
	IibPermitInfo            string `json:"iib_permit_info"`
	Division                 string `json:"division"`
	GarovPermitInfo          string `json:"garov_permit_info"`
	VehicleMadeYear          int    `json:"vehicle_made_year"`
	VehicleType              int    `json:"vehicle_type"`
	VehicleKuzov             string `json:"vehicle_kuzov"`
	VehicleFullWeight        int    `json:"vehicle_full_weight"`
	VehicleEmptyWeight       int    `json:"vehicle_empty_weight"`
	VehicleMotor             string `json:"vehicle_motor"`
	VehicleFuelType          int    `json:"vehicle_fuel_type"`
	VehicleSeats             int    `json:"vehicle_seats"`
	VehicleStands            int    `json:"vehicle_stands"`
	Inspection               string `json:"inspection"`
	VehicleTexpassportSeria  string `json:"vehicle_texpassport_seria"`
	VehicleTexpassportNumber string `json:"vehicle_texpassport_number"`
	BodyTypeName             string `json:"body_type_name"`
	VehicleShassi            string `json:"vehicle_shassi"`
	VehiclePower             int    `json:"vehicle_power"`
	DateSchetSpravka         string `json:"date_schet_spravka"`
	TuningPermit             string `json:"tuning_permit"`
	TuningGivenDate          string `json:"tuning_given_date"`
	TuningIssueDate          string `json:"tuning_issue_date"`
	PrevPnfl                 string `json:"prev_pnfl"`
	PrevOwner                string `json:"prev_owner"`
	PrevOwnerType            string `json:"prev_owner_type"`
	PrevPlateNumber          string `json:"prev_plate_number"`
	PrevTexpasportSery       string `json:"prev_texpasport_seria"`
	PrevTexpasportNumber     string `json:"prev_texpasport_number"`
	State                    string `json:"state"`
}

type assessmentDataWithGovInfo struct {
	AssessmentID string           `json:"assessment_id"`
	CBID         int              `json:"cbid"`
	Transport    appTransportInfo `json:"transport_info"`
	GovUzData    govUzData        `json:"gov_uz_data"`
}

// getReportData godoc
// @Router /assessment/data [GET]
// @Summary Get Assessment Data
// @Description API To Get Assessment Data
// @Tags Outsource Integration
// @Produce json
// @Param cbid query string true "cbid"
// @Param texPassportSeria query string true "tex_passport_seria"
// @Param texPassportNumber query string true "tex_passport_number"
// @Success 201 {object} Response{data=assessmentDataWithGovInfo}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getAssessmentData() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result assessmentDataWithGovInfo
		)
		cbid := c.Query("cbid")
		if cbid == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)

			return
		}
		texPassportSeria := c.Query("texPassportSeria")
		if texPassportSeria == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)

			return
		}
		texPassportNumber := c.Query("texPassportNumber")
		if texPassportNumber == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)

			return
		}
		resp, err := s.assessmentUC.GetAssessmentData(c, cbid, texPassportSeria, texPassportNumber)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = assessmentDataWithGovInfo{
			AssessmentID: resp.AssessmentID,
			CBID:         resp.CBID,
			Transport: appTransportInfo{
				TransportModel:       resp.Transport.Model,
				TransportName:        resp.Transport.Name,
				TransportPosition:    resp.Transport.Position,
				TransportYear:        resp.Transport.Year,
				TransportSpeedometer: resp.Transport.Speedometer,
				TransportCondition:   resp.Transport.Condition,
				TransportFuel:        resp.Transport.Fuel,
				TransportPrice:       resp.Transport.Price,
				TransportSellPrice:   resp.Transport.SellPrice,
			},
			GovUzData: govUzData{
				OwnerPinfl:               resp.GovUzStore.OwnerPinfl,
				OwnerFullname:            resp.GovUzStore.OwnerFullname,
				OwnerType:                resp.GovUzStore.OwnerType,
				OwnerDateBirth:           resp.GovUzStore.OwnerDateBirth,
				VehicleID:                resp.GovUzStore.VehicleID,
				VehiclePlateNumber:       resp.GovUzStore.VehiclePlateNumber,
				VehicleModel:             resp.GovUzStore.VehicleModel,
				VehicleColor:             resp.GovUzStore.VehicleColor,
				VehicleRegistrationDate:  resp.GovUzStore.VehicleRegistrationDate,
				IibPermitInfo:            resp.GovUzStore.IibPermitInfo,
				Division:                 resp.GovUzStore.Division,
				GarovPermitInfo:          resp.GovUzStore.GarovPermitInfo,
				VehicleMadeYear:          resp.GovUzStore.VehicleMadeYear,
				VehicleType:              resp.GovUzStore.VehicleType,
				VehicleKuzov:             resp.GovUzStore.VehicleKuzov,
				VehicleFullWeight:        resp.GovUzStore.VehicleFullWeight,
				VehicleEmptyWeight:       resp.GovUzStore.VehicleEmptyWeight,
				VehicleMotor:             resp.GovUzStore.VehicleMotor,
				VehicleFuelType:          resp.GovUzStore.VehicleFuelType,
				VehicleSeats:             resp.GovUzStore.VehicleSeats,
				VehicleStands:            resp.GovUzStore.VehicleStands,
				Inspection:               resp.GovUzStore.Inspection,
				VehicleTexpassportSeria:  resp.GovUzStore.VehicleTexpassportSeria,
				VehicleTexpassportNumber: resp.GovUzStore.VehicleTexpassportNumber,
				BodyTypeName:             resp.GovUzStore.BodyTypeName,
				VehicleShassi:            resp.GovUzStore.VehicleShassi,
				VehiclePower:             resp.GovUzStore.VehiclePower,
				DateSchetSpravka:         resp.GovUzStore.DateSchetSpravka,
				TuningPermit:             resp.GovUzStore.TuningPermit,
				TuningGivenDate:          resp.GovUzStore.TuningGivenDate,
				TuningIssueDate:          resp.GovUzStore.TuningIssueDate,
				PrevPnfl:                 resp.GovUzStore.PrevPnfl,
				PrevOwner:                resp.GovUzStore.PrevOwner,
				PrevOwnerType:            resp.GovUzStore.PrevOwnerType,
				PrevPlateNumber:          resp.GovUzStore.PrevPlateNumber,
				PrevTexpasportSery:       resp.GovUzStore.PrevTexpasportSery,
				PrevTexpasportNumber:     resp.GovUzStore.PrevTexpasportNumber,
				State:                    resp.GovUzStore.State,
			},
		}

		Return(c, result, nil)
	}
}

type govData struct {
	VehicleKuzov       string `json:"vehicle_kuzov"`
	OwnerPinfl         string `json:"owner_pinfl"`
	OwnerFullname      string `json:"owner_fullname"`
	VehicleModel       string `json:"vehicle_model"`
	VehicleColor       string `json:"vehicle_color"`
	VehicleMadeYear    int    `json:"vehicle_made_year"`
	VehicleMotor       string `json:"vehicle_motor"`
	VehicleFullWeight  int    `json:"vehicle_full_weight"`
	VehicleEmptyWeight int    `json:"vehicle_empty_weight"`
	VehiclePlateNumber string `json:"vehicle_plate_number"`
	PassportNumber     string `json:"passport_number"`
	PassportSeria      string `json:"passport_seria"`
	VehiclePower       int    `json:"vehicle_power"`
	VehicleShassi      string `json:"vehicle_shassi"`
}

// updateGovData godoc
// @Router /gov/update [PUT]
// @Summary Updated GovUz Data
// @Description Updated GovUz Data with AssessmentID and GovData
// @Tags Both Device
// @Produce json
// @Param token header string true "token"
// @Param assessment_id query string true "assessment_id"
// @Param request body govData true "Gov Data"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateGovData() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   govData
			result Result
		)

		assessmentID := c.Query("assessment_id")
		if assessmentID == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)
			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, result, errs.ErrAssessmentIDFormat)
			return
		}

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		request := domain.GovUzStore{
			OwnerFullname:            data.OwnerFullname,
			OwnerPinfl:               data.OwnerPinfl,
			VehicleMadeYear:          data.VehicleMadeYear,
			VehiclePlateNumber:       data.VehiclePlateNumber,
			VehicleModel:             data.VehicleModel,
			VehicleColor:             data.VehicleColor,
			VehicleKuzov:             data.VehicleKuzov,
			VehicleFullWeight:        data.VehicleFullWeight,
			VehicleEmptyWeight:       data.VehicleEmptyWeight,
			VehicleMotor:             data.VehicleMotor,
			VehicleTexpassportNumber: data.PassportNumber,
			VehicleTexpassportSeria:  data.PassportSeria,
			VehiclePower:             data.VehiclePower,
			VehicleShassi:            data.VehicleShassi,
		}
		resp, err := s.assessmentUC.UpdateGovData(c, assessmentID, request)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// deleteAssessment godoc
// @Router /assessment/delete [DELETE]
// @Summary Delete Assessment
// @Description API Delete Assessment
// @Tags Web
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) deleteAssessment() gin.HandlerFunc {
	return func(c *gin.Context) {

		ID := c.Query("id")
		if ID == "" {
			Return(c, Result{}, errs.ErrAssessmentIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, Result{}, errs.ErrAssessmentIDFormat)
			return

		}

		resp, err := s.assessmentUC.DeleteAssessment(c, ID)
		if err != nil {
			Return(c, Result{}, err)
			return
		}

		Return(c, resp, nil)
	}
}
