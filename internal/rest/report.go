package rest

import (
	"fmt"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// assessmentReport godoc
// @Router /generate/report [POST]
// @Summary Create a new PDF Report for Assessment
// @Description API To Create a new PDF Report for Assessment
// @Tags Web
// @Produce json
// @Param token header string true "Access token"
// @Param assessmentID query string true "Assessment ID"
// @Success 201 {object} Response{data=models.Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) assessmentReport() gin.HandlerFunc {
	return func(c *gin.Context) {

		assessmentID := c.Query("assessmentID")
		if assessmentID == "" {
			Return(c, nil, errs.ErrAssessmentIDEmpty)
			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, nil, errs.ErrAssessmentIDFormat)
			return
		}

		resp, err := s.reportUC.AssessmentReport(c, assessmentID)
		if err != nil {
			Return(c, nil, err)
			return
		}

		currentDateTime := time.Now().Format("2006-01-02_15-04")
		filename := fmt.Sprintf("%s.pdf", currentDateTime)

		c.Writer.Header().Set("Content-Disposition", "attachment; filename="+filename)
		c.Writer.Header().Set("Content-Type", "application/pdf")
		c.Data(http.StatusOK, "application/pdf", resp)

		Return(c, resp, nil)
	}
}
