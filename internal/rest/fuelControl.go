package rest

import (
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"

	"github.com/gin-gonic/gin"
)

type fuelControlParams struct {
	FuelID    string `json:"fuel_id"`
	Fuel      string `json:"fuel"`
	Price     int    `json:"price"`
	CreatedAt string `json:"created_at"`
}

type fuelControlUpdate struct {
	FuelID string `json:"fuel_id"`
	Price  int    `json:"price"`
}

type fuelControlStore struct {
	NameID string `json:"name_id"`
}

type fuelControls struct {
	ID           string              `json:"id"`
	NameID       string              `json:"name_id"`
	FuelControls []fuelControlParams `json:"fuel_controls"`
}

// getFuels godoc
// @Router /fuel/control/all [GET]
// @Summary Get a FuelControl
// @Description API Get a FuelControl
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param name_id query string true "FuelControl -> name_id"
// @Success 201 {object} Response{data=fuelControls}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getFuels() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			response fuelControls
		)
		nameID := c.Query("name_id")
		if nameID == "" {
			Return(c, response, errs.ErrNameIDEmpty)
			return
		}

		if !utils.IsValidUUID(nameID) {
			Return(c, response, errs.ErrNameIDFormat)
			return
		}

		info, err := s.fuelControlUC.GetFuels(c, nameID)
		if err != nil {
			Return(c, response, err)
			return
		}

		for _, fuelControl := range info.FuelControlParams {
			value := fuelControlParams{
				FuelID:    fuelControl.FuelID,
				Fuel:      fuelControl.Fuel,
				Price:     fuelControl.Price,
				CreatedAt: fuelControl.CreatedAt,
			}
			response.FuelControls = append(response.FuelControls, value)
		}
		response.ID = info.ID
		response.NameID = info.NameID

		Return(c, response, nil)
	}
}

// deleteFuelControl godoc
// @Router /fuel/control/delete [DELETE]
// @Summary Delete a FuelControl
// @Description API Delete a FuelControl
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param name_id query string true "name_id"
// @Param fuel_id query string true "fuel_id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) deleteFuelControl() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			response Result
		)
		nameID := c.Query("name_id")
		if nameID == "" {
			Return(c, response, errs.ErrNameIDEmpty)
			return
		}

		if !utils.IsValidUUID(nameID) {
			Return(c, response, errs.ErrNameIDFormat)
			return
		}

		fuelID := c.Query("fuel_id")
		if fuelID == "" {
			Return(c, response, errs.ErrFuelIDEmpty)
			return
		}

		if !utils.IsValidUUID(fuelID) {
			Return(c, response, errs.ErrFuelIDFormat)
			return
		}

		request := domain.FuelControlParams{
			FuelID: fuelID,
		}

		result, err := s.fuelControlUC.DeleteFuelControl(c, nameID, request)
		if err != nil {
			Return(c, response, err)
			return
		}

		response.Result = result.Result

		Return(c, response, nil)
	}
}

// updateFuelControl godoc
// @Router /fuel/control/update [PUT]
// @Summary Update a FuelControl
// @Description API Update a FuelControl
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param name_id query string true "FuelControl -> name_id"
// @Param request body fuelControlUpdate true "FuelControl -> details"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateFuelControl() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			response Result
			data     fuelControlUpdate
		)
		nameID := c.Query("name_id")
		if nameID == "" {
			Return(c, response, errs.ErrFuelIDEmpty)
			return
		}

		if !utils.IsValidUUID(nameID) {
			Return(c, response, errs.ErrFuelIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, response, errs.ErrValidation)
			return
		}
		request := domain.FuelControlParams{
			FuelID: data.FuelID,
			Price:  data.Price,
		}

		result, err := s.fuelControlUC.UpdateFuelControl(c, nameID, request)
		if err != nil {
			Return(c, response, err)
			return
		}

		response.Result = result.Result

		Return(c, response, nil)
	}
}

// storeFuelControl godoc
// @Router /fuel/control/store [POST]
// @Summary Create a FuelControl
// @Description API Create a FuelControl
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param request body fuelControlStore true "FuelControl -> details"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) storeFuelControl() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			request  domain.FuelControl
			response Result
			data     fuelControlStore
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, response, errs.ErrValidation)
			return
		}
		request = domain.FuelControl{
			NameID: data.NameID,
		}

		if _, err := s.fuelControlUC.StoreFuelControl(c, request); err != nil {
			Return(c, response, err)
			return
		}

		response.Result = true

		Return(c, response, nil)
	}
}
