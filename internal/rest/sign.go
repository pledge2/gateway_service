package rest

import (
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"
	"net/mail"
	"regexp"
	"strings"
	"unicode"

	"github.com/gin-gonic/gin"
)

type checkCbid struct {
	CBID       int    `json:"cbid" example:"44444" binding:"required"`
	DeviceType string `json:"device_type" example:"web / ios / android" binding:"required"`
	DeviceNum  string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265" binding:"required"`
}

// checkCbid godoc
// @Router /check/cbid [POST]
// @Summary Check cbid isValid
// @Description Check CBID
// @Tags Both Device
// @Produce json
// @Param request body checkCbid true "Validation code"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response
// @Failure 500 {object} Response
func (s *Server) checkCbid() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   checkCbid
			result Result
		)

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, nil, errs.ErrValidation)
			return
		}
		if data.CBID < 1 {
			Return(c, result, errs.ErrCbidFormat)
			return
		}

		if data.DeviceNum == "" {
			Return(c, result, errs.ErrDeviceFormat)
			return

		}

		if data.DeviceType == "" {
			Return(c, result, errs.ErrDeviceFormat)
			return

		}

		req := domain.CheckCbid{
			CBID: domain.CBID{
				CBID: data.CBID,
			},
			Device: domain.Device{
				DeviceNum:  data.DeviceNum,
				DeviceType: data.DeviceType,
			},
		}

		resp, err := s.authorizationUC.CheckCBID(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type changePassword struct {
	Password   string `json:"password" example:"44444" binding:"required"`
	RePassword string `json:"re_password" example:"44444" binding:"required"`
	CBID       int    `json:"cbid" example:"44444"`
	DeviceType string `json:"device_type" example:"web / ios / android"`
	DeviceNum  string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265"`
}

// changePassword godoc
// @Router /change/password [POST]
// @Summary Change Password By Cbid
// @Description Change Password
// @Tags Both Device
// @Produce json
// @Param request body changePassword true "Change Password"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response
// @Failure 500 {object} Response
func (s *Server) changePassword() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   changePassword
			result Result
		)

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		if data.Password != data.RePassword {
			Return(c, result, errs.ErrPasswordNotSame)
			return
		}

		if !isValidPass(data.Password) {
			Return(c, result, errs.ErrPasswordFormat)
			return
		}

		if data.CBID < 1 {
			Return(c, result, errs.ErrCbidFormat)
			return
		}

		if data.DeviceNum == "" {
			Return(c, result, errs.ErrDeviceFormat)
			return

		}

		if data.DeviceType == "" {
			Return(c, result, errs.ErrDeviceFormat)
			return

		}

		req := domain.ChangePassword{
			Password: domain.Password{
				Password:   data.Password,
				RePassword: data.RePassword,
			},
			CBID: domain.CBID{
				CBID: data.CBID,
			},
			Device: domain.Device{
				DeviceNum:  data.DeviceNum,
				DeviceType: data.DeviceType,
			},
		}

		resp, err := s.authorizationUC.ChangePassword(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type checkCode struct {
	Code       string `json:"code" example:"7656" binding:"required"`
	CBID       int    `json:"cbid" example:"44444" binding:"required"`
	DeviceType string `json:"device_type" example:"web / ios / android" binding:"required"`
	DeviceNum  string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265" binding:"required"`
}

// checkSMS godoc
// @Router /check/code [POST]
// @Summary Check code which is sent to Mail
// @Description Check Mail Code
// @Tags Both Device
// @Produce json
// @Param request body checkCode true "Validation code"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response
// @Failure 500 {object} Response
func (s *Server) checkCode() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   checkCode
			result Result
		)

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, nil, errs.ErrValidation)
			return
		}

		if data.CBID < 1 {
			Return(c, result, errs.ErrCbidFormat)
			return
		}

		if data.DeviceNum == "" {
			Return(c, result, errs.ErrDeviceFormat)
			return
		}

		if data.DeviceType == "" {
			Return(c, result, errs.ErrDeviceFormat)
			return
		}

		req := domain.CheckCode{
			Code: data.Code,
			CBID: domain.CBID{
				CBID: data.CBID,
			},
			Device: domain.Device{
				DeviceNum:  data.DeviceNum,
				DeviceType: data.DeviceType,
			},
		}

		resp, err := s.authorizationUC.CheckCode(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type resendCode struct {
	CBID       int    `json:"cbid" example:"44444" binding:"required"`
	DeviceType string `json:"device_type" example:"web / ios / android"`
	DeviceNum  string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265"`
}

// resendCode godoc
// @Router /resend/code [POST]
// @Summary Resend code to Mail
// @Description Resend Mail Code
// @Tags Both Device
// @Produce json
// @Param request body resendCode true "Validation code"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response
// @Failure 500 {object} Response
func (s *Server) reSendCode() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   resendCode
			result Result
		)

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		if data.CBID < 1 {
			Return(c, result, errs.ErrCbidFormat)
			return
		}

		if data.DeviceNum == "" {
			Return(c, result, errs.ErrDeviceFormat)
			return

		}

		if data.DeviceType == "" {
			Return(c, result, errs.ErrDeviceFormat)
			return

		}

		req := domain.ResendCode{
			CBID: domain.CBID{
				CBID: data.CBID,
			},
			Device: domain.Device{
				DeviceNum:  data.DeviceNum,
				DeviceType: data.DeviceType,
			},
		}

		resp, err := s.authorizationUC.ResendCode(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type signUp struct {
	Email      string `json:"e_mail" example:"@hamkorbank.uz" binding:"required,email"`
	Password   string `json:"password" example:"password" binding:"required,min=8"`
	RePassword string `json:"re_password" example:"re_password" binding:"required,min=8"`
	CBID       int    `json:"cbid" example:"44444" binding:"required"`
	DeviceType string `json:"device_type" example:"web / ios / android" binding:"required,min=3"`
	DeviceNum  string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265" binding:"required,min=7"`
}

// signUpByMail godoc
// @Router /sign/up [POST]
// @Summary Registration
// @Description Registration
// @Tags Both Device
// @Produce json
// @Param request body signUp true "User Info"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
func (s *Server) signUp() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   signUp
			result Result
		)
		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		if err := data.Validate(); err != nil {
			Return(c, result, err)
			return
		}

		if !isValidPass(data.Password) {
			Return(c, result, errs.ErrPasswordFormat)
			return
		}

		if data.Password != data.RePassword {
			Return(c, result, errs.ErrPasswordNotSame)
			return
		}

		if isValid(data.Email) {
			checkHamkor := strings.Split(data.Email, "@")
			if checkHamkor[1] != "hamkorbank.uz" {
				Return(c, result, errs.ErrBankEmail)
				return
			}
		} else {
			Return(c, result, errs.ErrEmailFormat)
			return
		}

		request := domain.SignUp{
			Email: data.Email,
			CBID: domain.CBID{
				CBID: data.CBID,
			},
			Password: domain.Password{
				Password:   data.Password,
				RePassword: data.RePassword,
			},
			Device: domain.Device{
				DeviceType: data.DeviceType,
				DeviceNum:  data.DeviceNum,
			},
		}

		info, err := s.authorizationUC.SignUp(c, request)
		if err != nil {
			Return(c, result, err)
			return
		}
		result.Result = info.Result

		Return(c, result, nil)
	}
}

type signIn struct {
	Password   string `json:"password" example:"password" binding:"required"`
	CBID       int    `json:"cbid" example:"44444" binding:"required"`
	DeviceType string `json:"device_type" example:"web / ios / android" binding:"required"`
	DeviceNum  string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265" binding:"required"`
}

// signInByMail godoc
// @Router /sign/in [POST]
// @Summary Login
// @Description Login method
// @Tags Both Device
// @Produce json
// @Param request body signIn true "User Info"
// @Success 200 {object} Response{data=tokenResponse}
// @Failure 400 {object} Response{data=Result}
// @Failure 500 {object} Response{data=Result}
func (s *Server) signIn() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   signIn
			result tokenResponse
		)

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		if data.CBID < 1 {
			Return(c, result, errs.ErrCbidFormat)
			return
		}

		if !isValidPass(data.Password) {
			Return(c, result, errs.ErrPasswordFormat)
			return
		}

		if data.DeviceNum == "" {
			Return(c, result, errs.ErrDeviceFormat)
			return

		}

		if data.DeviceType == "" {
			Return(c, result, errs.ErrDeviceFormat)
			return

		}

		req := domain.SignIn{
			CBID: domain.CBID{
				CBID: data.CBID,
			},
			Password: domain.Password{
				Password: data.Password,
			},
			Device: domain.Device{
				DeviceNum:  data.DeviceNum,
				DeviceType: data.DeviceType,
			}}

		response, err := s.authorizationUC.SignIn(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = tokenResponse{
			AccessToken:  response.AccessToken,
			RefreshToken: response.RefreshToken,
		}

		Return(c, result, nil)
	}
}

func isValid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

func isValidPass(pass string) bool {
	var lower, upper, digit bool
	for _, p := range pass {
		if unicode.IsDigit(p) {
			digit = true
		}
		if unicode.IsLower(p) {
			lower = true
		}
		if unicode.IsUpper(p) {
			upper = true
		}
		// if unicode.IsSymbol(p) {
		// 	symbol = true
		// }
		// if unicode.IsPunct(p) {
		// 	punc = true
		// }
	}
	if lower && upper && digit {
		return true
	} else {
		return false
	}
}

func (u *signUp) Validate() error {

	if u.CBID < 1 {
		return errs.ErrCbidFormat
	}
	var validNum = regexp.MustCompile(`[a-z0-9]$`)
	switch u.DeviceType {
	case "web":
		if utils.IsValidUUID(u.DeviceNum) {
			return nil
		} else {
			return errs.ErrDeviceFormat
		}
		// if net.ParseIP(u.DeviceNum) == nil {
		// 	return errs.ErrDeviceFormat
		// }
		// return nil
	case "android":
		if validNum.MatchString(u.DeviceNum) {
			return nil
		} else {
			return errs.ErrDeviceFormat
		}

	case "ios":
		if utils.IsValidUUID(u.DeviceNum) {
			return nil
		} else {
			return errs.ErrDeviceFormat
		}
	default:
		return errs.ErrDeviceFormat
	}
}
