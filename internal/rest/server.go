package rest

import (
	"context"
	"gateway_service/internal/config"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
	"mime/multipart"

	"github.com/dgrijalva/jwt-go"

	"net/http"

	"github.com/gin-gonic/gin"
)

type analogUseCase interface {
	StoreAnalog(ctx context.Context, data domain.Analog) (domain.IdResponse, error)
	AnalogFindView(ctx context.Context, id string) (domain.GetAnalog, error)
	AnalogFindOne(ctx context.Context, id string) (domain.GetAnalog, error)
	AnalogFindList(ctx context.Context, productID, typeID string, limit, page int) (domain.AnalogListResp, error)
	AnalogUpdate(ctx context.Context, id string, req domain.Analog) (domain.Result, error)
	AnalogUpdateStatus(ctx context.Context, status domain.UpdateAnalogStatus) (domain.Result, error)
	AnalogDelete(ctx context.Context, id string) (domain.Result, error)
	AnalogSearch(ctx context.Context, analogID, typeID, modelID, nameID, positionID, fromYear, toYear,
		fuel, fromSpeedometer, toSpeedometer, validity, fromPrice, toPrice, activeValue,
		fromDate, toDate, user, limit, page string) (domain.AnalogListResp, error)
	ChangeAnalogPdfFolder(ctx context.Context) (domain.Result, error)
	ChangeAnalogPhotosFolder(ctx context.Context) (domain.Result, error)
	ChangeAssessmentPhotoFolder(ctx context.Context) (domain.AssessmentPhotoFolderResponse, error)
}

type productUseCase interface {
	StoreProduct(ctx context.Context, product domain.StoreProduct) (domain.IdResponse, error)
	FindOneProduct(ctx context.Context, id string) (domain.ProductResponse, error)
	FindAllProduct(ctx context.Context) ([]domain.ProductResponse, error)
	UpdateProductPhoto(ctx context.Context, id string, product domain.UpdateProductPhoto) (domain.Result, error)
	ProductUpdate(ctx context.Context, id string, product domain.UpdateProduct) (domain.Result, error)
	UpdateProductStatus(ctx context.Context, id string, product domain.UpdateProductStatus) (domain.Result, error)
	ProductDelete(ctx context.Context, id string) (domain.Result, error)
	ChangeProductPhotosFolder(ctx context.Context) (domain.Result, error)
}

type minioUseCase interface {
	UploadFile(ctx context.Context, fileHeader *multipart.FileHeader, folder string) (domain.FileUrl, error)
	UploadPhoto(ctx context.Context, fileHeader *multipart.FileHeader, folder, suffix, assessmentID, lat, lon string) (domain.Result, error)
}

type nutanixUseCase interface {
	UploadFile(ctx context.Context, fileHeader *multipart.FileHeader, folder string) (domain.FileName, error)
	UploadPhoto(ctx context.Context, fileHeader *multipart.FileHeader, folder, suffix, nameKey, assessmentID, lat, lon string) (domain.Result, error)
}

type assessmentUseCase interface {
	OfficialAssessment(ctx context.Context, assessment domain.AssessmentStore) (domain.AssessmentResult, error)
	FastAssessment(ctx context.Context, assessment domain.AssessmentStore) (domain.AssessmentResult, error)
	StoreMainInfo(ctx context.Context, assessment domain.AssessmentStore) (domain.IdResponse, error)
	GovCarInfo(ctx context.Context, carInfo domain.TechNumberParams) (domain.GovUzResponse, error)
	GetPhotos(ctx context.Context, assessmentID string) (domain.AssessmentPhotos, error)
	FindAssessment(ctx context.Context, assessmentID string) (domain.Assessment, error)
	FindAssessmentByStatus(ctx context.Context, status, limit, page, cbid, roleCode, branchID, direct int) (domain.AssessmentPagination, error)
	CountStatusAssessment(ctx context.Context, cbid, branchID, roleCode, direct int) ([]domain.AssessmentStatus, error)
	ChangeAssessmentStatus(ctx context.Context, aStatus domain.AssessmentStatus) (domain.Result, error)
	AssessmentAnalogsLists(ctx context.Context, assessmentID string) ([]domain.GetAnalog, error)
	GetAssessmentData(ctx context.Context, cbid string, texPassSeria string, texPassNum string) (domain.GovInfoWithCBID, error)
	UpdateGovData(ctx context.Context, assessmentID string, store domain.GovUzStore) (domain.Result, error)
	DeleteAssessment(ctx context.Context, assessmentID string) (domain.Result, error)
}

type assessmentStatusUseCase interface {
	StoreStatus(ctx context.Context, aStatus domain.AssessmentStatus) (domain.IdResponse, error)
	FindAllStatus(ctx context.Context, role int) ([]domain.AssessmentStatus, error)
}

type appRolesUseCase interface {
	GetAppRolesByPostCode(ctx context.Context, roleID string) ([]domain.PostcodeList, error)
	SearchRolesByPostCode(ctx context.Context, postcode string) (domain.PostcodeList, error)
	GetAppRolesByCbID(ctx context.Context, roleID string) ([]domain.CBIDList, error)
	SearchEmpByCbID(ctx context.Context, cbid int) (domain.CBIDList, error)
	AddPostcodeToRole(ctx context.Context, roleID string, rData domain.PostcodeList) (domain.Result, error)
	DeletePostcodeFromRole(ctx context.Context, roleID string, rData domain.PostcodeList) (domain.Result, error)
	AddCbidToRole(ctx context.Context, id string, cData domain.CBIDList) (domain.Result, error)
	DeleteCbidFromRole(ctx context.Context, id string, cData domain.CBIDList) (domain.Result, error)
}

type authorizationUseCase interface {
	SignUp(ctx context.Context, authData domain.SignUp) (domain.Result, error)
	SignIn(ctx context.Context, authData domain.SignIn) (domain.JwtTokens, error)
	CheckCode(ctx context.Context, codeData domain.CheckCode) (domain.Result, error)
	CheckCBID(ctx context.Context, cbidData domain.CheckCbid) (domain.Result, error)
	ResendCode(ctx context.Context, code domain.ResendCode) (domain.Result, error)
	ChangePassword(ctx context.Context, authData domain.ChangePassword) (domain.Result, error)
}

type employeesUseCase interface {
	GetEmpDataIABS(ctx context.Context) (domain.Result, error)
	FindEmployee(ctx context.Context, cbid int) (domain.EmpInfo, error)
	FindAllEmployees(ctx context.Context, limit, page int) (domain.EmpPagination, error)
	UpdateEmpData(ctx context.Context, id string, empData domain.EmpInfo) (domain.Result, error)
}

type rolesUseCase interface {
	CreateRole(ctx context.Context, roleData domain.UserRole) (domain.IdResponse, error)
	FindOneRole(ctx context.Context, id string) (domain.UserRole, error)
	RoleFindAll(ctx context.Context, roleCode int) ([]domain.UserRole, error)
	UpdateRole(ctx context.Context, id string, usrData domain.UserRole) (domain.Result, error)
	UpdateRoleStatus(ctx context.Context, id string, rStatus domain.UserRole) (domain.Result, error)
	DeleteRole(ctx context.Context, id string) (domain.Result, error)
}

type usersUseCase interface {
	FindUserList(ctx context.Context) ([]domain.UserInfo, error)
	UserInfo(ctx context.Context, cbid int) (domain.UserInfo, error)
	AllUsersList(ctx context.Context, limit, page int) (domain.UserPagination, error)
	UpdateUserEmail(ctx context.Context, id string, uEmail domain.AppUsers) (domain.Result, error)
	DeleteUser(ctx context.Context, id string) (domain.Result, error)
	BlockUser(ctx context.Context, cbid int) (domain.Result, error)
}

type jwtUseCase interface {
	ExtractClaims(tokenString string) (jwt.MapClaims, error)
	GenerateToken(m map[string]interface{}) (string, string, error)
}

type typeUseCase interface {
	TypeStore(ctx context.Context, tData domain.TypeStore) (domain.IdResponse, error)
	TypeFindOne(ctx context.Context, id string) (domain.TypeStore, error)
	TypeFindAll(ctx context.Context) ([]domain.TypeStore, error)
	TypeFindPagination(ctx context.Context, limit, page int) (domain.TypePagination, error)
	TypeUpdate(ctx context.Context, id string, tData domain.TypeStore) (domain.Result, error)
	UpdateTypePhoto(ctx context.Context, id string, tPhoto domain.TypeStore) (domain.Result, error)
	UpdateTypeStatus(ctx context.Context, id string, tPhoto domain.TypeStore) (domain.Result, error)
	TypeDelete(ctx context.Context, id string) (domain.Result, error)
	ChangeTypeFolder(ctx context.Context) (domain.Result, error)
}

type modelUseCase interface {
	ModelStore(ctx context.Context, model domain.Model) (domain.IdResponse, error)
	ModelFindOne(ctx context.Context, id string) (domain.Model, error)
	ModelFindAll(ctx context.Context) ([]domain.Model, error)
	ModelFindPagination(ctx context.Context, limit, page int) (domain.ModelPagination, error)
	ModelFindAllByTypeID(ctx context.Context, typeID string) ([]domain.Model, error)
	ModelFilter(ctx context.Context, typeID, modelID string, limit, page int) (domain.ModelPagination, error)
	ModelUpdate(ctx context.Context, id string, model domain.Model) (domain.Result, error)
	UpdateModelPhoto(ctx context.Context, id string, model domain.Model) (domain.Result, error)
	UpdateModelStatus(ctx context.Context, id string, mData domain.Model) (domain.Result, error)
	ModelDelete(ctx context.Context, id string) (domain.Result, error)
	ChangeModelFolder(ctx context.Context) (domain.Result, error)
}

type nameUseCase interface {
	NameStore(ctx context.Context, name domain.Name) (domain.IdResponse, error)
	NameFindOne(ctx context.Context, id string) (domain.Name, error)
	NameFindAll(ctx context.Context) ([]domain.Name, error)
	NamePagination(ctx context.Context, limit, page int) (domain.NamePagination, error)
	FindAllNameByModelID(ctx context.Context, modelID string) ([]domain.Name, error)
	NameFilter(ctx context.Context, typeID, modelID, nameID string, limit, page int) (domain.NamePagination, error)
	NameUpdate(ctx context.Context, id string, name domain.Name) (domain.Result, error)
	UpdateNamePhoto(ctx context.Context, id string, name domain.Name) (domain.Result, error)
	UpdateNameStatus(ctx context.Context, id string, nStatus domain.Name) (domain.Result, error)
	NameDelete(ctx context.Context, id string) (domain.Result, error)
	ChangeNameFolder(ctx context.Context) (domain.Result, error)
}

type positionUseCase interface {
	PositionStore(ctx context.Context, position domain.Position) (domain.IdResponse, error)
	PositionFindOne(ctx context.Context, id string) (domain.Position, error)
	PositionFindAll(ctx context.Context) ([]domain.Position, error)
	PositionFindPagination(ctx context.Context, limit, page int) (domain.PositionPagination, error)
	PositionFindAllByTypeID(ctx context.Context, nameID string) ([]domain.Position, error)
	PositionFilter(ctx context.Context, typeID, modelID, nameID string, limit, page int) (domain.PositionPagination, error)
	PositionUpdate(ctx context.Context, id string, position domain.Position) (domain.Result, error)
	UpdatePositionStatus(ctx context.Context, id string, position domain.Position) (domain.Result, error)
	PositionDelete(ctx context.Context, id string) (domain.Result, error)
}

type fuelUseCase interface {
	FuelStore(ctx context.Context, fData domain.Fuel) (domain.IdResponse, error)
	FuelFindOne(ctx context.Context, id string) (domain.Fuel, error)
	FuelFindAll(ctx context.Context) ([]domain.Fuel, error)
	FuelUpdate(ctx context.Context, id string, fData domain.Fuel) (domain.Result, error)
	UpdateFuelStatus(ctx context.Context, id string, fData domain.Fuel) (domain.Result, error)
	FuelDelete(ctx context.Context, id string) (domain.Result, error)
}

type validityUseCase interface {
	ValidityStore(ctx context.Context, validity domain.Validity) (domain.IdResponse, error)
	ValidityFindOne(ctx context.Context, id string) (domain.Validity, error)
	FindAllValidity(ctx context.Context) ([]domain.Validity, error)
	ValidityUpdate(ctx context.Context, id string, validity domain.Validity) (domain.Result, error)
	UpdateValidityStatus(ctx context.Context, id string, validity domain.Validity) (domain.Result, error)
	ValidityDelete(ctx context.Context, id string) (domain.Result, error)
}

type discountUseCase interface {
	GetDiscounts(ctx context.Context, nameID string) (domain.Discounts, error)
	UpdateDiscount(ctx context.Context, nameID string, discount domain.DiscountParams) (domain.Result, error)
	StoreDiscounts(ctx context.Context, discount domain.Discount) (domain.Result, error)
	DeleteDiscount(ctx context.Context, nameID string, discount domain.DiscountParams) (domain.Result, error)
}

type specialCarsUseCase interface {
	StoreSpecial(ctx context.Context, tData domain.SpecialCars) (domain.IdResponse, error)
	SpecialFindAll(ctx context.Context) ([]domain.SpecialCars, error)
	UpdateSpecial(ctx context.Context, id string, tData domain.SpecialCars) (domain.Result, error)
	DeleteSpecial(ctx context.Context, id string) (domain.Result, error)
}

type referenceUseCase interface {
	GetAllMethods(ctx context.Context, typeID string) (domain.ReferenceAll, error)
}

type fuelControlUseCase interface {
	GetFuels(ctx context.Context, nameID string) (domain.FuelControls, error)
	UpdateFuelControl(ctx context.Context, nameID string, fuel domain.FuelControlParams) (domain.Result, error)
	StoreFuelControl(ctx context.Context, fuel domain.FuelControl) (domain.Result, error)
	DeleteFuelControl(ctx context.Context, nameID string, fuel domain.FuelControlParams) (domain.Result, error)
}

type reportUseCase interface {
	AssessmentReport(ctx context.Context, assessmentID string) ([]byte, error)
}

type Server struct {
	router *gin.Engine
	log    logger.Logger
	cfg    config.Config

	analogUC  analogUseCase
	productUC productUseCase

	minioUC   minioUseCase
	nutanixUC nutanixUseCase

	assessmentUC       assessmentUseCase
	assessmentStatusUC assessmentStatusUseCase

	appRolesUC      appRolesUseCase
	authorizationUC authorizationUseCase
	empUC           employeesUseCase
	roleUC          rolesUseCase
	usersUC         usersUseCase

	jwtUC jwtUseCase

	typeUC        typeUseCase
	modelUC       modelUseCase
	nameUC        nameUseCase
	positionUC    positionUseCase
	fuelUC        fuelUseCase
	validityUC    validityUseCase
	discountUC    discountUseCase
	specialUC     specialCarsUseCase
	referenceUC   referenceUseCase
	fuelControlUC fuelControlUseCase

	reportUC reportUseCase
}

func New(router *gin.Engine,
	cfg config.Config,
	log logger.Logger,
	analogUC analogUseCase,
	productUC productUseCase,
	minioUC minioUseCase,
	nutanixUC nutanixUseCase,
	assessmentUC assessmentUseCase,
	assessmentStatusUC assessmentStatusUseCase,
	appRolesUC appRolesUseCase,
	authorizationUC authorizationUseCase,
	empUC employeesUseCase,
	roleUC rolesUseCase,
	usersUC usersUseCase,
	jwtUC jwtUseCase,
	typeUC typeUseCase,
	modelUC modelUseCase,
	nameUC nameUseCase,
	positionUC positionUseCase,
	fuelUC fuelUseCase,
	validityUC validityUseCase,
	discountUC discountUseCase,
	specialUC specialCarsUseCase,
	referenceUC referenceUseCase,
	reportUC reportUseCase,
	fuelControlUC fuelControlUseCase,
) *Server {
	srv := &Server{
		router:             router,
		log:                log,
		cfg:                cfg,
		analogUC:           analogUC,
		productUC:          productUC,
		minioUC:            minioUC,
		nutanixUC:          nutanixUC,
		assessmentUC:       assessmentUC,
		assessmentStatusUC: assessmentStatusUC,
		appRolesUC:         appRolesUC,
		authorizationUC:    authorizationUC,
		empUC:              empUC,
		roleUC:             roleUC,
		usersUC:            usersUC,
		jwtUC:              jwtUC,
		typeUC:             typeUC,
		modelUC:            modelUC,
		nameUC:             nameUC,
		positionUC:         positionUC,
		fuelUC:             fuelUC,
		validityUC:         validityUC,
		discountUC:         discountUC,
		specialUC:          specialUC,
		referenceUC:        referenceUC,
		reportUC:           reportUC,
		fuelControlUC:      fuelControlUC,
	}

	srv.Router()

	return srv
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
