package rest

import (
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"
	"strconv"

	"github.com/gin-gonic/gin"
)

// ------------------------------------------------------Type------------------------------------------------------------------------

type typeStore struct {
	Type      string `json:"type" binding:"required"`
	Active    bool   `json:"active"`
	PhotoName string `json:"photo_name"`
}

// typeStore godoc
// @Router /type/store [POST]
// @Summary Create a new Type
// @Description API Create a Type
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param request body typeStore true "Type details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) typeStore() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   typeStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return

		}

		req := domain.TypeStore{
			Type:      data.Type,
			Active:    data.Active,
			PhotoName: data.PhotoName,
		}

		id, err := s.typeUC.TypeStore(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

type typeResponse struct {
	ID        string `json:"id"`
	Type      string `json:"type"`
	Active    bool   `json:"active"`
	PhotoUrl  string `json:"photo_url"`
	CreatedAt string `json:"created_at"`
}

// findType godoc
// @Router /type/find/one [GET]
// @Summary Find a Type
// @Description API Find a Type
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "Type -> id"
// @Success 201 {object} Response{data=typeResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) typeFindOne() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result typeResponse

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrTypeIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrTypeIDFormat)
			return
		}

		data, err := s.typeUC.TypeFindOne(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = typeResponse{
			ID:        data.ID,
			Type:      data.Type,
			Active:    data.Active,
			PhotoUrl:  data.PhotoUrl,
			CreatedAt: data.CreateAt,
		}

		Return(c, result, nil)
	}
}

type typesResponse struct {
	ID          string `json:"id"`
	Type        string `json:"type"`
	TypeKey     string `json:"type_key"`
	FastUrl     string `json:"fast_url"`
	Active      bool   `json:"active"`
	OfficialUrl string `json:"official_url"`
	PhotoUrl    string `json:"photo_url"`
	CreatedAt   string `json:"created_at"`
}

// findAllType godoc
// @Router /type/find/all [GET]
// @Summary Find all Types
// @Description API to find all Types
// @Tags Both Device
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]typesResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllType() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []typesResponse

		data, err := s.typeUC.TypeFindAll(c)
		if err != nil {

			Return(c, result, err)
			return
		}

		for _, val := range data {
			tData := typesResponse{
				ID:          val.ID,
				Type:        val.Type,
				TypeKey:     val.TypeKey,
				FastUrl:     val.FastURL,
				Active:      val.Active,
				OfficialUrl: val.OfficialURL,
				PhotoUrl:    val.PhotoUrl,
				CreatedAt:   val.CreateAt,
			}
			result = append(result, tData)
		}

		Return(c, result, nil)
	}
}

type types struct {
	ID          string `json:"id"`
	TypeKey     string `json:"type_key"`
	Index       int    `json:"index"`
	Type        string `json:"type"`
	Active      bool   `json:"active"`
	FastURL     string `json:"fast_url"`
	OfficialURL string `json:"official_url"`
	PhotoName   string `json:"photo_name"`
	PhotoUrl    string `json:"photo_url"`
	CreateAt    string `json:"created_at"`
	UpdateAt    string `json:"updated_at"`
}

type typePagination struct {
	Type       []types `json:"type"`
	TotalPages int     `json:"total_pages"`
}

// typeFindPagination godoc
// @Router /type/pagination [GET]
// @Summary Find Type With Pagination
// @Description API Find Type With Pagination
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "Access token"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Success 201 {object} Response{data=typePagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) typeFindPagination() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result typePagination

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		data, err := s.typeUC.TypeFindPagination(c, intLimit, intPage)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.TotalPages = data.TotalPages

		for _, val := range data.Type {
			tData := types{
				ID:          val.ID,
				TypeKey:     val.TypeKey,
				Index:       val.Index,
				Type:        val.Type,
				Active:      val.Active,
				FastURL:     val.FastURL,
				OfficialURL: val.OfficialURL,
				PhotoName:   val.PhotoName,
				PhotoUrl:    val.PhotoUrl,
				CreateAt:    val.CreateAt,
				UpdateAt:    val.UpdateAt,
			}
			result.Type = append(result.Type, tData)
		}

		Return(c, result, nil)
	}
}

// typeUpdate godoc
// @Router /type/update [PUT]
// @Summary Update Type
// @Description API to Update Type
// @Tags Admin Page Reference
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body typeStore true "Type data"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) typeUpdate() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   typeStore
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrTypeIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrTypeIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return

		}

		req := domain.TypeStore{
			Type:      data.Type,
			Active:    data.Active,
			PhotoName: data.PhotoName,
		}

		resp, err := s.typeUC.TypeUpdate(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// deleteType godoc
// @Router /type/delete [DELETE]
// @Summary Delete  Type
// @Description API Delete  Type
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) typeDelete() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrTypeIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrTypeIDFormat)
			return
		}

		resp, err := s.typeUC.TypeDelete(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type typePhoto struct {
	PhotoName string `json:"photo_name"`
}

// updatePhotoUrl godoc
// @Router /type/update/photo [PUT]
// @Summary Update Photo Url
// @Description API to Update Photo Url
// @Tags Web
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body typePhoto true "Type photo"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateTypeUrl() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   typePhoto
			result Result
		)

		ID := c.Query("id")

		if ID == "" {
			Return(c, result, errs.ErrTypeIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrTypeIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.TypeStore{
			PhotoName: data.PhotoName,
		}

		resp, err := s.typeUC.UpdateTypePhoto(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type typeStatus struct {
	Active bool `json:"active"`
}

// updateTypeStatus godoc
// @Router /type/update/status [PUT]
// @Summary Update Type Status
// @Description API to Update Type Status
// @Tags Web
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body typeStatus true "Type status"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateTypeStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   typeStatus
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrTypeIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrTypeIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.TypeStore{
			Active: data.Active,
		}

		resp, err := s.typeUC.UpdateTypeStatus(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// ------------------------------------------------------Model------------------------------------------------------------------------

type modelStore struct {
	TypeID    string `json:"type_id" binding:"required"`
	Model     string `json:"model" binding:"required"`
	Active    bool   `json:"active"`
	PhotoName string `json:"photo_name"`
}

// modelStore godoc
// @Router /model/store [POST]
// @Summary Create a new Model
// @Description API Create a Model
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param request body modelStore true "Model details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) modelStore() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   modelStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return

		}

		req := domain.Model{
			TypeID:    data.TypeID,
			Model:     data.Model,
			Active:    data.Active,
			PhotoName: data.PhotoName,
		}

		id, err := s.modelUC.ModelStore(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

type modelResponse struct {
	ID       string `json:"id"`
	Type     string `json:"type"`
	Model    string `json:"model"`
	Active   bool   `json:"active"`
	PhotoUrl string `json:"photo_url"`
	CreateAt string `json:"created_at"`
}

// modelFindOne godoc
// @Router /model/find/one [GET]
// @Summary Find a Model
// @Description API Find a Model
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "Model -> id"
// @Success 201 {object} Response{data=modelResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) modelFindOne() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result modelResponse

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrModelIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrModelIDFormat)
			return
		}

		data, err := s.modelUC.ModelFindOne(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = modelResponse{
			ID:       data.ID,
			Type:     data.TypeID,
			Model:    data.Model,
			Active:   data.Active,
			PhotoUrl: data.PhotoUrl,
			CreateAt: data.CreateAt,
		}

		Return(c, result, nil)
	}
}

type allModels struct {
	ID       string `json:"id"`
	Type     string `json:"type"`
	Model    string `json:"model"`
	PhotoUrl string `json:"photo_url"`
}

// findAllModel godoc
// @Router /model/find/all [GET]
// @Summary Find all Models
// @Description API to find all Models
// @Tags Web
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]allModels}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllModel() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []allModels

		data, err := s.modelUC.ModelFindAll(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			mData := allModels{
				ID:       val.ID,
				Type:     val.TypeID,
				Model:    val.Model,
				PhotoUrl: val.PhotoUrl,
			}
			result = append(result, mData)
		}

		Return(c, result, nil)
	}
}

type model struct {
	ID        string `json:"id"`
	Index     int    `json:"index"`
	TypeID    string `json:"type_id"`
	Type      string `json:"type"`
	Model     string `json:"model"`
	Active    bool   `json:"active"`
	PhotoName string `json:"photo_name"`
	PhotoUrl  string `json:"photo_url"`
	CreateAt  string `json:"created_at"`
}

type modelPagination struct {
	Model      []model `json:"model"`
	TotalPages int     `json:"total_pages"`
}

// modelFindPagination godoc
// @Router /model/pagination [GET]
// @Summary Find Model With Pagination
// @Description API Model With Pagination
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "Access token"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Success 201 {object} Response{data=modelPagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) modelFindPagination() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result modelPagination

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		data, err := s.modelUC.ModelFindPagination(c, intLimit, intPage)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.TotalPages = data.TotalPages

		for _, val := range data.Model {
			mData := model{
				ID:        val.ID,
				Index:     val.Index,
				TypeID:    val.TypeID,
				Type:      val.Type,
				Model:     val.Model,
				Active:    val.Active,
				PhotoName: val.PhotoName,
				PhotoUrl:  val.PhotoUrl,
				CreateAt:  val.CreateAt,
			}
			result.Model = append(result.Model, mData)
		}

		Return(c, result, nil)
	}
}

type modelList struct {
	ID    string `json:"id"`
	Type  string `json:"type"`
	Model string `json:"model"`
}

// findAllModelByTypeID godoc
// @Router /model/find/all/type [GET]
// @Summary Find a Model
// @Description API Find a Model
// @Tags Web
// @Produce json
// @Param token header string true "token"
// @Param type_id query string true "Type -> id"
// @Success 201 {object} Response{data=[]modelList}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllModelByTypeID() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []modelList

		typeID := c.Query("type_id")
		if typeID == "" {
			Return(c, result, errs.ErrTypeIDEmpty)
			return
		}

		if !utils.IsValidUUID(typeID) {
			Return(c, result, errs.ErrTypeIDFormat)
			return
		}

		data, err := s.modelUC.ModelFindAllByTypeID(c, typeID)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			mData := modelList{
				ID:    val.ID,
				Type:  val.TypeID,
				Model: val.Model,
			}
			result = append(result, mData)
		}

		Return(c, result, nil)
	}
}

// modelFilter godoc
// @Router /model/filter [GET]
// @Summary Filter By model and type
// @Description Filter By model and type
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Param type_id query string false "Type -> id"
// @Param model_id query string false "Model -> id"
// @Success 201 {object} Response{data=modelPagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) modelFilter() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result modelPagination

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		typeID := c.Query("type_id")
		if typeID != "" {
			if !utils.IsValidUUID(typeID) {
				Return(c, result, errs.ErrTypeIDFormat)
				return
			}
		}

		modelID := c.Query("model_id")
		if modelID != "" {
			if !utils.IsValidUUID(modelID) {
				Return(c, result, errs.ErrModelIDFormat)
				return
			}
		}

		data, err := s.modelUC.ModelFilter(c, typeID, modelID, intLimit, intPage)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.TotalPages = data.TotalPages

		for _, val := range data.Model {
			mData := model{
				ID:        val.ID,
				Index:     val.Index,
				TypeID:    val.TypeID,
				Type:      val.Type,
				Model:     val.Model,
				Active:    val.Active,
				PhotoName: val.PhotoName,
				PhotoUrl:  val.PhotoUrl,
				CreateAt:  val.CreateAt,
			}
			result.Model = append(result.Model, mData)
		}

		Return(c, result, nil)
	}
}

// modelUpdate godoc
// @Router /model/update [PUT]
// @Summary Update Model
// @Description API to Update Model
// @Tags Admin Page Reference
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body modelStore true "Model data"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) modelUpdate() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   modelStore
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrModelIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrModelIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Model{
			TypeID:    data.TypeID,
			Model:     data.Model,
			Active:    data.Active,
			PhotoName: data.PhotoName,
		}

		resp, err := s.modelUC.ModelUpdate(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type modelPhoto struct {
	PhotoName string `json:"photo_name"`
}

// updatePhotoUrl godoc
// @Router /model/update/photo [PUT]
// @Summary Update Photo Url
// @Description API to Update Photo Url
// @Tags Web
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body modelPhoto true "Model photo"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateModelUrl() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   modelPhoto
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrModelIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrModelIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Model{
			PhotoName: data.PhotoName,
		}

		resp, err := s.modelUC.UpdateModelPhoto(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type modelStatus struct {
	Active bool `json:"active"`
}

// updateModelStatus godoc
// @Router /model/update/status [PUT]
// @Summary Update Model Status
// @Description API to Update Model Status
// @Tags Web
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body modelStatus true "Model status"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateModelStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   modelStatus
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrModelIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrModelIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Model{
			Active: data.Active,
		}

		resp, err := s.modelUC.UpdateModelStatus(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// deleteType godoc
// @Router /model/delete [DELETE]
// @Summary Delete  Type
// @Description API Delete  Type
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) modelDelete() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrModelIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrModelIDFormat)
			return
		}

		resp, err := s.modelUC.ModelDelete(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// ------------------------------------------------------Name------------------------------------------------------------------------

type nameStore struct {
	ModelID   string `json:"model_id" binding:"required"`
	Name      string `json:"name" binding:"required"`
	TypeKey   string `json:"type_key" binding:"required"`
	NameKey   string `json:"name_key"`
	PhotoName string `json:"photo_name"`
	Active    bool   `json:"active"`
}

// nameStore godoc
// @Router /name/store [POST]
// @Summary Create a new Name
// @Description API Create a Name
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param request body nameStore true "Name details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) nameStore() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   nameStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return

		}

		if utils.IsSpecial(data.TypeKey) {
			if data.NameKey == "" {
				Return(c, result, errs.New("Transport guruhlari bo'sh kelmoqda"))
				return
			}
		}

		req := domain.Name{
			ModelID:   data.ModelID,
			Name:      data.Name,
			TypeKey:   data.TypeKey,
			NameKey:   data.NameKey,
			PhotoName: data.PhotoName,
			Active:    data.Active,
		}

		id, err := s.nameUC.NameStore(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

type nameResponse struct {
	ID             string `json:"id"`
	Model          string `json:"model"`
	Name           string `json:"name"`
	TransportGroup string `json:"transport_group"`
	PhotoURL       string `json:"photo_url"`
	Active         bool   `json:"active"`
	CreateAt       string `json:"created_at"`
}

// nameFindOne godoc
// @Router /name/find/one [GET]
// @Summary Find Name
// @Description API Find a Name
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "Name -> id"
// @Success 201 {object} Response{data=nameResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) nameFindOne() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result nameResponse

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrNameIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrNameIDFormat)
			return
		}

		data, err := s.nameUC.NameFindOne(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = nameResponse{
			ID:             data.ID,
			Model:          data.ModelID,
			Name:           data.Name,
			TransportGroup: data.TransportGroup,
			PhotoURL:       data.PhotoURL,
			Active:         data.Active,
			CreateAt:       data.CreateAt,
		}

		Return(c, result, nil)
	}
}

type namesResponse struct {
	ID       string `json:"id"`
	Model    string `json:"model"`
	Name     string `json:"name"`
	NameKey  string `json:"name_key"`
	PhotoURL string `json:"photo_url"`
}

// findAllName godoc
// @Router /name/find/all [GET]
// @Summary Find all Names
// @Description API to find all Names
// @Tags Web
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]namesResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllName() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []namesResponse

		info, err := s.nameUC.NameFindAll(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range info {
			value := namesResponse{
				ID:       val.ID,
				Model:    val.ModelID,
				Name:     val.Name,
				NameKey:  val.NameKey,
				PhotoURL: val.PhotoURL,
			}
			result = append(result, value)
		}

		Return(c, result, nil)
	}
}

type name struct {
	ID             string `json:"id"`
	Index          int    `json:"index"`
	Type           string `json:"type"`
	ModelID        string `json:"model_id"`
	Model          string `json:"model"`
	Name           string `json:"name"`
	NameKey        string `json:"name_key"`
	TypeKey        string `json:"type_key"`
	TransportGroup string `json:"transport_group"`
	PhotoName      string `json:"photo_name"`
	PhotoURL       string `json:"photo_url"`
	Active         bool   `json:"active"`
	CreateAt       string `json:"created_at"`
}

type namePagination struct {
	Name       []name `json:"name"`
	TotalPages int    `json:"total_pages"`
}

// nameFindPagination godoc
// @Router /name/pagination [GET]
// @Summary Find Name With Pagination
// @Description API Name With Pagination
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "Access token"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Success 201 {object} Response{data=namePagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) nameFindPagination() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result namePagination

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		data, err := s.nameUC.NamePagination(c, intLimit, intPage)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.TotalPages = data.TotalPages

		for _, val := range data.Name {
			nData := name{
				ID:        val.ID,
				Index:     val.Index,
				Type:      val.Type,
				ModelID:   val.ModelID,
				Model:     val.Model,
				Name:      val.Name,
				PhotoName: val.PhotoName,
				PhotoURL:  val.PhotoURL,
				Active:    val.Active,
				CreateAt:  val.CreateAt,
			}
			result.Name = append(result.Name, nData)
		}

		Return(c, result, nil)
	}
}

type nameList struct {
	ID      string `json:"id"`
	Model   string `json:"model"`
	Name    string `json:"name"`
	NameKey string `json:"name_key"`
}

// findAllNameByModelID godoc
// @Router /name/find/all/model [GET]
// @Summary Find All Names
// @Description API Find All Names
// @Tags Web
// @Produce json
// @Param token header string true "token"
// @Param model_id query string true "Model -> id"
// @Success 201 {object} Response{data=[]nameList}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllNameByModelID() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []nameList

		modelID := c.Query("model_id")
		if modelID == "" {
			Return(c, result, errs.ErrModelIDEmpty)
			return
		}

		if !utils.IsValidUUID(modelID) {
			Return(c, result, errs.ErrModelIDFormat)
			return
		}

		data, err := s.nameUC.FindAllNameByModelID(c, modelID)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			nData := nameList{
				ID:      val.ID,
				Model:   val.Model,
				Name:    val.Name,
				NameKey: val.NameKey,
			}
			result = append(result, nData)
		}

		Return(c, result, nil)
	}
}

// nameFilter godoc
// @Router /name/filter [GET]
// @Summary Filter By model and name
// @Description Filter By model and name
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Param type_id query string false "Type -> id"
// @Param model_id query string false "Model -> id"
// @Param name_id query string false "Name -> id"
// @Success 201 {object} Response{data=namePagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) nameFilter() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result namePagination

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		typeID := c.Query("type_id")
		if typeID != "" {
			if !utils.IsValidUUID(typeID) {
				Return(c, result, errs.ErrTypeIDFormat)
				return
			}
		}

		modelID := c.Query("model_id")
		if modelID != "" {
			if !utils.IsValidUUID(modelID) {
				Return(c, result, errs.ErrModelIDFormat)
				return
			}
		}

		nameID := c.Query("name_id")
		if nameID != "" {
			if !utils.IsValidUUID(nameID) {
				Return(c, result, errs.ErrNameIDFormat)
				return
			}
		}

		data, err := s.nameUC.NameFilter(c, typeID, modelID, nameID, intLimit, intPage)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.TotalPages = data.TotalPages

		for _, val := range data.Name {
			nData := name{
				ID:             val.ID,
				Index:          val.Index,
				Type:           val.Type,
				ModelID:        val.ModelID,
				Model:          val.Model,
				Name:           val.Name,
				NameKey:        val.NameKey,
				TypeKey:        val.TypeKey,
				TransportGroup: val.TransportGroup,
				PhotoName:      val.PhotoName,
				PhotoURL:       val.PhotoURL,
				Active:         val.Active,
				CreateAt:       val.CreateAt,
			}
			result.Name = append(result.Name, nData)
		}

		Return(c, result, nil)
	}
}

// nameUpdate godoc
// @Router /name/update [PUT]
// @Summary Update Name
// @Description API to Update Name
// @Tags Admin Page Reference
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body nameStore true "Name data"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) nameUpdate() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   nameStore
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrNameIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrNameIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		if utils.IsSpecial(data.TypeKey) {
			if data.NameKey == "" {
				Return(c, result, errs.New("Transport guruhlari bo'sh kelmoqda"))
				return
			}
		}

		req := domain.Name{
			ModelID:   data.ModelID,
			Name:      data.Name,
			TypeKey:   data.TypeKey,
			NameKey:   data.NameKey,
			Active:    data.Active,
			PhotoName: data.PhotoName,
		}

		resp, err := s.nameUC.NameUpdate(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type namePhoto struct {
	PhotoName string `json:"photo_name"`
}

// updatePhotoUrl godoc
// @Router /name/update/photo [PUT]
// @Summary Update Photo Url
// @Description API to Update Photo Url
// @Tags Web
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body namePhoto true "Name photo"
// @Success 200 {object} Response{data}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateNameUrl() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   namePhoto
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrNameIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrNameIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Name{
			PhotoName: data.PhotoName,
		}

		resp, err := s.nameUC.UpdateNamePhoto(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type nameStatus struct {
	Active bool `json:"active"`
}

// updateNameStatus godoc
// @Router /name/update/status [PUT]
// @Summary Update Name Status
// @Description API to Update Name Status
// @Tags Web
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body nameStatus true "Name status"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateNameStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   nameStatus
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrNameIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrNameIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Name{
			Active: data.Active,
		}

		resp, err := s.nameUC.UpdateNameStatus(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// nameDelete godoc
// @Router /name/delete [DELETE]
// @Summary Delete Name
// @Description API Delete Name
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) nameDelete() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrNameIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrNameIDFormat)
			return
		}

		resp, err := s.nameUC.NameDelete(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// ------------------------------------------------------Position------------------------------------------------------------------------

type positionStore struct {
	NameID   string `json:"name_id" binding:"required"`
	Position string `json:"position" binding:"required"`
	Active   bool   `json:"active"`
}

// positionStore godoc
// @Router /position/store [POST]
// @Summary Create a new Position
// @Description API Create a Position
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param request body positionStore true "Position details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) positionStore() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   positionStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Position{
			NameID:   data.NameID,
			Position: data.Position,
			Active:   data.Active,
		}

		id, err := s.positionUC.PositionStore(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

type positionResponse struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Position string `json:"position"`
	Active   bool   `json:"active"`
	CreateAt string `json:"created_at"`
}

// positionFindOne godoc
// @Router /position/find/one [GET]
// @Summary Find Position
// @Description API Find Position
// @Tags Admin Page Reference
// @Produce json
// @Param id query string true "Position -> id"
// @Success 201 {object} Response{data=positionResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) positionFindOne() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result positionResponse

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrPositionIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrPositionIDFormat)
			return
		}

		data, err := s.positionUC.PositionFindOne(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = positionResponse{
			ID:       data.ID,
			Name:     data.NameID,
			Position: data.Position,
			Active:   data.Active,
			CreateAt: data.CreateAt,
		}

		Return(c, result, nil)
	}
}

// findAllPosition godoc
// @Router /position/find/all [GET]
// @Summary Find all Positions
// @Description API to find all Positions
// @Tags Web
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]positionResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllPosition() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []positionResponse

		data, err := s.positionUC.PositionFindAll(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			pData := positionResponse{
				ID:       val.ID,
				Name:     val.NameID,
				Position: val.Position,
				Active:   val.Active,
				CreateAt: val.CreateAt,
			}
			result = append(result, pData)
		}

		Return(c, result, nil)
	}
}

type position struct {
	ID       string `json:"id"`
	Index    int    `json:"index"`
	Type     string `json:"type"`
	Model    string `json:"model"`
	NameID   string `json:"name_id"`
	Name     string `json:"name"`
	Position string `json:"position"`
	Active   bool   `json:"active"`
	CreateAt string `json:"created_at"`
}

type positionPagination struct {
	Position   []position `json:"position"`
	TotalPages int        `json:"total_pages"`
}

// positionFindPagination godoc
// @Router /position/pagination [GET]
// @Summary Find Position With Pagination
// @Description API Position With Pagination
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "Access token"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Success 201 {object} Response{data=positionPagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) positionFindPagination() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result positionPagination

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		data, err := s.positionUC.PositionFindPagination(c, intLimit, intPage)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.TotalPages = data.TotalPages

		for _, val := range data.Position {
			pData := position{
				ID:       val.ID,
				Index:    val.Index,
				Type:     val.Type,
				Model:    val.Model,
				NameID:   val.NameID,
				Name:     val.Name,
				Position: val.Position,
				Active:   val.Active,
				CreateAt: val.CreateAt,
			}
			result.Position = append(result.Position, pData)
		}

		Return(c, result, nil)
	}
}

type positionList struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Position string `json:"position"`
}

// findAllPositionByNameID godoc
// @Router /position/find/all/name [GET]
// @Summary Find All Positions
// @Description API Find All Positions
// @Tags Web
// @Produce json
// @Param token header string true "token"
// @Param name_id query string true "Name -> id"
// @Success 201 {object} Response{data=[]positionList}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllPositionByNameID() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []positionList

		nameID := c.Query("name_id")

		if nameID == "" {
			Return(c, result, errs.ErrNameIDEmpty)
			return
		}

		if !utils.IsValidUUID(nameID) {
			Return(c, result, errs.ErrTypeIDFormat)
			return
		}

		data, err := s.positionUC.PositionFindAllByTypeID(c, nameID)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			pData := positionList{
				ID:       val.ID,
				Name:     val.NameID,
				Position: val.Position,
			}
			result = append(result, pData)
		}

		Return(c, result, nil)
	}
}

// positionFilter godoc
// @Router /position/filter [GET]
// @Summary Filter By name and position
// @Description Filter By name and position
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Param type_id query string false "Type -> id"
// @Param model_id query string false "Model -> id"
// @Param name_id query string false "Name -> id"
// @Success 201 {object} Response{data=positionPagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) positionFilter() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result positionPagination

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)
			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)
			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)
			return
		}

		type_id := c.Query("type_id")
		if type_id != "" {
			if !utils.IsValidUUID(type_id) {
				Return(c, result, errs.ErrTypeIDFormat)
				return
			}
		}
		model_id := c.Query("model_id")
		if model_id != "" {
			if !utils.IsValidUUID(model_id) {
				Return(c, result, errs.ErrModelIDFormat)
				return
			}
		}
		name_id := c.Query("name_id")
		if name_id != "" {
			if !utils.IsValidUUID(name_id) {
				Return(c, result, errs.ErrNameIDFormat)
				return
			}
		}

		data, err := s.positionUC.PositionFilter(c, type_id, model_id, name_id, intLimit, intPage)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.TotalPages = data.TotalPages

		for _, val := range data.Position {
			pData := position{
				ID:       val.ID,
				Index:    val.Index,
				Type:     val.Type,
				Model:    val.Model,
				NameID:   val.NameID,
				Name:     val.Name,
				Position: val.Position,
				Active:   val.Active,
				CreateAt: val.CreateAt,
			}
			result.Position = append(result.Position, pData)
		}

		Return(c, result, nil)
	}
}

type PositionUpdate struct {
	NameID   string `json:"name_id"`
	Position string `json:"position"`
	Active   bool   `json:"active"`
}

// positionUpdate godoc
// @Router /position/update [PUT]
// @Summary Update Position
// @Description API to Update Position
// @Tags Admin Page Reference
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body positionStore true "Position data"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) positionUpdate() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   positionStore
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrPositionIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrPositionIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return

		}

		req := domain.Position{
			NameID:   data.NameID,
			Position: data.Position,
			Active:   data.Active,
		}

		resp, err := s.positionUC.PositionUpdate(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type positionStatus struct {
	Active bool `json:"active"`
}

// updatePositionStatus godoc
// @Router /position/update/status [PUT]
// @Summary Update Position Status
// @Description API to Update Position Status
// @Tags Web
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body positionStatus true "Position status"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updatePositionStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   positionStatus
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrPositionIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrPositionIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Position{
			Active: data.Active,
		}

		resp, err := s.positionUC.UpdatePositionStatus(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// positionDelete godoc
// @Router /position/delete [DELETE]
// @Summary Delete Position
// @Description API Delete Position
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) positionDelete() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		ID := c.Query("id")

		if ID == "" {
			Return(c, result, errs.ErrPositionIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrPositionIDFormat)
			return
		}

		resp, err := s.positionUC.PositionDelete(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// ------------------------------------------------------Fuel-------------------------------------------------------------------------

type fuel struct {
	Fuel   string `json:"fuel" binding:"required"`
	Active bool   `json:"active"`
}

// fuelStore godoc
// @Router /fuel/store [POST]
// @Summary Create a new Fuel
// @Description API Create a Fuel
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param request body fuel true "Fuel details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) fuelStore() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   fuel
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Fuel{
			Fuel:   data.Fuel,
			Active: data.Active,
		}

		id, err := s.fuelUC.FuelStore(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

type fuelResponse struct {
	ID        string `json:"id"`
	Fuel      string `json:"fuel"`
	Active    bool   `json:"active"`
	CreatedAt string `json:"created_at"`
}

// fuelFindOne godoc
// @Router /fuel/find/one [GET]
// @Summary Find Fuel
// @Description API Find Fuel
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "Fuel -> id"
// @Success 201 {object} Response{data=fuelResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) fuelFindOne() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result fuelResponse

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrFuelIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrFuelIDFormat)
			return
		}

		data, err := s.fuelUC.FuelFindOne(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = fuelResponse{
			ID:        data.ID,
			Fuel:      data.Fuel,
			Active:    data.Active,
			CreatedAt: data.CreateAt,
		}

		Return(c, result, nil)
	}
}

type fuelsResponse struct {
	ID   string `json:"id"`
	Fuel string `json:"fuel"`
}

// findAllFuel godoc
// @Router /fuel/find/all [GET]
// @Summary Find all Fuels
// @Description API to find all Fuels
// @Tags Web
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]fuelsResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllFuel() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []fuelsResponse

		data, err := s.fuelUC.FuelFindAll(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			fData := fuelsResponse{
				ID:   val.ID,
				Fuel: val.Fuel,
			}
			result = append(result, fData)
		}

		Return(c, result, nil)
	}
}

// fuelUpdate godoc
// @Router /fuel/update [PUT]
// @Summary Update Fuel
// @Description API to Update Fuel
// @Tags Admin Page Reference
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body fuel true "Fuel data"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) fuelUpdate() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   fuel
			result Result
		)

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrFuelIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrFuelIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Fuel{
			Fuel:   data.Fuel,
			Active: data.Active,
		}

		resp, err := s.fuelUC.FuelUpdate(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type fuelStatus struct {
	Active bool `json:"active"`
}

// updateFuelStatus godoc
// @Router /fuel/update/status [PUT]
// @Summary Update Fuel Status
// @Description API to Update Fuel Status
// @Tags Web
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body fuelStatus true "Fuel status"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateFuelStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   fuelStatus
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrFuelIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrFuelIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Fuel{
			Active: data.Active,
		}

		resp, err := s.fuelUC.UpdateFuelStatus(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// fuelDelete godoc
// @Router /fuel/delete [DELETE]
// @Summary Delete Fuel
// @Description API Delete Fuel
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) fuelDelete() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrFuelIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrFuelIDFormat)
			return
		}

		resp, err := s.fuelUC.FuelDelete(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// ------------------------------------------------------Validity-------------------------------------------------------------------------

type validityStore struct {
	Validity string `json:"validity" binding:"required"`
	Code     int    `json:"code"`
	Active   bool   `json:"active"`
}

// validityStore godoc
// @Router /validity/store [POST]
// @Summary Create a new Validity
// @Description API Create a Validity
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param request body validityStore true "Validity details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) validityStore() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   validityStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Validity{
			Validity: data.Validity,
			Code:     data.Code,
			Active:   data.Active,
		}

		id, err := s.validityUC.ValidityStore(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: id.ID,
		}

		Return(c, result, nil)
	}
}

type validityResponse struct {
	ID       string `json:"id"`
	Validity string `json:"validity"`
	Code     int    `json:"code"`
	Active   bool   `json:"active"`
	CreateAt string `json:"created_at"`
}

// validityFindOne godoc
// @Router /validity/find/one [GET]
// @Summary Find Validity
// @Description API Find Validity
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "Validity -> id"
// @Success 201 {object} Response{data=validityResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) validityFindOne() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result validityResponse

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrValidityIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrValidityIDFormat)
			return
		}

		data, err := s.validityUC.ValidityFindOne(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = validityResponse{
			ID:       data.ID,
			Validity: data.Validity,
			Code:     data.Code,
			Active:   data.Active,
			CreateAt: data.CreateAt,
		}

		Return(c, result, nil)
	}
}

type allValidity struct {
	Validity string `json:"validity"`
	Code     int    `json:"code"`
}

// findAllValidity godoc
// @Router /validity/find/all [GET]
// @Summary Find all Validity
// @Description API to find all Validity
// @Tags Web
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]allValidity}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAllValidity() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []allValidity

		data, err := s.validityUC.FindAllValidity(c)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			vData := allValidity{
				Validity: val.Validity,
				Code:     val.Code,
			}
			result = append(result, vData)
		}

		Return(c, result, nil)
	}
}

// validityUpdate godoc
// @Router /validity/update [PUT]
// @Summary Update Validity
// @Description API to Update Validity
// @Tags Admin Page Reference
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body validityStore true "Validity data"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) validityUpdate() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   validityStore
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrValidityIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrValidityIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Validity{
			Validity: data.Validity,
			Active:   data.Active,
			Code:     data.Code,
		}

		resp, err := s.validityUC.ValidityUpdate(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

type validityStatus struct {
	Active bool `json:"active"`
}

// updateValidityStatus godoc
// @Router /validity/update/status [PUT]
// @Summary Update Validity Status
// @Description API to Update Validity Status
// @Tags Web
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body validityStatus true "Validity status"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateValidityStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   validityStatus
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrValidityIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrValidityIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Validity{
			Active: data.Active,
		}

		resp, err := s.validityUC.UpdateValidityStatus(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// validityDelete godoc
// @Router /validity/delete [DELETE]
// @Summary Delete Validity
// @Description API Delete Validity
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) validityDelete() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		ID := c.Query("id")

		if ID == "" {
			Return(c, result, errs.ErrValidityIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrValidityIDFormat)
			return
		}

		resp, err := s.validityUC.ValidityDelete(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// ------------------------------------------------------Reference All-------------------------------------------------------------------------

type referenceAll struct {
	Model    []allModels        `json:"model"`
	Name     []namesResponse    `json:"name"`
	Position []positionResponse `json:"position"`
	Fuel     []fuelsResponse    `json:"fuel"`
	Validity []allValidity      `json:"validity"`
}

// getAllMethods godoc
// @Router /reference/all [GET]
// @Summary Call All Reference Service Methods
// @Description API to call all Reference Service Methods
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param type_id query string true "Type id"
// @Success 201 {object} Response{data=referenceAll}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getAllMethods() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result referenceAll

		typeID := c.Query("type_id")

		if typeID == "" {
			Return(c, result, errs.ErrTypeIDEmpty)
			return
		}

		if !utils.IsValidUUID(typeID) {
			Return(c, result, errs.ErrTypeIDFormat)
			return
		}

		resp, err := s.referenceUC.GetAllMethods(c, typeID)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range resp.Model {
			mData := allModels{
				ID:       val.ID,
				Type:     val.TypeID,
				Model:    val.Model,
				PhotoUrl: val.PhotoUrl,
			}
			result.Model = append(result.Model, mData)
		}

		for _, val := range resp.Name {
			nData := namesResponse{
				ID:       val.ID,
				Model:    val.ModelID,
				Name:     val.Name,
				NameKey:  val.NameKey,
				PhotoURL: val.PhotoURL,
			}
			result.Name = append(result.Name, nData)
		}

		for _, val := range resp.Position {
			pData := positionResponse{
				ID:       val.ID,
				Name:     val.NameID,
				Position: val.Position,
			}
			result.Position = append(result.Position, pData)
		}

		for _, val := range resp.Fuel {
			fData := fuelsResponse{
				ID:   val.ID,
				Fuel: val.Fuel,
			}
			result.Fuel = append(result.Fuel, fData)
		}

		for _, val := range resp.Validity {
			vData := allValidity{
				Validity: val.Validity,
				Code:     val.Code,
			}
			result.Validity = append(result.Validity, vData)
		}

		Return(c, result, nil)
	}
}

// ------------------------------------------------------Discounts-------------------------------------------------------------------------

type discountParam struct {
	Code      int    `json:"code"`
	Percent   int    `json:"percent"`
	CreatedAt string `json:"created_at"`
}

type discountUpdate struct {
	Code    int `json:"code"`
	Percent int `json:"percent"`
}

type discountStore struct {
	NameID string `json:"name_id"`
}

type discounts struct {
	ID        string          `json:"id"`
	NameID    string          `json:"name_id"`
	Discounts []discountParam `json:"discounts"`
}

// getDiscounts godoc
// @Router /discount/all [GET]
// @Summary Get all Discounts
// @Description API Get all Discounts
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param name_id query string true "Discount -> id"
// @Success 201 {object} Response{data=discounts}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getDiscounts() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result discounts
		)
		nameID := c.Query("name_id")
		if nameID == "" {
			Return(c, result, errs.ErrNameIDEmpty)
			return
		}

		if !utils.IsValidUUID(nameID) {
			Return(c, result, errs.ErrNameIDFormat)
			return
		}

		data, err := s.discountUC.GetDiscounts(c, nameID)
		if err != nil {
			Return(c, result, err)
			return
		}
		result.ID = data.ID
		result.NameID = data.NameID

		for _, val := range data.DiscountParams {
			data := discountParam{
				Code:      val.Code,
				Percent:   val.Percent,
				CreatedAt: val.CreatedAt,
			}
			result.Discounts = append(result.Discounts, data)
		}

		Return(c, result, nil)
	}
}

// updateDiscount godoc
// @Router /discount/update [PUT]
// @Summary Update a Discount
// @Description API Update a Discount
// @Tags Admin Page Reference
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param name_id query string true "name_id"
// @Param request body discountUpdate true "Discount details"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateDiscount() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   discountUpdate
			result Result
		)

		nameID := c.Query("name_id")
		if nameID == "" {
			Return(c, result, errs.ErrDiscountIDEmpty)
			return
		}

		if !utils.IsValidUUID(nameID) {
			Return(c, result, errs.ErrDiscountIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.DiscountParams{
			Code:    data.Code,
			Percent: data.Percent,
		}

		resp, err := s.discountUC.UpdateDiscount(c, nameID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// storeDiscount godoc
// @Router /discount/store [POST]
// @Summary Create a Discount
// @Description API Create a Discount
// @Tags Admin Page Reference
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param request body discountStore true "Discount details"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) storeDiscount() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   discountStore
			result Result
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		req := domain.Discount{
			NameID: data.NameID,
		}

		resp, err := s.discountUC.StoreDiscounts(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// deleteDiscount godoc
// @Router /discount/delete [DELETE]
// @Summary Delete a Discount
// @Description API Delete a Discount
// @Tags Admin Page Reference
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param name_id query string true "name_id"
// @Param code query string true "Discount code"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) deleteDiscount() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result Result
		)

		nameID := c.Query("name_id")
		if nameID == "" {
			Return(c, result, errs.ErrDiscountIDEmpty)
			return
		}

		if !utils.IsValidUUID(nameID) {
			Return(c, result, errs.ErrDiscountIDFormat)
			return
		}

		code := c.Query("code")
		if code == "" {
			Return(c, result, errs.ErrDiscountCodeEmpty)
			return
		}

		intCode, err := strconv.Atoi(code)
		if err != nil {
			Return(c, result, errs.ErrDiscountCodeFormat)
			return
		}

		if intCode < 0 {
			Return(c, result, errs.ErrDiscountCodeFormat)
			return
		}

		req := domain.DiscountParams{
			Code: intCode,
		}

		resp, err := s.discountUC.DeleteDiscount(c, nameID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// ------------------------------------------------------Specials------------------------------------------------------

type specialStore struct {
	Key  string `json:"key" binding:"required"`
	Name string `json:"name" binding:"required"`
}

// storeSpecial godoc
// @Router /special/store [POST]
// @Summary Create a new Special Car
// @Description API Create a Special Car
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param request body specialStore true "Special Cars details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) storeSpecial() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   specialStore
			result idResponse
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return

		}

		req := domain.SpecialCars{
			Key:  data.Key,
			Name: data.Name,
		}

		resp, err := s.specialUC.StoreSpecial(c, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = idResponse{
			ID: resp.ID,
		}

		Return(c, result, nil)
	}
}

type specials struct {
	ID        string `json:"id"`
	Key       string `json:"key"`
	Name      string `json:"name"`
	FL        bool   `json:"fl"`
	KM        bool   `json:"km"`
	MH        bool   `json:"mh"`
	CA        bool   `json:"ca"`
	LE        bool   `json:"le"`
	PO        bool   `json:"po"`
	VO        bool   `json:"vo"`
	WI        bool   `json:"wi"`
	SE        bool   `json:"se"`
	CreatedAt string `json:"created_at"`
}

// findSpecials godoc
// @Router /special/find/all [GET]
// @Summary Find all Special Cars
// @Description API to find all Special Cars
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Success 201 {object} Response{data=[]specials}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findSpecials() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []specials

		data, err := s.specialUC.SpecialFindAll(c)
		if err != nil {

			Return(c, result, err)
			return
		}

		for _, val := range data {
			tData := specials{
				ID:        val.ID,
				Key:       val.Key,
				Name:      val.Name,
				FL:        val.FL,
				KM:        val.KM,
				MH:        val.MH,
				CA:        val.CA,
				LE:        val.LE,
				PO:        val.PO,
				VO:        val.VO,
				WI:        val.WI,
				SE:        val.SE,
				CreatedAt: val.CreatedAt,
			}
			result = append(result, tData)
		}

		Return(c, result, nil)
	}
}

// updateSpecial godoc
// @Router /special/update [PUT]
// @Summary Update Special Car
// @Description API to Update Special Car
// @Tags Admin Page Reference
// @Accept json
// @Produce json
// @Param token header string true "token"
// @Param id query string true "ID"
// @Param request body specialStore true "Special Car data"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateSpecial() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   specialStore
			result Result
		)
		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrSpecialIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrSpecialIDFormat)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return

		}

		req := domain.SpecialCars{
			Key:  data.Key,
			Name: data.Name,
		}

		resp, err := s.specialUC.UpdateSpecial(c, ID, req)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

// deleteSpecial godoc
// @Router /special/delete [DELETE]
// @Summary Delete Special Car
// @Description API Delete Special Car
// @Tags Admin Page Reference
// @Produce json
// @Param token header string true "token"
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) deleteSpecial() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrSpecialIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrSpecialIDFormat)
			return
		}

		resp, err := s.specialUC.DeleteSpecial(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}
