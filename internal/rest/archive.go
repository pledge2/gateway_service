package rest

import (
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
)

type File struct {
	File string `json:"file"`
}

// UploadFile godoc swagger
// @Router /minio/upload/file [POST]
// @Summary upload any file
// @Description upload any file
// @Tags Web
// @Accept multipart/form-data
// @Produce json
// @Param token header string true "token"
// @Param folder formData string true "folder"
// @Param file formData file true "this is a test file"
// @Success 200 {object} Response{data=File}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) uploadFile() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result File

		folder := c.Request.FormValue("folder")
		if folder == "" {
			Return(c, result, errs.ErrFolderEmpty)
			return
		}

		file, fileHeader, err := c.Request.FormFile("file")
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		fileExt := filepath.Ext(fileHeader.Filename)
		originalFileName := strings.TrimSuffix(filepath.Base(fileHeader.Filename), filepath.Ext(fileHeader.Filename)) + fileExt
		path := filepath.Join(".", "files")
		err = os.MkdirAll(path, os.ModePerm)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}
		fullPath := path + "/" + originalFileName

		out, err := os.Create(fullPath)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}
		defer out.Close()

		_, err = io.Copy(out, file)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}

		fileName, err := s.minioUC.UploadFile(c, fileHeader, folder)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = File{
			File: fileName.FileUrl,
		}

		Return(c, result, nil)
	}
}

// uploadPhoto godoc swagger
// @Router /minio/upload/photo [POST]
// @Summary  upload a picture
// @Description upload a picture by mobile
// @Tags Mobile
// @Accept multipart/form-data
// @Produce json
// @Param token header string true "token"
// @Param lat formData string true "lat"
// @Param lon formData string true "lon"
// @Param assessmentID formData string true "assessmentID"
// @Param suffix formData string true "suffix"
// @Param folder formData string true "folder"
// @Param file formData file true "this is a test file"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) uploadPhoto() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		lat := c.Request.FormValue("lat")
		if lat == "" {
			Return(c, result, errs.ErrLatEmpty)
			return
		}

		lon := c.Request.FormValue("lon")
		if lon == "" {
			Return(c, result, errs.ErrLonEmpty)
			return
		}

		assessmentID := c.Request.FormValue("assessmentID")
		if assessmentID == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)
			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, result, errs.ErrAssessmentIDFormat)
			return
		}

		suffix := c.Request.FormValue("suffix")
		if suffix == "" {
			Return(c, result, errs.ErrSuffixEmpty)
			return
		}

		folder := c.Request.FormValue("folder")
		if folder == "" {
			Return(c, result, errs.ErrFolderEmpty)
			return
		}

		file, fileHeader, err := c.Request.FormFile("file")
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		fileExt := filepath.Ext(fileHeader.Filename)
		originalFileName := strings.TrimSuffix(filepath.Base(fileHeader.Filename), filepath.Ext(fileHeader.Filename)) + fileExt
		path := filepath.Join(".", "files")
		err = os.MkdirAll(path, os.ModePerm)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}
		fullPath := path + "/" + originalFileName

		out, err := os.Create(fullPath)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}
		defer out.Close()

		_, err = io.Copy(out, file)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}

		finalResult, err := s.minioUC.UploadPhoto(c, fileHeader, folder, suffix, assessmentID, lat, lon)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = finalResult.Result

		Return(c, result, err)
	}
}

// uploadFileToNutanix godoc swagger
// @Router /nutanix/upload/file [POST]
// @Summary Save file into Nutanix
// @Description upload file
// @Tags Nutanix
// @Accept multipart/form-data
// @Produce json
// @Param token header string true "token"
// @Param folder formData string true "this is a folder name"
// @Param file formData file true "this is a test file"
// @Success 200 {object} Response{data=File}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) uploadFileToNutanix() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result File

		folder := c.Request.FormValue("folder")
		if folder == "" {
			Return(c, nil, errs.ErrFolderEmpty)
			return
		}

		file, fileHeader, err := c.Request.FormFile("file")
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		fileExt := filepath.Ext(fileHeader.Filename)
		originalFileName := strings.TrimSuffix(filepath.Base(fileHeader.Filename), filepath.Ext(fileHeader.Filename)) + fileExt
		path := filepath.Join(".", "files")
		err = os.MkdirAll(path, os.ModePerm)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}
		fullPath := path + "/" + originalFileName

		out, err := os.Create(fullPath)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}
		defer out.Close()

		_, err = io.Copy(out, file)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}

		fileName, err := s.nutanixUC.UploadFile(c, fileHeader, folder)
		if err != nil {
			Return(c, result, err)
			return
		}

		result = File{
			File: fileName.FileName,
		}

		Return(c, result, nil)
	}
}

// uploadPhotoToNutanix godoc swagger
// @Router /nutanix/upload/photo [POST]
// @Summary  upload a picture
// @Description upload a picture by mobile
// @Tags Nutanix
// @Accept multipart/form-data
// @Produce json
// @Param token header string true "token"
// @Param lat formData string true "lat"
// @Param lon formData string true "lon"
// @Param assessmentID formData string true "assessmentID"
// @Param suffix formData string true "suffix"
// @Param nameKey formData string false "this is a transport group"
// @Param folder formData string true "folder"
// @Param file formData file true "this is a test file"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) uploadPhotoToNutanix() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result Result

		lat := c.Request.FormValue("lat")
		if lat == "" {
			Return(c, result, errs.ErrLatEmpty)
			return
		}

		lon := c.Request.FormValue("lon")
		if lon == "" {
			Return(c, result, errs.ErrLonEmpty)
			return
		}

		assessmentID := c.Request.FormValue("assessmentID")
		if assessmentID == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)
			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, result, errs.ErrAssessmentIDFormat)
			return
		}

		suffix := c.Request.FormValue("suffix")
		if suffix == "" {
			Return(c, result, errs.ErrSuffixEmpty)
			return
		}

		nameKey := c.Request.FormValue("nameKey")

		folder := c.Request.FormValue("folder")
		if folder == "" {
			Return(c, nil, errs.ErrFolderEmpty)
			return
		}

		file, fileHeader, err := c.Request.FormFile("file")
		if err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		fileExt := filepath.Ext(fileHeader.Filename)
		originalFileName := strings.TrimSuffix(filepath.Base(fileHeader.Filename), filepath.Ext(fileHeader.Filename)) + fileExt
		path := filepath.Join(".", "files")
		err = os.MkdirAll(path, os.ModePerm)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}
		fullPath := path + "/" + originalFileName

		out, err := os.Create(fullPath)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}
		defer out.Close()

		_, err = io.Copy(out, file)
		if err != nil {
			Return(c, result, errs.ErrInternal)
			return
		}

		finalResult, err := s.nutanixUC.UploadPhoto(c, fileHeader, folder, suffix, nameKey, assessmentID, lat, lon)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = finalResult.Result

		Return(c, result, err)
	}
}
