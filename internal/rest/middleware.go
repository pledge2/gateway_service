package rest

import (
	"fmt"
	"gateway_service/internal/errs"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (s *Server) middleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		token := c.Request.Header.Get("token")
		if token == "" {
			Return(c, nil, errs.ErrTokenNotFound)
			c.Abort()
			return
		}

		if token != "" {
			tokenData, err := s.jwtUC.ExtractClaims(token)
			if err != nil {

				Return(c, nil, errs.ErrAccessTokenExpired)
				c.Abort()
				return
			}
			for key, value := range tokenData {
				switch key {
				case "cbid":
					cbid := fmt.Sprint(value)
					c.Set("cbid", cbid)
				case "device_num":
					c.Set("device_num", fmt.Sprint(value))
				case "device_type":
					c.Set("device_type", fmt.Sprint(value))
				case "role":
					c.Set("role", fmt.Sprint(value))
				case "full_name":
					c.Set("full_name", fmt.Sprint(value))
				case "role_name":
					c.Set("role_name", fmt.Sprint(value))
				case "branch_id":
					c.Set("branch_id", fmt.Sprint(value))
				case "direct":
					c.Set("direct", fmt.Sprint(value))
				}

			}

			branchID, isExist := c.Get("branch_id")
			if !isExist {
				Return(c, nil, errs.ErrBranchIDEmpty)
				c.Abort()
				return
			}
			_, err = strconv.Atoi(fmt.Sprint(branchID))
			if err != nil {
				Return(c, nil, errs.ErrBranchIDFormat)
				c.Abort()
				return
			}

			direct, isExist := c.Get("direct")
			if !isExist {
				Return(c, nil, errs.ErrDirectEmpty)
				c.Abort()
				return
			}
			_, err = strconv.Atoi(fmt.Sprint(direct))
			if err != nil {
				Return(c, nil, errs.ErrDirectFormat)
				c.Abort()
				return
			}

			cbid, isExist := c.Get("cbid")
			if !isExist {
				Return(c, nil, errs.ErrCbidEmpty)
				c.Abort()
				return
			}

			intCbid, err := strconv.Atoi(fmt.Sprint(cbid))
			if err != nil {
				Return(c, nil, errs.ErrCbidFormat)
				c.Abort()
				return
			}

			if intCbid != 1 {
				user, err := s.usersUC.UserInfo(c, intCbid)
				if err != nil {
					Return(c, nil, errs.ErrUnauthorized)
					c.Abort()
					return
				}

				c.Set("user_id", user.ID)

				role, isExist := c.Get("role")
				if !isExist {
					Return(c, nil, errs.ErrRoleCodeEmpty)
					c.Abort()
					return
				}

				roleCode, err := strconv.Atoi(fmt.Sprint(role))
				if err != nil {
					Return(c, nil, errs.ErrRoleCodeFormat)
					c.Abort()
					return
				}

				if user.Role != roleCode {
					Return(c, nil, errs.ErrRoleChanged)
					c.Abort()
					return
				}
			}

		} else {
			Return(c, nil, errs.ErrTokenNotFound)
			c.Abort()
			return
		}

		c.Next()
	}
}
