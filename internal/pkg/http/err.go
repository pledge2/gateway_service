package http

import (
	"errors"
	"fmt"
)

type Error struct {
	Err   error
	Info  string
	Retry bool
}

func e(err error, message string, retry bool) *Error {
	return &Error{
		Err:   err,
		Info:  message,
		Retry: retry,
	}
}

func (e *Error) Error() string {
	return fmt.Sprintf("%v: %v", e.Err.Error(), e.Info)
}

func (e *Error) Unwrap() error { return e.Err }

var (
	ErrWrongRequest           = errors.New("wrong request")
	ErrConnectionFailed       = errors.New("could not connect to service")
	ErrClientIsBlocked        = errors.New("client is blocked")
	ErrFailedToRetrieveToken  = errors.New("client is blocked: Failed to retrieve token")
	ErrHTTPError              = errors.New("http error")
	ErrWrongResponse          = errors.New("views is invalid")
	ErrErrorResponse          = errors.New("service returned error")
	ErrUnexpectedResponseData = errors.New("service returned unexpected data")
)
