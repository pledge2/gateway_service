package utils

import "strings"

func IsTrailer(value string) bool {
	defaultValue := "NE"
	if strings.Contains(value, defaultValue) {
		return true
	} else {
		return false
	}
}

func IsMotoHour(value string) bool {
	defaultValue := "MH"
	if strings.Contains(value, defaultValue) {
		return true
	} else {
		return false
	}
}
