package utils

import "strings"

func IsSpecial(val string) bool {
	return strings.HasPrefix(val, "SP")
}
