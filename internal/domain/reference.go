package domain

type ReferenceAll struct {
	Model    []Model
	Name     []Name
	Position []Position
	Fuel     []Fuel
	Validity []Validity
}
