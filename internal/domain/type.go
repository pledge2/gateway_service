package domain

type TypeStore struct {
	ID          string
	TypeKey     string
	Index       int
	Type        string
	Active      bool
	FastURL     string
	OfficialURL string
	PhotoName   string
	PhotoUrl    string
	CreateAt    string
	UpdateAt    string
}

type TypePagination struct {
	Type       []TypeStore
	TotalPages int
}
