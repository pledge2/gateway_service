package domain

import "time"

type EmpInfo struct {
	ID          string
	Index       int
	BranchID    int
	CategoryEmp string
	CBID        int
	DepCode     string
	DepName     string
	EmpState    string
	LocalCode   string
	MailAddress string
	Name        string
	PhoneMobil  string
	PostCode    string
	PostName    string
	State       string
	UserState   string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type EmpPagination struct {
	EmpInfo    []EmpInfo
	TotalPages int
}
