package domain

type StoreProduct struct {
	Product   string
	PhotoName string
}

type ProductResponse struct {
	ID        string
	Product   string
	PhotoUrl  string
	Active    bool
	CreatedAt string
	UpdatedAt string
}

type UpdateProduct struct {
	Product string
	Active  bool
}

type UpdateProductPhoto struct {
	PhotoName string
}

type IdResponse struct {
	ID string
}

type Result struct {
	Result bool
}

type UpdateProductStatus struct {
	Active bool
}
