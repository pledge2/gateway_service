package domain

type FuelControl struct {
	ID                string
	NameID            string
	FuelControlParams FuelControlParams
}

type FuelControls struct {
	ID                string
	NameID            string
	FuelControlParams []FuelControlParams
}

type FuelControlParams struct {
	FuelID    string
	Price     int
	Fuel      string
	CreatedAt string
}
