package domain

type Model struct {
	ID        string
	Index     int
	TypeID    string
	Type      string
	Model     string
	Active    bool
	PhotoName string
	PhotoUrl  string
	CreateAt  string
}

type ModelPagination struct {
	Model      []Model
	TotalPages int
}
