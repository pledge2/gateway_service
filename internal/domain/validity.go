package domain

type Validity struct {
	ID       string
	Validity string
	Code     int
	Active   bool
	CreateAt string
}
