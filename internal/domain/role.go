package domain

import "time"

type UserRole struct {
	ID       string
	Index    int
	Role     string
	Code     int
	Direct   int
	Active   bool
	CreateAt time.Time
	UpdateAt time.Time
	RoleCount
	RolePositions
}

type RolePositions struct {
	PostCode []string
	CBID     []string
}

type RoleCount struct {
	PostCode int
	CBID     int
}
