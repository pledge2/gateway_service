package domain

type Fuel struct {
	ID       string
	Fuel     string
	Active   bool
	CreateAt string
}
