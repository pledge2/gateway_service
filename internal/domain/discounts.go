package domain

type Discount struct {
	ID             string
	NameID         string
	DiscountParams DiscountParams
}

type Discounts struct {
	ID             string
	NameID         string
	DiscountParams []DiscountParams
}

type DiscountParams struct {
	Code      int
	Percent   int
	CreatedAt string
}
