package domain

type Name struct {
	ID             string
	Index          int
	ModelID        string
	Type           string
	Model          string
	Name           string
	Active         bool
	PhotoName      string
	PhotoURL       string
	CreateAt       string
	TypeKey        string
	NameKey        string
	TransportGroup string
}

type NamePagination struct {
	Name       []Name
	TotalPages int
}
