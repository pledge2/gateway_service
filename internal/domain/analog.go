package domain

// Analog used for requests
type Analog struct {
	ID             string
	PID            string
	UID            string
	Type           string
	Model          string
	Name           string
	Position       string
	Speedometer    int
	Year           int
	Fuel           string
	Price          int
	Validity       int
	SellPrice      int
	Photos         AllPhotosName
	SourceName     string
	Comment        string
	SourceFileName string
	Active         bool
	NameKey        string
}

type AllPhotosName struct {
	PhotosName []PhotosName
}

type PhotosName struct {
	PhotoName
	Source
}

type PhotoName struct {
	PhotoName string
}

type Source struct {
	LAT   string
	LON   string
	Title string
	Order int
}

// UpdateAnalogStatus used for request
type UpdateAnalogStatus struct {
	Analog []Analog
}

// GetAnalog used for responses
type GetAnalog struct {
	ID            string
	Type          string
	Model         string
	Name          string
	Position      string
	Speedometer   int
	Year          int
	Fuel          string
	SellPrice     int
	Price         int
	Validity      int
	Condition     string
	Active        bool
	Comment       string
	SourceFileUrl string
	SourceFile    string
	AllPhotosUrl  AllPhotosUrl
	Employee      string
	CreatedAt     string
	UpdatedAt     string
}

type AllPhotosUrl struct {
	PhotosUrl []PhotosUrl
}

type PhotosUrl struct {
	PhotoUrl
	Source
}
type PhotoUrl struct {
	PhotoUrl string
}

type AnalogListResp struct {
	AnalogList []AnalogList
	TotalPages int
}

type AnalogList struct {
	Index       int
	ID          string
	Model       string
	Name        string
	Position    string
	Speedometer int
	Year        int
	Fuel        string
	SellPrice   int
	Active      bool
	CreatedAt   string
}
