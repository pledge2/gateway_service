package domain

type PostcodeList struct {
	Index        int
	PostCode     string
	DepName      string
	PositionName string
	Direction    string
	Direct       int
}

type CBIDList struct {
	Index     int
	CBID      int
	BranchID  int
	Name      string
	Direction string
	Direct    int
	ExpiredAt string
}
