package domain

type UserInfo struct {
	ID       string
	CBID     int
	Name     string
	Direct   int
	Role     int
	Position string
	BranchID int
}

type AppUsers struct {
	ID            string
	Index         int
	CBID          int
	FIO           string
	Email         string
	Devices       []string
	DevicesStatus []string
	Role          string
	BranchID      int
	CreatedAt     string
}

type UserPagination struct {
	UsersList  []AppUsers
	TotalPages int
}
