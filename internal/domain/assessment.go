package domain

type TechNumberParams struct {
	AssessmentID   string
	PassportNumber string
	PassportSeria  string
	PlateNumber    string
}

type GovUzResponse struct {
	VehicleKuzov       string
	OwnerPinfl         string
	OwnerFullname      string
	VehicleModel       string
	VehicleColor       string
	VehicleMadeYear    int
	VehicleMotor       string
	VehicleFullWeight  int
	VehicleEmptyWeight int
	VehiclePlateNumber string
	PassportNumber     string
	PassportSeria      string
	VinNumber          string
	VehiclePower       int
	Seats              int
	VehicleShassi      string
}

type AssessmentPhotos struct {
	PhotoSource []PhotoSource
}

type PhotoSource struct {
	Title    string
	Order    int
	PhotoUrl string
	LAT      string
	LON      string
}

type AssessmentResult struct {
	MarketPrice       int
	CreditPrice       int
	TransportModel    string
	TransportName     string
	TransportPosition string
	TransportYear     int
}

type AssessmentStore struct {
	ID        string
	CBID      int
	Direct    int
	EXTCBID   int
	BranchID  int
	AIDs      []string
	AID       string
	PID       string
	GID       string
	IIBInfoID string
	Status    int
	Photos    AllPhotosName
	Transport Transport
	CardID    string
}

type GovUz struct {
	VehicleKuzov       string
	OwnerPinfl         string
	OwnerFullname      string
	VehicleModel       string
	VehicleColor       string
	VehicleMadeYear    int
	VehicleMotor       string
	VehicleFullWeight  int
	VehicleEmptyWeight int
	VehiclePlateNumber string
	VehiclePower       int
	PassportNumber     string
	PassportSeria      string
	VinNumber          string
	VehicleShassi      string
}

type Assessment struct {
	ID            string
	CBID          int
	PID           string
	AID           string
	GID           string
	Transport     Transport
	TransportView TransportView
	GovUz         GovUz
	IIBPermitInfo string
	IIBInfoYear   int
	IIBInfoFuel   int
	IIBInfoID     string
	Status        int
	MarketPrice   int
	CreditPrice   int
	Url           string
	AllPhotosUrl  AssessmentPhotos
	CreatedAt     string
	CardID        string
	EmpName       string
}

type Transport struct {
	ID            string
	Type          string
	TypeKey       string
	Model         string
	Name          string
	NameKey       string
	Position      string
	Year          int
	Speedometer   int
	Validity      int
	Condition     string
	Fuel          string
	ExtraParam    string
	Price         int
	SellPrice     int
	AllPhotosUrl  AllPhotosUrl
	AllPhotosName AllPhotosName
}

type TransportView struct {
	TransportModel    string
	TransportName     string
	TransportPosition string
	TransportYear     int
}

type AssessmentPagination struct {
	Assessments []Assessment
	TotalPages  int
	StatusName  string
}

type GovInfoWithCBID struct {
	AssessmentID string
	CBID         int
	Transport
	GovUzStore
}

type GovUzStore struct {
	OwnerPinfl               string
	OwnerFullname            string
	OwnerType                int
	OwnerDateBirth           string
	VehicleID                int
	VehiclePlateNumber       string
	VehicleModel             string
	VehicleColor             string
	VehicleRegistrationDate  string
	IibPermitInfo            string
	Division                 string
	GarovPermitInfo          string
	VehicleMadeYear          int
	VehicleType              int
	VehicleKuzov             string
	VehicleFullWeight        int
	VehicleEmptyWeight       int
	VehicleMotor             string
	VehicleFuelType          int
	VehicleSeats             int
	VehicleStands            int
	Inspection               string
	VehicleTexpassportSeria  string
	VehicleTexpassportNumber string
	BodyTypeName             string
	VehicleShassi            string
	VehiclePower             int
	DateSchetSpravka         string
	TuningPermit             string
	TuningGivenDate          string
	TuningIssueDate          string
	PrevPnfl                 string
	PrevOwner                string
	PrevOwnerType            string
	PrevPlateNumber          string
	PrevTexpasportSery       string
	PrevTexpasportNumber     string
	State                    string
}

type AssessmentPhotoFolderResponse struct {
	Result []string
}
