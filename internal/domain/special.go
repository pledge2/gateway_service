package domain

type SpecialCars struct {
	ID        string
	Key       string
	Name      string
	FL        bool
	KM        bool
	MH        bool
	CA        bool
	LE        bool
	PO        bool
	VO        bool
	WI        bool
	SE        bool
	CreatedAt string
}
