package domain

type Position struct {
	ID       string
	Index    int
	Type     string
	Model    string
	NameID   string
	Name     string
	Position string
	Active   bool
	CreateAt string
}

type PositionPagination struct {
	Position   []Position
	TotalPages int
}
