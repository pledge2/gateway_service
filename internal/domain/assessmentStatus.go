package domain

type AssessmentStatus struct {
	ID          string
	StatusCode  int
	StatusName  string
	StatusCount int
	Url         string
	Comment     string
	CreatedAt   string
}
