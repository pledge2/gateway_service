package domain

type SignUp struct {
	Email string
	CBID
	Password
	Device
}

type SignIn struct {
	CBID
	Password
	Device
}

type CheckCode struct {
	Code string
	CBID
	Device
}

type CheckCbid struct {
	CBID
	Device
}

type ChangePassword struct {
	Password
	CBID
	Device
}

type ResendCode struct {
	CBID
	Device
}

type CBID struct {
	CBID int
}

type Device struct {
	DeviceType string
	DeviceNum  string
}

type Password struct {
	Password   string
	RePassword string
}

type JwtTokens struct {
	AccessToken  string
	RefreshToken string
}
