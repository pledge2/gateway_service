package report

import (
	"context"
	"gateway_service/internal/pkg/logger"
)

type IReport interface {
	AssessmentReport(ctx context.Context, assessmentID string) ([]byte, error)
}

type UseCase struct {
	report IReport
	log    logger.Logger
}

func New(r IReport, log logger.Logger) *UseCase {
	return &UseCase{
		report: r,
		log:    log,
	}
}

func (s *UseCase) AssessmentReport(ctx context.Context, assessmentID string) ([]byte, error) {
	var logMsg = "uc.Assessment.AssessmentReport "

	result, err := s.report.AssessmentReport(ctx, assessmentID)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.AssessmentReport", logger.Error(err))

		return nil, err
	}
	return result, nil
}
