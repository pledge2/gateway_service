package employees

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type EmpI interface {
	FindEmployee(ctx context.Context, cbid int) (domain.EmpInfo, error)
	FindAllEmployees(ctx context.Context, limit, page int) (domain.EmpPagination, error)
	UpdateEmpData(ctx context.Context, id string, empData domain.EmpInfo) (domain.Result, error)
	GetEmployeeDataIABS(ctx context.Context) (domain.Result, error)
}

type UseCase struct {
	emp EmpI
	log logger.Logger
}

func New(emp EmpI, log logger.Logger) *UseCase {
	return &UseCase{
		emp: emp,
		log: log,
	}
}

func (uc *UseCase) GetEmpDataIABS(ctx context.Context) (domain.Result, error) {
	var (
		logMsg = "uc.Employees.GetEmpDataIABS "
		result domain.Result
	)

	result, err := uc.emp.GetEmployeeDataIABS(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.emp.GetEmployeeDataIABS", logger.Error(err))

		return result, err
	}

	return result, nil

}

func (uc *UseCase) FindEmployee(ctx context.Context, cbid int) (domain.EmpInfo, error) {
	var (
		logMsg = "uc.Employees.FindEmployee "
		result domain.EmpInfo
	)

	result, err := uc.emp.FindEmployee(ctx, cbid)
	if err != nil {
		uc.log.Error(logMsg+"uc.emp.FindEmployee", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) FindAllEmployees(ctx context.Context, limit, page int) (domain.EmpPagination, error) {
	var (
		logMsg = "uc.Employees.FindAllEmployees "
		result domain.EmpPagination
	)

	result, err := uc.emp.FindAllEmployees(ctx, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.emp.FindAllEmployees", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateEmpData(ctx context.Context, id string, empData domain.EmpInfo) (domain.Result, error) {
	var (
		logMsg = "uc.Employees.UpdateEmpData "
		result domain.Result
	)

	result, err := uc.emp.UpdateEmpData(ctx, id, empData)
	if err != nil {
		uc.log.Error(logMsg+"uc.emp.UpdateEmpData", logger.Error(err))

		return result, err
	}

	return result, nil
}
