package authorization

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type AuthI interface {
	SignUp(ctx context.Context, authData domain.SignUp) (domain.Result, error)
	SignIn(ctx context.Context, authData domain.SignIn) (domain.JwtTokens, error)
	CheckCode(ctx context.Context, codeData domain.CheckCode) (domain.Result, error)
	CheckCbid(ctx context.Context, cbidData domain.CheckCbid) (domain.Result, error)
	ChangePassword(ctx context.Context, authData domain.ChangePassword) (domain.Result, error)
	ResendCode(ctx context.Context, code domain.ResendCode) (domain.Result, error)
}

type UseCase struct {
	auth AuthI
	log  logger.Logger
}

func New(auth AuthI, log logger.Logger) *UseCase {
	return &UseCase{
		auth: auth,
		log:  log,
	}
}

func (uc *UseCase) SignUp(ctx context.Context, authData domain.SignUp) (domain.Result, error) {
	var (
		logMsg = "uc.Authorization.SignUp "
		result domain.Result
	)

	result, err := uc.auth.SignUp(ctx, authData)
	if err != nil {
		uc.log.Error(logMsg+"uc.auth.SignUp", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) SignIn(ctx context.Context, authData domain.SignIn) (domain.JwtTokens, error) {
	var (
		logMsg = "uc.Authorization.SignIn "
		result domain.JwtTokens
	)

	result, err := uc.auth.SignIn(ctx, authData)
	if err != nil {
		uc.log.Error(logMsg+"uc.auth.SignIn", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) CheckCode(ctx context.Context, codeData domain.CheckCode) (domain.Result, error) {
	var (
		logMsg = "uc.Authorization.CheckCode "
		result domain.Result
	)

	result, err := uc.auth.CheckCode(ctx, codeData)
	if err != nil {
		uc.log.Error(logMsg+"uc.auth.CheckCode", logger.Error(err))

		return result, err
	}
	return result, nil
}

func (uc *UseCase) CheckCBID(ctx context.Context, cbidData domain.CheckCbid) (domain.Result, error) {
	var (
		logMsg = "uc.Authorization.CheckCbid "
		result domain.Result
	)

	result, err := uc.auth.CheckCbid(ctx, cbidData)
	if err != nil {
		uc.log.Error(logMsg+"uc.auth.CheckCbid", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ResendCode(ctx context.Context, code domain.ResendCode) (domain.Result, error) {
	var (
		logMsg = "uc.Authorization.ResendCode "
		result domain.Result
	)

	result, err := uc.auth.ResendCode(ctx, code)
	if err != nil {
		uc.log.Error(logMsg+"uc.auth.ResendCode", logger.Error(err))

		return result, err
	}
	return result, nil
}

func (uc *UseCase) ChangePassword(ctx context.Context, authData domain.ChangePassword) (domain.Result, error) {
	var (
		logMsg = "uc.Authorization.ChangePassword "
		result domain.Result
	)

	result, err := uc.auth.ChangePassword(ctx, authData)
	if err != nil {
		uc.log.Error(logMsg+"uc.auth.ChangePassword", logger.Error(err))

		return result, err
	}

	return result, nil
}
