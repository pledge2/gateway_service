package roles

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type RoleI interface {
	CreateRole(ctx context.Context, roleData domain.UserRole) (domain.IdResponse, error)
	FindOneRole(ctx context.Context, id string) (domain.UserRole, error)
	RoleFindAll(ctx context.Context, roleCode int) ([]domain.UserRole, error)
	GetRole(ctx context.Context, code int) (domain.UserRole, error)
	UpdateRole(ctx context.Context, id string, usrData domain.UserRole) (domain.Result, error)
	UpdateRoleStatus(ctx context.Context, id string, rStatus domain.UserRole) (domain.Result, error)
	DeleteRole(ctx context.Context, id string) (domain.Result, error)
}

type UseCase struct {
	roles RoleI
	log   logger.Logger
}

func New(roles RoleI, log logger.Logger) *UseCase {
	return &UseCase{
		roles: roles,
		log:   log,
	}
}

func (uc *UseCase) CreateRole(ctx context.Context, roleData domain.UserRole) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Roles.CreateRole "
		result domain.IdResponse
	)

	result, err := uc.roles.CreateRole(ctx, roleData)
	if err != nil {
		uc.log.Error(logMsg+"uc.roles.CreateRole", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) FindOneRole(ctx context.Context, id string) (domain.UserRole, error) {
	var (
		logMsg = "uc.Roles.FindOneRole "
		result domain.UserRole
	)

	result, err := uc.roles.FindOneRole(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.roles.FindOneRole", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) RoleFindAll(ctx context.Context, roleCode int) ([]domain.UserRole, error) {
	var (
		logMsg = "uc.Roles.RoleFindAll "
		result []domain.UserRole
	)

	result, err := uc.roles.RoleFindAll(ctx, roleCode)
	if err != nil {
		uc.log.Error(logMsg+"uc.roles.RoleFindAll", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateRole(ctx context.Context, id string, usrData domain.UserRole) (domain.Result, error) {
	var (
		logMsg = "uc.Roles.UpdateRole "
		result domain.Result
	)

	result, err := uc.roles.UpdateRole(ctx, id, usrData)
	if err != nil {
		uc.log.Error(logMsg+"uc.roles.UpdateRole", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateRoleStatus(ctx context.Context, id string, rStatus domain.UserRole) (domain.Result, error) {
	var (
		logMsg = "uc.Roles.UpdateUserStatus "
		result domain.Result
	)

	result, err := uc.roles.UpdateRoleStatus(ctx, id, rStatus)
	if err != nil {
		uc.log.Error(logMsg+"uc.roles.UpdateRoleStatus", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) DeleteRole(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Roles.DeleteRole "
		result domain.Result
	)

	result, err := uc.roles.DeleteRole(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.roles.DeleteRole", logger.Error(err))

		return result, err
	}

	return result, nil
}
