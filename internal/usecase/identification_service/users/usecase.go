package users

import (
	"context"
	"fmt"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type UsersI interface {
	FindUserList(ctx context.Context) ([]domain.UserInfo, error)
	GetUserInfo(ctx context.Context, cbid int) (domain.UserInfo, error)
	GetUserName(ctx context.Context, id string) (domain.UserInfo, error)
	AllUsersList(ctx context.Context, limit, page int) (domain.UserPagination, error)
	UpdateUserEmail(ctx context.Context, id string, uEmail domain.AppUsers) (domain.Result, error)
	DeleteUser(ctx context.Context, id string) (domain.Result, error)
	BlockUser(ctx context.Context, cbid int) (domain.Result, error)
}

type UseCase struct {
	users UsersI
	log   logger.Logger
}

func New(users UsersI, log logger.Logger) *UseCase {
	return &UseCase{
		users: users,
		log:   log,
	}
}

func (uc *UseCase) FindUserList(ctx context.Context) ([]domain.UserInfo, error) {
	var (
		logMsg = "uc.Users.FindUserList "
		result []domain.UserInfo
	)

	result, err := uc.users.FindUserList(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.users.FindUserList", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UserInfo(ctx context.Context, cbid int) (domain.UserInfo, error) {
	var (
		logMsg = "uc.Users.UserInfo "
		result domain.UserInfo
	)

	result, err := uc.users.GetUserInfo(ctx, cbid)
	if err != nil {
		uc.log.Error(logMsg+"uc.users.GetUserInfo", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) AllUsersList(ctx context.Context, limit, page int) (domain.UserPagination, error) {
	var (
		logMsg = "uc.Users.AllUsersList "
		result domain.UserPagination
	)

	resp, err := uc.users.AllUsersList(ctx, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.users.AllUsersList", logger.Error(err))

		return result, err
	}

	for _, val := range resp.UsersList {
		data := domain.AppUsers{
			ID:        val.ID,
			Index:     val.Index,
			FIO:       val.FIO,
			Email:     val.Email,
			Role:      val.Role,
			BranchID:  val.BranchID,
			CreatedAt: val.CreatedAt,
		}

		for _, status := range val.DevicesStatus {
			deviceStatus := changeStatus(status)
			data.DevicesStatus = append(data.DevicesStatus, deviceStatus)
		}

		for _, status := range val.Devices {
			deviceName := changeDeviceName(status)
			data.Devices = append(data.Devices, deviceName)
		}
		result.UsersList = append(result.UsersList, data)
	}
	result.TotalPages = resp.TotalPages

	return result, nil
}

func (uc *UseCase) UpdateUserEmail(ctx context.Context, id string, uEmail domain.AppUsers) (domain.Result, error) {
	var (
		logMsg = "uc.Users.UpdateUserEmail "
		result domain.Result
	)

	result, err := uc.users.UpdateUserEmail(ctx, id, uEmail)
	if err != nil {
		uc.log.Error(logMsg+"uc.users.UpdateUserEmail", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) DeleteUser(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Users.DeleteUser "
		result domain.Result
	)

	result, err := uc.users.DeleteUser(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.users.DeleteUser", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) BlockUser(ctx context.Context, cbid int) (domain.Result, error) {
	var (
		logMsg = "uc.Users.BlockUser "
		result domain.Result
	)

	result, err := uc.users.BlockUser(ctx, cbid)
	if err != nil {
		uc.log.Error(logMsg+"uc.users.BlockUser", logger.Error(err))

		return result, err
	}

	return result, nil
}

func changeStatus(status string) string {
	var result string
	switch status {
	case "1":
		result = "Process"
	case "2":
		result = "Active"
	case "3":
		result = "Blocked"
	default:
		fmt.Println("Unknown Status")
	}
	return result
}

func changeDeviceName(device string) string {
	var result string

	switch device {
	case "android":
		result = "Android"
	case "web":
		result = "Web"
	case "ios":
		result = "IOS"
	default:
		fmt.Println("Unknown Device")
	}
	return result
}
