package app_roles

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type AppRolesI interface {
	GetAppRolesByPostCode(ctx context.Context, roleID string) ([]domain.PostcodeList, error)
	GetAppRolesByCbID(ctx context.Context, roleID string) ([]domain.CBIDList, error)
	SearchEmpByPostCode(ctx context.Context, postcode string) (domain.PostcodeList, error)
	SearchEmpByCbID(ctx context.Context, cbid int) (domain.CBIDList, error)
	AddPostcodeToRole(ctx context.Context, id string, rData domain.PostcodeList) (domain.Result, error)
	DeletePostcodeFromRole(ctx context.Context, id string, rData domain.PostcodeList) (domain.Result, error)
	AddCbidToRole(ctx context.Context, id string, cData domain.CBIDList) (domain.Result, error)
	DeleteCbidFromRole(ctx context.Context, id string, cData domain.CBIDList) (domain.Result, error)
}

type UseCase struct {
	appRoles AppRolesI
	log      logger.Logger
}

func New(s AppRolesI, log logger.Logger) *UseCase {
	return &UseCase{
		appRoles: s,
		log:      log,
	}
}

func (uc *UseCase) GetAppRolesByPostCode(ctx context.Context, roleID string) ([]domain.PostcodeList, error) {
	var (
		logMsg = "uc.AppRoles.GetAppRolesByPostCode "
		result []domain.PostcodeList
	)

	result, err := uc.appRoles.GetAppRolesByPostCode(ctx, roleID)
	if err != nil {
		uc.log.Error(logMsg+"uc.appRoles.GetAppRolesByPostCode", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) SearchRolesByPostCode(ctx context.Context, postcode string) (domain.PostcodeList, error) {
	var (
		logMsg = "uc.AppRoles.SearchRolesByPostCode "
		result domain.PostcodeList
	)

	result, err := uc.appRoles.SearchEmpByPostCode(ctx, postcode)
	if err != nil {
		uc.log.Error(logMsg+"uc.appRoles.SearchEmpByPostCode", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) GetAppRolesByCbID(ctx context.Context, roleID string) ([]domain.CBIDList, error) {
	var (
		logMsg = "uc.AppRoles.GetAppRolesByCbID "
		result []domain.CBIDList
	)

	result, err := uc.appRoles.GetAppRolesByCbID(ctx, roleID)
	if err != nil {
		uc.log.Error(logMsg+"uc.appRoles.GetAppRolesByCbID", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) SearchEmpByCbID(ctx context.Context, cbid int) (domain.CBIDList, error) {
	var (
		logMsg = "uc.AppRoles.SearchEmpByCbID "
		result domain.CBIDList
	)

	result, err := uc.appRoles.SearchEmpByCbID(ctx, cbid)
	if err != nil {
		uc.log.Error(logMsg+"uc.appRoles.SearchEmpByCbID", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) AddPostcodeToRole(ctx context.Context, roleID string, rData domain.PostcodeList) (domain.Result, error) {
	var (
		logMsg = "uc.AppRoles.AddPostcodeToRole "
		result domain.Result
	)

	result, err := uc.appRoles.AddPostcodeToRole(ctx, roleID, rData)
	if err != nil {
		uc.log.Error(logMsg+"uc.appRoles.AddPostcodeToRole", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) DeletePostcodeFromRole(ctx context.Context, roleID string, rData domain.PostcodeList) (domain.Result, error) {
	var (
		logMsg = "uc.AppRoles.DeletePostcodeFromRole "
		result domain.Result
	)

	result, err := uc.appRoles.DeletePostcodeFromRole(ctx, roleID, rData)
	if err != nil {
		uc.log.Error(logMsg+"uc.appRoles.DeletePostcodeFromRole", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) AddCbidToRole(ctx context.Context, id string, cData domain.CBIDList) (domain.Result, error) {
	var (
		logMsg = "uc.AppRoles.AddCbidToRole "
		result domain.Result
	)

	result, err := uc.appRoles.AddCbidToRole(ctx, id, cData)
	if err != nil {
		uc.log.Error(logMsg+"uc.appRoles.AddCbidToRole", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) DeleteCbidFromRole(ctx context.Context, id string, cData domain.CBIDList) (domain.Result, error) {
	var (
		logMsg = "uc.AppRoles.DeleteCbidFromRole "
		result domain.Result
	)

	result, err := uc.appRoles.DeleteCbidFromRole(ctx, id, cData)
	if err != nil {
		uc.log.Error(logMsg+"uc.appRoles.DeleteCbidFromRole", logger.Error(err))

		return result, err
	}

	return result, nil
}
