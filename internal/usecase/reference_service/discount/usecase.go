package discount

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type DiscountI interface {
	GetDiscounts(ctx context.Context, nameID string) (domain.Discounts, error)
	StoreDiscount(ctx context.Context, discount domain.Discount) (domain.Result, error)
	UpdateDiscount(ctx context.Context, nameID string, discount domain.DiscountParams) (domain.Result, error)
	DeleteDiscount(ctx context.Context, nameID string, discount domain.DiscountParams) (domain.Result, error)
}

type UseCase struct {
	service DiscountI
	log     logger.Logger
}

func New(srv DiscountI, log logger.Logger) *UseCase {
	return &UseCase{
		service: srv,
		log:     log,
	}
}

func (uc *UseCase) GetDiscounts(ctx context.Context, nameID string) (domain.Discounts, error) {
	var (
		logMsg = "uc.Discount.GetDiscounts "
		result domain.Discounts
	)

	result, err := uc.service.GetDiscounts(ctx, nameID)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.GetDiscounts", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateDiscount(ctx context.Context, nameID string, discount domain.DiscountParams) (domain.Result, error) {
	var (
		logMsg = "uc.Discount.UpdateDiscount "
		result domain.Result
	)

	result, err := uc.service.UpdateDiscount(ctx, nameID, discount)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateDiscount", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) StoreDiscounts(ctx context.Context, discount domain.Discount) (domain.Result, error) {
	var (
		logMsg = "uc.Discount.StoreDiscount "
		result domain.Result
	)

	result, err := uc.service.StoreDiscount(ctx, discount)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.StoreDiscount", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) DeleteDiscount(ctx context.Context, nameID string, discount domain.DiscountParams) (domain.Result, error) {
	var (
		logMsg = "uc.Discount.DeleteDiscount "
		result domain.Result
	)

	result, err := uc.service.DeleteDiscount(ctx, nameID, discount)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.DeleteDiscount", logger.Error(err))

		return result, err
	}

	return result, nil
}
