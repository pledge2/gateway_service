package position

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type PositionI interface {
	StorePosition(ctx context.Context, position domain.Position) (domain.IdResponse, error)
	FindOnePosition(ctx context.Context, id string) (domain.Position, error)
	FindAllPosition(ctx context.Context) ([]domain.Position, error)
	FindAllPositionByNameID(ctx context.Context, nameID string) ([]domain.Position, error)
	PositionPagination(ctx context.Context, limit, page int) (domain.PositionPagination, error)
	FilterPosition(ctx context.Context, typeID, modelID, nameID string, limit, page int) (domain.PositionPagination, error)
	UpdatePosition(ctx context.Context, id string, position domain.Position) (domain.Result, error)
	UpdatePositionStatus(ctx context.Context, id string, position domain.Position) (domain.Result, error)
	DeletePosition(ctx context.Context, id string) (domain.Result, error)
}

type UseCase struct {
	service PositionI
	log     logger.Logger
}

func New(srv PositionI, log logger.Logger) *UseCase {
	return &UseCase{
		service: srv,
		log:     log,
	}
}

func (uc *UseCase) PositionStore(ctx context.Context, position domain.Position) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Position.PositionStore "
		result domain.IdResponse
	)

	result, err := uc.service.StorePosition(ctx, position)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.StorePosition", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) PositionFindOne(ctx context.Context, id string) (domain.Position, error) {
	var (
		logMsg = "uc.Position.PositionFindOne "
		result domain.Position
	)

	result, err := uc.service.FindOnePosition(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindOnePosition", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) PositionFindAll(ctx context.Context) ([]domain.Position, error) {
	var (
		logMsg = "uc.Position.PositionFindAll "
		result []domain.Position
	)

	result, err := uc.service.FindAllPosition(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindAllPosition", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) PositionFindPagination(ctx context.Context, limit, page int) (domain.PositionPagination, error) {
	var (
		logMsg = "uc.Position.PositionFindPagination "
		result domain.PositionPagination
	)

	result, err := uc.service.PositionPagination(ctx, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.PositionPagination", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) PositionFindAllByTypeID(ctx context.Context, nameID string) ([]domain.Position, error) {
	var (
		logMsg = "uc.Position.PositionFindAllByTypeID "
		result []domain.Position
	)

	result, err := uc.service.FindAllPositionByNameID(ctx, nameID)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindAllPositionByNameID", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) PositionFilter(ctx context.Context, typeID, modelID, nameID string, limit, page int) (domain.PositionPagination, error) {
	var (
		logMsg = "uc.Position.PositionFilter "
		result domain.PositionPagination
	)

	result, err := uc.service.FilterPosition(ctx, typeID, modelID, nameID, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FilterPosition", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) PositionUpdate(ctx context.Context, id string, position domain.Position) (domain.Result, error) {
	var (
		logMsg = "uc.Position.PositionUpdate "
		result domain.Result
	)

	result, err := uc.service.UpdatePosition(ctx, id, position)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdatePosition", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdatePositionStatus(ctx context.Context, id string, position domain.Position) (domain.Result, error) {
	var (
		logMsg = "uc.Position.UpdatePositionStatus "
		result domain.Result
	)

	result, err := uc.service.UpdatePositionStatus(ctx, id, position)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdatePositionStatus", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) PositionDelete(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Position.PositionDelete "
		result domain.Result
	)

	result, err := uc.service.DeletePosition(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.DeletePosition", logger.Error(err))

		return result, err
	}

	return result, nil
}
