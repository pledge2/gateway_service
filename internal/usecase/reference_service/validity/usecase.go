package validity

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type ValidityI interface {
	StoreValidity(ctx context.Context, validity domain.Validity) (domain.IdResponse, error)
	FindOneValidity(ctx context.Context, id string) (domain.Validity, error)
	ValidityFindAll(ctx context.Context) ([]domain.Validity, error)
	ValidityUpdate(ctx context.Context, id string, validity domain.Validity) (domain.Result, error)
	UpdateValidityStatus(ctx context.Context, id string, validity domain.Validity) (domain.Result, error)
	ValidityDelete(ctx context.Context, id string) (domain.Result, error)
}

type UseCase struct {
	service ValidityI
	log     logger.Logger
}

func New(srv ValidityI, log logger.Logger) *UseCase {
	return &UseCase{
		service: srv,
		log:     log,
	}
}

func (uc *UseCase) ValidityStore(ctx context.Context, validity domain.Validity) (domain.IdResponse, error) {
	var logMsg = "uc.Validity.ValidityStore "

	result, err := uc.service.StoreValidity(ctx, validity)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.StoreValidity", logger.Error(err))

		return domain.IdResponse{}, err
	}

	return result, nil
}

func (uc *UseCase) ValidityFindOne(ctx context.Context, id string) (domain.Validity, error) {
	var (
		logMsg = "uc.Validity.ValidityFindOne "
		result domain.Validity
	)

	result, err := uc.service.FindOneValidity(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindOneValidity", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) FindAllValidity(ctx context.Context) ([]domain.Validity, error) {
	var (
		logMsg = "uc.Validity.FindAllValidity "
		result []domain.Validity
	)

	result, err := uc.service.ValidityFindAll(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ValidityFindAll", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ValidityUpdate(ctx context.Context, id string, validity domain.Validity) (domain.Result, error) {
	var (
		logMsg = "uc.Validity.ValidityUpdate "
		result domain.Result
	)

	result, err := uc.service.ValidityUpdate(ctx, id, validity)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ValidityUpdate", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateValidityStatus(ctx context.Context, id string, validity domain.Validity) (domain.Result, error) {
	var (
		logMsg = "uc.Validity.UpdateValidityStatus "
		result domain.Result
	)

	result, err := uc.service.UpdateValidityStatus(ctx, id, validity)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateValidityStatus", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ValidityDelete(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Validity.ValidityDelete "
		result domain.Result
	)

	result, err := uc.service.ValidityDelete(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ValidityDelete", logger.Error(err))

		return result, err
	}

	return result, nil
}
