package name

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type NameI interface {
	StoreName(ctx context.Context, name domain.Name) (domain.IdResponse, error)
	FindOneName(ctx context.Context, id string) (domain.Name, error)
	FindAllName(ctx context.Context) ([]domain.Name, error)
	FindAllNameByModelID(ctx context.Context, modelID string) ([]domain.Name, error)
	NamePagination(ctx context.Context, limit, page int) (domain.NamePagination, error)
	FilterName(ctx context.Context, typeID, modelID, nameID string, limit, page int) (domain.NamePagination, error)
	UpdateName(ctx context.Context, id string, name domain.Name) (domain.Result, error)
	UpdateNamePhoto(ctx context.Context, id string, name domain.Name) (domain.Result, error)
	UpdateNameStatus(ctx context.Context, id string, nStatus domain.Name) (domain.Result, error)
	DeleteName(ctx context.Context, id string) (domain.Result, error)
	ChangeNamePhotosFolder(ctx context.Context) (domain.Result, error)
}

type UseCase struct {
	service NameI
	log     logger.Logger
}

func New(srv NameI, log logger.Logger) *UseCase {
	return &UseCase{
		service: srv,
		log:     log,
	}
}

func (uc *UseCase) NameStore(ctx context.Context, name domain.Name) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Name.NameStore "
		result domain.IdResponse
	)

	result, err := uc.service.StoreName(ctx, name)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.StoreName", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) NameFindOne(ctx context.Context, id string) (domain.Name, error) {
	var (
		logMsg = "uc.Name.NameFindOne "
		result domain.Name
	)

	result, err := uc.service.FindOneName(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindOneName", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) NameFindAll(ctx context.Context) ([]domain.Name, error) {
	var (
		logMsg = "uc.Name.NameFindAll "
		result []domain.Name
	)

	result, err := uc.service.FindAllName(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindAllName", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) NamePagination(ctx context.Context, limit, page int) (domain.NamePagination, error) {
	var (
		logMsg = "uc.Name.NameFindPagination "
		result domain.NamePagination
	)

	result, err := uc.service.NamePagination(ctx, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.NamePagination", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) FindAllNameByModelID(ctx context.Context, modelID string) ([]domain.Name, error) {
	var (
		logMsg = "uc.Name.FindAllNameByModelID "
		result []domain.Name
	)

	result, err := uc.service.FindAllNameByModelID(ctx, modelID)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.NameFindAllByTypeID", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) NameFilter(ctx context.Context, typeID, modelID, nameID string, limit, page int) (domain.NamePagination, error) {
	var (
		logMsg = "uc.Reference.NameFilter "
		result domain.NamePagination
	)

	result, err := uc.service.FilterName(ctx, typeID, modelID, nameID, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.NameFilter", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) NameUpdate(ctx context.Context, id string, name domain.Name) (domain.Result, error) {
	var (
		logMsg = "uc.Reference.NameUpdate "
		result domain.Result
	)

	result, err := uc.service.UpdateName(ctx, id, name)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateName", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateNamePhoto(ctx context.Context, id string, name domain.Name) (domain.Result, error) {
	var (
		logMsg = "uc.Reference.UpdateNamePhoto "
		result domain.Result
	)

	result, err := uc.service.UpdateNamePhoto(ctx, id, name)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateNamePhoto", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateNameStatus(ctx context.Context, id string, nStatus domain.Name) (domain.Result, error) {
	var (
		logMsg = "uc.Reference.UpdateNameStatus "
		result domain.Result
	)

	result, err := uc.service.UpdateNameStatus(ctx, id, nStatus)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateNameStatus", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) NameDelete(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Reference.NameDelete "
		result domain.Result
	)

	result, err := uc.service.DeleteName(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.DeleteName", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ChangeNameFolder(ctx context.Context) (domain.Result, error) {
	var (
		logMsg = "uc.Type.ChangeModelFolder "
	)

	result, err := uc.service.ChangeNamePhotosFolder(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ChangeTypePhotosFolder", logger.Error(err))

		return domain.Result{}, err
	}

	return result, nil
}
