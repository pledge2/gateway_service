package fuel_control

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type FuelControlI interface {
	GetFuels(ctx context.Context, nameID string) (domain.FuelControls, error)
	StoreFuelControl(ctx context.Context, FuelControl domain.FuelControl) (domain.Result, error)
	UpdateFuelControl(ctx context.Context, nameID string, FuelControl domain.FuelControlParams) (domain.Result, error)
	DeleteFuelControl(ctx context.Context, nameID string, FuelControl domain.FuelControlParams) (domain.Result, error)
}

type UseCase struct {
	service FuelControlI
	log     logger.Logger
}

func New(srv FuelControlI, log logger.Logger) *UseCase {
	return &UseCase{
		service: srv,
		log:     log,
	}
}

func (uc *UseCase) GetFuels(ctx context.Context, nameID string) (domain.FuelControls, error) {
	var (
		logMsg = "uc.FuelControl.GetFuelControls "
		result domain.FuelControls
	)

	result, err := uc.service.GetFuels(ctx, nameID)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.GetFuelControls", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateFuelControl(ctx context.Context, nameID string, FuelControl domain.FuelControlParams) (domain.Result, error) {
	var (
		logMsg = "uc.FuelControl.UpdateFuelControl "
		result domain.Result
	)

	result, err := uc.service.UpdateFuelControl(ctx, nameID, FuelControl)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateFuelControl", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) StoreFuelControl(ctx context.Context, FuelControl domain.FuelControl) (domain.Result, error) {
	var (
		logMsg = "uc.FuelControl.StoreFuelControl "
		result domain.Result
	)

	result, err := uc.service.StoreFuelControl(ctx, FuelControl)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.StoreFuelControl", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) DeleteFuelControl(ctx context.Context, nameID string, FuelControl domain.FuelControlParams) (domain.Result, error) {
	var (
		logMsg = "uc.FuelControl.DeleteFuelControl "
		result domain.Result
	)

	result, err := uc.service.DeleteFuelControl(ctx, nameID, FuelControl)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.DeleteFuelControl", logger.Error(err))

		return result, err
	}

	return result, nil
}
