package reference_usecase

import (
	"context"
	"errors"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/logger"
)

type Model interface {
	FindAllModelByTypeID(ctx context.Context, typeID string) ([]domain.Model, error)
}

type Name interface {
	FindAllNameByModelID(ctx context.Context, modelID string) ([]domain.Name, error)
}

type Position interface {
	FindAllPositionByNameID(ctx context.Context, nameID string) ([]domain.Position, error)
}

type Fuel interface {
	FindAllFuel(ctx context.Context) ([]domain.Fuel, error)
}

type Validity interface {
	ValidityFindAll(ctx context.Context) ([]domain.Validity, error)
}

type UseCase struct {
	model    Model
	name     Name
	position Position
	fuel     Fuel
	validity Validity
	log      logger.Logger
}

func New(model Model, name Name, position Position, fuel Fuel, validity Validity, log logger.Logger) *UseCase {
	return &UseCase{
		model:    model,
		name:     name,
		position: position,
		fuel:     fuel,
		validity: validity,
		log:      log,
	}
}

func (uc *UseCase) GetAllMethods(ctx context.Context, typeID string) (domain.ReferenceAll, error) {
	var (
		logMsg = "uc.Reference.GetAllMethods "
		result domain.ReferenceAll
	)

	model, err := uc.model.FindAllModelByTypeID(ctx, typeID)
	if err != nil {
		uc.log.Error(logMsg+"uc.FindAllModelByTypeID", logger.Error(err))

		return result, err
	}
	result.Model = model

	for _, val := range model {
		names, err := uc.name.FindAllNameByModelID(ctx, val.ID)
		if err != nil {
			uc.log.Error(logMsg+"uc.FindAllNameByModelID", logger.Error(err))
			if !errors.Is(err, errs.ErrDataNotFound) {
				continue
			}
			return result, err
		}
		result.Name = append(result.Name, names...)

		for _, name := range names {
			positions, err := uc.position.FindAllPositionByNameID(ctx, name.ID)
			if err != nil {
				uc.log.Error(logMsg+"uc.FindAllPositionByNameID", logger.Error(err))
				if !errors.Is(err, errs.ErrDataNotFound) {
					continue
				}
				return result, err
			}
			result.Position = append(result.Position, positions...)
		}
	}

	fuel, err := uc.fuel.FindAllFuel(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.FuelFindAll", logger.Error(err))

		return result, err
	}
	result.Fuel = fuel

	validity, err := uc.validity.ValidityFindAll(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.ValidityFindAll", logger.Error(err))

		return result, err
	}
	result.Validity = validity

	return result, nil
}
