package model

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type ModelI interface {
	StoreModel(ctx context.Context, model domain.Model) (domain.IdResponse, error)
	FindOneModel(ctx context.Context, id string) (domain.Model, error)
	FindAllModel(ctx context.Context) ([]domain.Model, error)
	FindAllModelByTypeID(ctx context.Context, typeID string) ([]domain.Model, error)
	ModelPagination(ctx context.Context, limit, page int) (domain.ModelPagination, error)
	FilterModel(ctx context.Context, typeID, modelID string, limit, page int) (domain.ModelPagination, error)
	UpdateModel(ctx context.Context, id string, model domain.Model) (domain.Result, error)
	UpdateModelPhoto(ctx context.Context, id string, mData domain.Model) (domain.Result, error)
	UpdateModelStatus(ctx context.Context, id string, mData domain.Model) (domain.Result, error)
	DeleteModel(ctx context.Context, id string) (domain.Result, error)
	ChangeModelPhotosFolder(ctx context.Context) (domain.Result, error)
}

type UseCase struct {
	service ModelI
	log     logger.Logger
}

func New(srv ModelI, log logger.Logger) *UseCase {
	return &UseCase{
		service: srv,
		log:     log,
	}
}

func (uc *UseCase) ModelStore(ctx context.Context, model domain.Model) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Model.ModelStore "
		result domain.IdResponse
	)

	result, err := uc.service.StoreModel(ctx, model)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ModelStore", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ModelFindOne(ctx context.Context, id string) (domain.Model, error) {
	var (
		logMsg = "uc.Model.ModelFindOne "
		result domain.Model
	)

	result, err := uc.service.FindOneModel(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindOneModel", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ModelFindAll(ctx context.Context) ([]domain.Model, error) {
	var (
		logMsg = "uc.Reference.ModelFindAll "
		result []domain.Model
	)

	result, err := uc.service.FindAllModel(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindAllModel", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ModelFindPagination(ctx context.Context, limit, page int) (domain.ModelPagination, error) {
	var (
		logMsg = "uc.Model.ModelFindPagination "
		result domain.ModelPagination
	)

	result, err := uc.service.ModelPagination(ctx, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ModelPagination", logger.Error(err))

		return result, err
	}
	return result, nil
}

func (uc *UseCase) ModelFindAllByTypeID(ctx context.Context, typeID string) ([]domain.Model, error) {
	var (
		logMsg = "uc.Model.ModelFindAllByTypeID "
		result []domain.Model
	)

	result, err := uc.service.FindAllModelByTypeID(ctx, typeID)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindAllModelByTypeID", logger.Error(err))

		return result, err
	}
	return result, nil
}

func (uc *UseCase) ModelFilter(ctx context.Context, typeID, modelID string, limit, page int) (domain.ModelPagination, error) {
	var (
		logMsg = "uc.Reference.ModelFilter "
		result domain.ModelPagination
	)

	result, err := uc.service.FilterModel(ctx, typeID, modelID, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FilterModel", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ModelUpdate(ctx context.Context, id string, model domain.Model) (domain.Result, error) {
	var (
		logMsg = "uc.Reference.ModelUpdate "
		result domain.Result
	)

	result, err := uc.service.UpdateModel(ctx, id, model)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateModel", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateModelPhoto(ctx context.Context, id string, model domain.Model) (domain.Result, error) {
	var (
		logMsg = "uc.Reference.UpdateModelPhoto "
		result domain.Result
	)

	result, err := uc.service.UpdateModelPhoto(ctx, id, model)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateModelPhoto", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateModelStatus(ctx context.Context, id string, mData domain.Model) (domain.Result, error) {
	var (
		logMsg = "uc.Reference.UpdateModelStatus "
		result domain.Result
	)

	result, err := uc.service.UpdateModelStatus(ctx, id, mData)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateModelStatus", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ModelDelete(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Reference.ModelDelete "
		result domain.Result
	)

	result, err := uc.service.DeleteModel(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.DeleteModel", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ChangeModelFolder(ctx context.Context) (domain.Result, error) {
	var (
		logMsg = "uc.Type.ChangeModelFolder "
	)

	result, err := uc.service.ChangeModelPhotosFolder(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ChangeTypePhotosFolder", logger.Error(err))

		return domain.Result{}, err
	}

	return result, nil
}
