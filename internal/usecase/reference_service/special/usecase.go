package special

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type SpecialI interface {
	StoreSpecial(ctx context.Context, sData domain.SpecialCars) (domain.IdResponse, error)
	FindAllSpecials(ctx context.Context) ([]domain.SpecialCars, error)
	UpdateSpecial(ctx context.Context, id string, tData domain.SpecialCars) (domain.Result, error)
	DeleteSpecial(ctx context.Context, id string) (domain.Result, error)
}

type UseCase struct {
	service SpecialI
	log     logger.Logger
}

func New(srv SpecialI, log logger.Logger) *UseCase {
	return &UseCase{
		service: srv,
		log:     log,
	}
}

func (uc *UseCase) StoreSpecial(ctx context.Context, tData domain.SpecialCars) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Special.StoreSpecial "
		result domain.IdResponse
	)

	result, err := uc.service.StoreSpecial(ctx, tData)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.StoreSpecial", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) SpecialFindAll(ctx context.Context) ([]domain.SpecialCars, error) {
	var (
		logMsg = "uc.Special.SpecialFindAll "
		result []domain.SpecialCars
	)

	result, err := uc.service.FindAllSpecials(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindAllSpecials", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateSpecial(ctx context.Context, id string, tData domain.SpecialCars) (domain.Result, error) {
	var (
		logMsg = "uc.Special.UpdateSpecial "
		result domain.Result
	)

	result, err := uc.service.UpdateSpecial(ctx, id, tData)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateSpecial", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) DeleteSpecial(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Special.DeleteSpecial "
		result domain.Result
	)

	result, err := uc.service.DeleteSpecial(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.DeleteType", logger.Error(err))

		return result, err
	}

	return result, nil
}
