package type_usecase

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type TypeI interface {
	StoreType(ctx context.Context, tData domain.TypeStore) (domain.IdResponse, error)
	FindOneType(ctx context.Context, id string) (domain.TypeStore, error)
	FindAllType(ctx context.Context) ([]domain.TypeStore, error)
	TypePagination(ctx context.Context, limit, page int) (domain.TypePagination, error)
	UpdateType(ctx context.Context, id string, tData domain.TypeStore) (domain.Result, error)
	UpdateTypePhoto(ctx context.Context, id string, tPhoto domain.TypeStore) (domain.Result, error)
	UpdateTypeStatus(ctx context.Context, id string, tPhoto domain.TypeStore) (domain.Result, error)
	DeleteType(ctx context.Context, id string) (domain.Result, error)
	ChangeTypePhotosFolder(ctx context.Context) (domain.Result, error)
}

type UseCase struct {
	service TypeI
	log     logger.Logger
}

func New(srv TypeI, log logger.Logger) *UseCase {
	return &UseCase{
		service: srv,
		log:     log,
	}
}

func (uc *UseCase) TypeStore(ctx context.Context, tData domain.TypeStore) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Type.TypeStore "
		result domain.IdResponse
	)

	result, err := uc.service.StoreType(ctx, tData)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.StoreType", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) TypeFindOne(ctx context.Context, id string) (domain.TypeStore, error) {
	var (
		logMsg = "uc.Type.TypeFindOne "
		result domain.TypeStore
	)

	result, err := uc.service.FindOneType(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindOneType", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) TypeFindAll(ctx context.Context) ([]domain.TypeStore, error) {
	var (
		logMsg = "uc.Type.TypeFindAll "
		result []domain.TypeStore
	)

	result, err := uc.service.FindAllType(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindAllType", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) TypeFindPagination(ctx context.Context, limit, page int) (domain.TypePagination, error) {
	var (
		logMsg = "uc.Type.TypeFindPagination "
		result domain.TypePagination
	)

	result, err := uc.service.TypePagination(ctx, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.TypePagination", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) TypeUpdate(ctx context.Context, id string, tData domain.TypeStore) (domain.Result, error) {
	var (
		logMsg = "uc.Type.TypeUpdate "
		result domain.Result
	)

	result, err := uc.service.UpdateType(ctx, id, tData)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateType", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateTypePhoto(ctx context.Context, id string, tPhoto domain.TypeStore) (domain.Result, error) {
	var (
		logMsg = "uc.Type.UpdateTypePhoto "
		result domain.Result
	)

	result, err := uc.service.UpdateTypePhoto(ctx, id, tPhoto)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateTypePhoto", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateTypeStatus(ctx context.Context, id string, tPhoto domain.TypeStore) (domain.Result, error) {
	var (
		logMsg = "uc.Type.UpdateTypeStatus "
		result domain.Result
	)

	result, err := uc.service.UpdateTypeStatus(ctx, id, tPhoto)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateTypeStatus", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) TypeDelete(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Type.TypeDelete "
		result domain.Result
	)

	result, err := uc.service.DeleteType(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.DeleteType", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ChangeTypeFolder(ctx context.Context) (domain.Result, error) {
	var (
		logMsg = "uc.Type.ChangeTypeFolder "
	)

	result, err := uc.service.ChangeTypePhotosFolder(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ChangeTypePhotosFolder", logger.Error(err))

		return domain.Result{}, err
	}

	return result, nil
}
