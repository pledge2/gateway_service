package fuel

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type FuelI interface {
	StoreFuel(ctx context.Context, fData domain.Fuel) (domain.IdResponse, error)
	FindOneFuel(ctx context.Context, id string) (domain.Fuel, error)
	FindAllFuel(ctx context.Context) ([]domain.Fuel, error)
	UpdateFuel(ctx context.Context, id string, fData domain.Fuel) (domain.Result, error)
	UpdateFuelStatus(ctx context.Context, id string, fData domain.Fuel) (domain.Result, error)
	DeleteFuel(ctx context.Context, id string) (domain.Result, error)
}

type UseCase struct {
	service FuelI
	log     logger.Logger
}

func New(srv FuelI, log logger.Logger) *UseCase {
	return &UseCase{
		service: srv,
		log:     log,
	}
}

func (uc *UseCase) FuelStore(ctx context.Context, fData domain.Fuel) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Fuel.FuelStore "
		result domain.IdResponse
	)

	result, err := uc.service.StoreFuel(ctx, fData)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.StoreFuel", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) FuelFindOne(ctx context.Context, id string) (domain.Fuel, error) {
	var (
		logMsg = "uc.Fuel.FuelFindOne "
		result domain.Fuel
	)

	result, err := uc.service.FindOneFuel(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FindOneFuel", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) FuelFindAll(ctx context.Context) ([]domain.Fuel, error) {
	var (
		logMsg = "uc.Fuel.FuelFindAll "
		result []domain.Fuel
	)

	result, err := uc.service.FindAllFuel(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.FuelFindAll", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) FuelUpdate(ctx context.Context, id string, fData domain.Fuel) (domain.Result, error) {
	var (
		logMsg = "uc.Fuel.FuelUpdate "
		result domain.Result
	)

	result, err := uc.service.UpdateFuel(ctx, id, fData)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateFuel", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateFuelStatus(ctx context.Context, id string, fData domain.Fuel) (domain.Result, error) {
	var (
		logMsg = "uc.Fuel.UpdateFuelStatus "
		result domain.Result
	)

	result, err := uc.service.UpdateFuelStatus(ctx, id, fData)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.UpdateFuelStatus", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) FuelDelete(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Fuel.FuelDelete "
		result domain.Result
	)

	result, err := uc.service.DeleteFuel(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.DeleteFuel", logger.Error(err))

		return result, err
	}

	return result, nil
}
