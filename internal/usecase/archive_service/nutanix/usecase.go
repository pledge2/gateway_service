package nutanix_usecase

import (
	"context"
	"fmt"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
	"mime/multipart"
)

type NutanixI interface {
	UploadPhotoToNutanix(ctx context.Context, fileHeader *multipart.FileHeader, folder, suffix, nameKey, assessmentID, lat, lon string) (domain.Result, error)
	UploadFileToNutanix(ctx context.Context, fileHeader *multipart.FileHeader, folder string) (domain.FileName, error)
}

type UseCase struct {
	nutanix NutanixI
	log     logger.Logger
}

func New(nutanix NutanixI, log logger.Logger) *UseCase {
	return &UseCase{
		nutanix: nutanix,
		log:     log,
	}
}

func (uc *UseCase) UploadFile(ctx context.Context, fileHeader *multipart.FileHeader, folder string) (domain.FileName, error) {
	var (
		logMsg = "uc.Nutanix.UploadFile "
	)

	result, err := uc.nutanix.UploadFileToNutanix(ctx, fileHeader, folder)
	if err != nil {
		uc.log.Error(logMsg+"uc.nutanix.UploadFileToNutanix", logger.Error(err))

		return domain.FileName{}, err
	}

	return result, nil
}

func (uc *UseCase) UploadPhoto(ctx context.Context, fileHeader *multipart.FileHeader, folder, suffix, nameKey, assessmentID, lat, lon string) (domain.Result, error) {
	var (
		logMsg = "uc.Nutanix.UploadPhoto "
	)

	uc.log.Info(fmt.Sprint("Fayl o'lchami ", fileHeader.Size/1024))

	result, err := uc.nutanix.UploadPhotoToNutanix(ctx, fileHeader, folder, suffix, nameKey, assessmentID, lat, lon)
	if err != nil {
		uc.log.Error(logMsg+"uc.nutanix.UploadPhotoToNutanix", logger.Error(err))

		return domain.Result{}, err
	}

	return result, nil
}
