package minio_usecase

import (
	"context"
	"fmt"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
	"mime/multipart"
)

type MinioI interface {
	UploadPhoto(ctx context.Context, fileHeader *multipart.FileHeader, folder, suffix, assessmentID, lat, lon string) (domain.Result, error)
	UploadFile(ctx context.Context, fileHeader *multipart.FileHeader, folder string) (domain.FileName, error)
	GetFile(ctx context.Context, folder, file string) (domain.FileUrl, error)
}

type UseCase struct {
	archive MinioI
	log     logger.Logger
}

func New(archive MinioI, log logger.Logger) *UseCase {
	return &UseCase{
		archive: archive,
		log:     log,
	}
}

func (uc *UseCase) UploadFile(ctx context.Context, fileHeader *multipart.FileHeader, folder string) (domain.FileUrl, error) {
	var (
		logMsg = "uc.Archive.UploadFile "
		result domain.FileUrl
	)

	file, err := uc.archive.UploadFile(ctx, fileHeader, folder)
	if err != nil {
		uc.log.Error(logMsg+"uc.archive.UploadFile", logger.Error(err))

		return result, err
	}

	result = domain.FileUrl{
		FileUrl: file.FileName,
	}

	return result, nil
}

func (uc *UseCase) UploadPhoto(ctx context.Context, fileHeader *multipart.FileHeader, folder, suffix, assessmentID, lat, lon string) (domain.Result, error) {
	var (
		logMsg = "uc.Archive.UploadPhoto "
		result domain.Result
	)

	uc.log.Info(fmt.Sprint("Fayl o'lchami ", fileHeader.Size/1024))

	response, err := uc.archive.UploadPhoto(ctx, fileHeader, folder, suffix, assessmentID, lat, lon)
	if err != nil {
		uc.log.Error(logMsg+"uc.archive.UploadPhoto", logger.Error(err))

		return result, err
	}

	result.Result = response.Result

	return result, nil
}
