package jwt

import (
	"context"
	"fmt"
	"gateway_service/internal/domain"
	"strconv"
	"time"

	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/logger"

	"github.com/dgrijalva/jwt-go"
)

type UsersI interface {
	GetUserInfo(ctx context.Context, cbid int) (domain.UserInfo, error)
}

type RolesI interface {
	GetRole(ctx context.Context, code int) (domain.UserRole, error)
}

type TokenUseCase struct {
	accessTokenTTL  time.Duration
	refreshTokenTTL time.Duration
	tokenSecret     string
	sign            UsersI
	role            RolesI
	log             logger.Logger
}

func New(a_ttl time.Duration, r_ttl time.Duration, secret string, sign UsersI, role RolesI, log logger.Logger) *TokenUseCase {
	return &TokenUseCase{
		accessTokenTTL:  a_ttl,
		refreshTokenTTL: r_ttl,
		tokenSecret:     secret,
		sign:            sign,
		role:            role,
		log:             log,
	}
}

// ExtractClaims extracts claims from given token
func (t *TokenUseCase) ExtractClaims(tokenString string) (jwt.MapClaims, error) {
	var (
		logMsg = "uc.JWT.ExtractClaims "
		token  *jwt.Token
		err    error
	)

	token, err = jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(t.tokenSecret), nil
	})
	if err != nil {
		t.log.Error(logMsg+"jwt.Parse", logger.Error(err))
		return nil, errs.ErrInternal
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !(ok && token.Valid) {
		t.log.Error(logMsg+"token.Claims.", logger.Error(err))
		return nil, errs.ErrInternal
	}

	return claims, nil
}

func (t *TokenUseCase) GenerateToken(m map[string]interface{}) (string, string, error) {
	var logMsg = "uc.JWT.GenerateToken "

	aToken := jwt.New(jwt.SigningMethodHS256)
	rToken := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	// This is the information which frontend can use
	// The backend can also decode the token and get admin etc.
	aClaims := aToken.Claims.(jwt.MapClaims)
	rClaims := rToken.Claims.(jwt.MapClaims)

	for key, value := range m {

		if key == "cbid" {
			intCBID, err := strconv.Atoi(fmt.Sprint(value))
			if err != nil {
				return "", "", errs.ErrCbidFormat
			}
			role, err := t.changeRole(intCBID)
			if err != nil {
				t.log.Error(logMsg+"t.changeRole", logger.Error(err))
				return "", "", err
			}
			aClaims["role"] = role.Code
			rClaims["role"] = role.Code

			aClaims["role_name"] = role.Role
			rClaims["role_name"] = role.Role

			aClaims["direct"] = role.Direct
			rClaims["direct"] = role.Direct

		}
		if key != "role" && key != "role_name" && key != "direct" {
			aClaims[key] = value
			rClaims[key] = value
		}
	}

	aClaims["iat"] = time.Now().Unix()
	aClaims["exp"] = time.Now().Add(t.accessTokenTTL).Unix()

	// Generate encoded token and send it as response.
	// The signing string should be secret (a generated UUID works too)
	accessToken, err := aToken.SignedString([]byte(t.tokenSecret))
	if err != nil {
		t.log.Error(logMsg+"aToken.SignedString", logger.Error(err))
		return "", "", err
	}

	rClaims["iat"] = time.Now().Unix()
	rClaims["exp"] = time.Now().Add(t.refreshTokenTTL).Unix()

	refreshToken, err := rToken.SignedString([]byte(t.tokenSecret))
	if err != nil {
		t.log.Error(logMsg+"rToken.SignedString", logger.Error(err))
		return "", "", err
	}
	return accessToken, refreshToken, nil
}

func (t *TokenUseCase) changeRole(cbid int) (domain.UserRole, error) {
	var (
		logMsg = "uc.JWT.changeRole "
		result domain.UserRole
	)
	userInfo, err := t.sign.GetUserInfo(context.Background(), cbid)
	if err != nil {
		t.log.Error(logMsg+"t.sign.GetUserInfo", logger.Error(err))
		return result, err
	}

	roleName, err := t.role.GetRole(context.Background(), userInfo.Role)
	if err != nil {
		t.log.Error(logMsg+"t.sign.GetRole", logger.Error(err))
		return result, err
	}

	result = domain.UserRole{
		Role:   roleName.Role,
		Code:   userInfo.Role,
		Direct: userInfo.Direct,
	}

	return result, nil

}
