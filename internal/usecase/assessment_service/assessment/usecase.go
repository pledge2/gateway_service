package assessment

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type Assessment interface {
	StoreMainInfo(ctx context.Context, assessment domain.AssessmentStore) (domain.IdResponse, error)
	GetPhotos(ctx context.Context, id string) (domain.AssessmentPhotos, error)
	FastAssessment(ctx context.Context, assessment domain.AssessmentStore) (domain.AssessmentResult, error)
	OfficialAssessment(ctx context.Context, assessment domain.AssessmentStore) (domain.AssessmentResult, error)
	FindAssessment(ctx context.Context, id string) (domain.Assessment, error)
	FindAssessmentByStatus(ctx context.Context, statusCode, limit, page, cbid, roleCode, branchID, direct int) (domain.AssessmentPagination, error)
	CountStatusAssessment(ctx context.Context, cbid, branchID, roleCode, direct int) ([]domain.AssessmentStatus, error)
	ChangeAssessmentStatus(ctx context.Context, aStatus domain.AssessmentStatus) (domain.Result, error)
	AssessmentAnalogsList(ctx context.Context, id string) ([]domain.GetAnalog, error)
	AssessmentData(ctx context.Context, cbid, texPassSeria, texPassNum string) (domain.GovInfoWithCBID, error)
	DeleteAssessment(ctx context.Context, id string) (domain.Result, error)

	GovCarInfo(ctx context.Context, carInfo domain.TechNumberParams) (domain.GovUzResponse, error)
	UpdateGovData(ctx context.Context, assessmentID string, info domain.GovUzStore) (domain.Result, error)
}

type UsersI interface {
	GetUserInfo(ctx context.Context, cbid int) (domain.UserInfo, error)
}

type UseCase struct {
	assessment Assessment
	user       UsersI
	log        logger.Logger
}

func New(s Assessment, i UsersI, log logger.Logger) *UseCase {
	return &UseCase{
		assessment: s,
		user:       i,
		log:        log,
	}
}

func (s *UseCase) OfficialAssessment(ctx context.Context, assessment domain.AssessmentStore) (domain.AssessmentResult, error) {
	var (
		logMsg = "uc.Assessment.OfficialAssessment "
	)

	result, err := s.assessment.OfficialAssessment(ctx, assessment)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.OfficialAssessment", logger.Error(err))

		return domain.AssessmentResult{}, err
	}

	return result, nil
}

func (s *UseCase) FastAssessment(ctx context.Context, assessment domain.AssessmentStore) (domain.AssessmentResult, error) {
	var (
		logMsg = "uc.Assessment.FastAssessment "
	)

	result, err := s.assessment.FastAssessment(ctx, assessment)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.FastAssessment", logger.Error(err))

		return domain.AssessmentResult{}, err
	}

	return result, nil
}

func (s *UseCase) StoreMainInfo(ctx context.Context, assessment domain.AssessmentStore) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Assessment.StoreMainInfo "
	)

	result, err := s.assessment.StoreMainInfo(ctx, assessment)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.StoreMainInfo", logger.Error(err))
		return domain.IdResponse{}, err
	}

	return result, nil
}

func (s *UseCase) GovCarInfo(ctx context.Context, carInfo domain.TechNumberParams) (domain.GovUzResponse, error) {
	var (
		logMsg = "uc.Assessment.GovCarInfo "
	)

	result, err := s.assessment.GovCarInfo(ctx, carInfo)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.GovCarInfo", logger.Error(err))

		return domain.GovUzResponse{}, err
	}

	return result, nil
}

func (s *UseCase) GetPhotos(ctx context.Context, assessmentID string) (domain.AssessmentPhotos, error) {
	var (
		logMsg = "uc.Assessment.GetPhotos "
	)

	result, err := s.assessment.GetPhotos(ctx, assessmentID)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.GetPhotos", logger.Error(err))

		return domain.AssessmentPhotos{}, err
	}

	return result, nil
}

func (s *UseCase) FindAssessment(ctx context.Context, assessmentID string) (domain.Assessment, error) {
	var (
		logMsg = "uc.Assessment.FindAssessment "
	)

	result, err := s.assessment.FindAssessment(ctx, assessmentID)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.FindAssessment", logger.Error(err))

		return domain.Assessment{}, err
	}
	return result, nil
}

func (s *UseCase) FindAssessmentByStatus(ctx context.Context, status, limit, page, cbid, roleCode, branchID, direct int) (domain.AssessmentPagination, error) {
	var (
		logMsg   = "uc.Assessment.FindAssessmentByStatus "
		empNames domain.UserInfo
		result   domain.AssessmentPagination
	)
	result, err := s.assessment.FindAssessmentByStatus(ctx, status, limit, page, cbid, roleCode, branchID, direct)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.FindAssessmentByStatus", logger.Error(err))

		return result, err
	}

	var aStatus []domain.Assessment
	for _, val := range result.Assessments {
		if val.CBID != 1 {
			empNames, err = s.user.GetUserInfo(ctx, val.CBID)
			if err != nil {
				s.log.Error(logMsg+"s.user.GetUserInfo", logger.Error(err))

				return result, err
			}

			val.EmpName = empNames.Name
		} else {
			val.EmpName = "Demo"
		}

		data := domain.Assessment{
			ID:     val.ID,
			CBID:   val.CBID,
			GID:    val.GID,
			Status: val.Status,
			GovUz: domain.GovUz{
				OwnerFullname:      val.GovUz.OwnerFullname,
				VehiclePlateNumber: val.GovUz.VehiclePlateNumber,
				VehicleKuzov:       val.GovUz.VehicleKuzov,
			},
			EmpName: val.EmpName,
			TransportView: domain.TransportView{
				TransportName: val.TransportView.TransportName,
			},
			AllPhotosUrl: val.AllPhotosUrl,
			CreatedAt:    val.CreatedAt,
			CardID:       val.CardID,
		}
		aStatus = append(aStatus, data)
	}

	result.Assessments = aStatus

	return result, nil
}

func (s *UseCase) CountStatusAssessment(ctx context.Context, cbid, branchID, roleCode, direct int) ([]domain.AssessmentStatus, error) {
	var (
		logMsg = "uc.Assessment.CountStatusAssessment "
		result []domain.AssessmentStatus
	)
	result, err := s.assessment.CountStatusAssessment(ctx, cbid, branchID, roleCode, direct)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.CountStatusAssessment", logger.Error(err))

		return result, err
	}
	return result, nil
}

func (s *UseCase) ChangeAssessmentStatus(ctx context.Context, aStatus domain.AssessmentStatus) (domain.Result, error) {
	var (
		logMsg = "uc.Assessment.ChangeAssessmentStatus "
	)
	result, err := s.assessment.ChangeAssessmentStatus(ctx, aStatus)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.ChangeAssessmentStatus", logger.Error(err))

		return domain.Result{}, err
	}

	return result, nil
}

func (s *UseCase) AssessmentAnalogsLists(ctx context.Context, assessmentID string) ([]domain.GetAnalog, error) {
	var (
		logMsg = "uc.Assessment.AssessmentAnalogsLists "
		result []domain.GetAnalog
	)

	result, err := s.assessment.AssessmentAnalogsList(ctx, assessmentID)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.AssessmentAnalogsList", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (s *UseCase) GetAssessmentData(ctx context.Context, cbid string, texPassSeria string, texPassNum string) (domain.GovInfoWithCBID, error) {
	var (
		logMsg = "uc.Assessment.GetAssessmentData "
	)

	result, err := s.assessment.AssessmentData(ctx, cbid, texPassSeria, texPassNum)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.AssessmentData", logger.Error(err))

		return domain.GovInfoWithCBID{}, err
	}

	return result, nil
}

func (s *UseCase) UpdateGovData(ctx context.Context, assessmentID string, store domain.GovUzStore) (domain.Result, error) {
	var (
		logMsg = "uc.Assessment.UpdateGovData "
	)

	result, err := s.assessment.UpdateGovData(ctx, assessmentID, store)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.UpdateGovData", logger.Error(err))

		return domain.Result{}, err
	}

	return result, nil
}

func (s *UseCase) DeleteAssessment(ctx context.Context, assessmentID string) (domain.Result, error) {
	var (
		logMsg = "uc.Assessment.DeleteAssessment "
	)

	result, err := s.assessment.DeleteAssessment(ctx, assessmentID)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.DeleteAssessment", logger.Error(err))

		return domain.Result{}, err
	}

	return result, nil
}
