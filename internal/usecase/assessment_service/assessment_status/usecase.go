package assessment_status

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type StatusI interface {
	StatusStore(ctx context.Context, aStatus domain.AssessmentStatus) (domain.IdResponse, error)
	FindAllStatus(ctx context.Context) ([]domain.AssessmentStatus, error)
}

type UseCase struct {
	status StatusI
	log    logger.Logger
}

func New(s StatusI, log logger.Logger) *UseCase {
	return &UseCase{
		status: s,

		log: log,
	}
}

func (s *UseCase) StoreStatus(ctx context.Context, aStatus domain.AssessmentStatus) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Status.StoreStatus "
		result domain.IdResponse
	)

	result, err := s.status.StatusStore(ctx, aStatus)
	if err != nil {
		s.log.Error(logMsg+"r.service.StatusStore", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (s *UseCase) FindAllStatus(ctx context.Context, role int) ([]domain.AssessmentStatus, error) {
	var logMsg = "uc.Assessment.FindAllStatus "

	info, err := s.status.FindAllStatus(ctx)
	if err != nil {
		s.log.Error(logMsg+"s.assessment.FindAllAssessment", logger.Error(err))

		return nil, err
	}

	result := filterStatus(role, info)

	return result, nil
}

func filterStatus(role int, data []domain.AssessmentStatus) []domain.AssessmentStatus {
	var result []domain.AssessmentStatus
	switch role {
	case 100:
		{
			result = append(result, data...)
			return result
		}
	case 20:
		{
			for _, status := range data {

				if status.StatusCode == 4 {

					result = append(result, status)
				}
			}
			return result
		}
	case 40:
		{
			for _, status := range data {

				if status.StatusCode == 2 || status.StatusCode == 4 || status.StatusCode == 5 {

					result = append(result, status)
				}
			}
			return result
		}
	case 60:
		{
			for _, status := range data {

				if status.StatusCode == 4 {

					result = append(result, status)
				}
			}
			return result
		}
	case 80:
		{
			for _, status := range data {

				if status.StatusCode == 2 || status.StatusCode == 4 || status.StatusCode == 5 {

					result = append(result, status)
				}
			}
			return result
		}
	default:
		{
			return result

		}
	}
}
