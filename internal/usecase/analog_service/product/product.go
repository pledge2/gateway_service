package product

import (
	"context"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type Product interface {
	ProductStore(ctx context.Context, product domain.StoreProduct) (domain.IdResponse, error)
	ProductFindOne(ctx context.Context, id string) (domain.ProductResponse, error)
	FindAllProduct(ctx context.Context) ([]domain.ProductResponse, error)
	ProductUpdate(ctx context.Context, id string, product domain.UpdateProduct) (domain.Result, error)
	UpdateProductPhoto(ctx context.Context, id string, product domain.UpdateProductPhoto) (domain.Result, error)
	UpdateProductStatus(ctx context.Context, id string, product domain.UpdateProductStatus) (domain.Result, error)
	ProductDelete(ctx context.Context, id string) (domain.Result, error)
	ChangeProductPhotosFolder(ctx context.Context) (domain.Result, error)
}

type UseCase struct {
	product Product
	log     logger.Logger
}

func New(product Product, log logger.Logger) *UseCase {
	return &UseCase{
		product: product,
		log:     log,
	}
}

func (uc *UseCase) StoreProduct(ctx context.Context, product domain.StoreProduct) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Product.StoreProduct "
		result domain.IdResponse
	)

	result, err := uc.product.ProductStore(ctx, product)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.ProductStore", logger.Error(err))

		return result, err
	}
	return result, nil
}

func (uc *UseCase) FindOneProduct(ctx context.Context, id string) (domain.ProductResponse, error) {
	var (
		logMsg = "uc.Product.ProductFindOne "
		result domain.ProductResponse
	)

	result, err := uc.product.ProductFindOne(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.ProductFindOne", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) FindAllProduct(ctx context.Context) ([]domain.ProductResponse, error) {
	var (
		logMsg = "uc.Analog.FindAllProduct "
		result []domain.ProductResponse
	)

	result, err := uc.product.FindAllProduct(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.FindAllProduct", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateProductPhoto(ctx context.Context, id string, product domain.UpdateProductPhoto) (domain.Result, error) {
	var (
		logMsg = "uc.Product.UpdateProductPhoto "
		result domain.Result
	)

	result, err := uc.product.UpdateProductPhoto(ctx, id, product)
	if err != nil {
		uc.log.Error(logMsg+"uc.product.UpdateProductPhoto", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ProductUpdate(ctx context.Context, id string, product domain.UpdateProduct) (domain.Result, error) {
	var (
		logMsg = "uc.Product.UpdateProductPhotoUrl "
		result domain.Result
	)

	result, err := uc.product.ProductUpdate(ctx, id, product)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.ProductUpdate", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) UpdateProductStatus(ctx context.Context, id string, product domain.UpdateProductStatus) (domain.Result, error) {
	var (
		logMsg = "uc.Product.UpdateProductStatus "
		result domain.Result
	)

	result, err := uc.product.UpdateProductStatus(ctx, id, product)
	if err != nil {
		uc.log.Error(logMsg+"r.service.UpdateProductStatus", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ProductDelete(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Product.ProductDelete "
		result domain.Result
	)

	result, err := uc.product.ProductDelete(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.ProductDelete", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ChangeProductPhotosFolder(ctx context.Context) (domain.Result, error) {
	var (
		logMsg = "uc.Product.ChangeProductPhotosFolder "
	)

	result, err := uc.product.ChangeProductPhotosFolder(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ChangeAnalogPdfFolder", logger.Error(err))

		return domain.Result{}, err
	}

	return result, nil
}
