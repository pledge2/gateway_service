package analog

import (
	"context"
	"fmt"
	"gateway_service/internal/domain"
	"gateway_service/internal/pkg/logger"
)

type Analog interface {
	AnalogStore(ctx context.Context, analog domain.Analog) (domain.IdResponse, error)
	AnalogFindOne(ctx context.Context, id string) (domain.GetAnalog, error)
	AnalogFindView(ctx context.Context, id string) (domain.GetAnalog, error)
	AnalogFindList(ctx context.Context, productID, typeID string, limit, page int) (domain.AnalogListResp, error)
	AnalogUpdate(ctx context.Context, id string, analog domain.Analog) (domain.Result, error)
	AnalogUpdateStatus(ctx context.Context, analog domain.UpdateAnalogStatus) (domain.Result, error)
	AnalogDelete(ctx context.Context, id string) (domain.Result, error)
	AnalogSearch(ctx context.Context, analogID, typeID, modelID, nameID, positionID, fromYear, toYear, fuel,
		fromSpeedometer, toSpeedometer, validity, fromPrice, toPrice, activeValue, fromDate, toDate, user, limit, page string) (domain.AnalogListResp, error)
	ChangeAnalogPdfFolder(ctx context.Context) (domain.Result, error)
	ChangeAnalogPhotosFolder(ctx context.Context) (domain.Result, error)
	ChangeAssessmentPhotoFolder(ctx context.Context) (domain.AssessmentPhotoFolderResponse, error)
}

type UsersI interface {
	GetUserInfo(ctx context.Context, cbid int) (domain.UserInfo, error)
	GetUserName(ctx context.Context, id string) (domain.UserInfo, error)
}

type UseCase struct {
	analog Analog
	users  UsersI
	log    logger.Logger
}

func New(analog Analog, users UsersI, log logger.Logger) *UseCase {
	return &UseCase{
		analog: analog,
		users:  users,
		log:    log,
	}
}

func (uc *UseCase) StoreAnalog(ctx context.Context, data domain.Analog) (domain.IdResponse, error) {
	var (
		logMsg = "uc.Analog.StoreAnalog "
		result domain.IdResponse
	)

	result, err := uc.analog.AnalogStore(ctx, data)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.AnalogStore", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) AnalogFindView(ctx context.Context, id string) (domain.GetAnalog, error) {
	var (
		logMsg = "uc.Analog.AnalogFindView "
		result domain.GetAnalog
	)

	response, err := uc.analog.AnalogFindView(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.AnalogFindView", logger.Error(err))

		return result, err
	}

	user, err := uc.users.GetUserName(ctx, response.Employee)
	if err != nil {
		uc.log.Error(logMsg+"uc.users.GetUserName", logger.Error(err))

		return result, err
	}

	result = domain.GetAnalog{
		ID:            response.ID,
		Type:          response.Type,
		Model:         response.Model,
		Name:          response.Name,
		Position:      response.Position,
		Speedometer:   response.Speedometer,
		Year:          response.Year,
		Fuel:          response.Fuel,
		SellPrice:     response.SellPrice,
		Price:         response.Price,
		Condition:     response.Condition,
		Active:        response.Active,
		Comment:       response.Comment,
		SourceFileUrl: response.SourceFileUrl,
		SourceFile:    response.SourceFile,
		Employee:      fmt.Sprint(user.Name, "( ", user.CBID, " )"),
		CreatedAt:     response.CreatedAt,
		UpdatedAt:     response.UpdatedAt,
	}

	for _, photo := range response.AllPhotosUrl.PhotosUrl {
		value := domain.PhotosUrl{
			PhotoUrl: domain.PhotoUrl{
				PhotoUrl: photo.PhotoUrl.PhotoUrl,
			},
			Source: domain.Source{
				LAT:   photo.Source.LAT,
				LON:   photo.Source.LON,
				Title: photo.Source.Title,
				Order: photo.Source.Order,
			},
		}
		result.AllPhotosUrl.PhotosUrl = append(result.AllPhotosUrl.PhotosUrl, value)
	}

	return result, nil
}

func (uc *UseCase) AnalogFindOne(ctx context.Context, id string) (domain.GetAnalog, error) {
	var (
		logMsg = "uc.Analog.AnalogFindOne "
		result domain.GetAnalog
	)

	result, err := uc.analog.AnalogFindOne(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.AnalogFindOne", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) AnalogFindList(ctx context.Context, productID, typeID string, limit, page int) (domain.AnalogListResp, error) {
	var (
		logMsg = "uc.Analog.AnalogFindList "
		result domain.AnalogListResp
	)

	result, err := uc.analog.AnalogFindList(ctx, productID, typeID, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.AnalogFindList", logger.Error(err))
		return result, err
	}

	return result, nil
}

func (uc *UseCase) AnalogUpdate(ctx context.Context, id string, req domain.Analog) (domain.Result, error) {
	var (
		logMsg = "uc.Analog.AnalogUpdate "
		result domain.Result
	)

	result, err := uc.analog.AnalogUpdate(ctx, id, req)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.AnalogUpdate", logger.Error(err))
		return result, err
	}

	return result, nil
}

func (uc *UseCase) AnalogUpdateStatus(ctx context.Context, status domain.UpdateAnalogStatus) (domain.Result, error) {
	var (
		logMsg = "uc.Analog.AnalogUpdateStatus "
		result domain.Result
	)

	result, err := uc.analog.AnalogUpdateStatus(ctx, status)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.AnalogUpdateStatus", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) AnalogDelete(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg = "uc.Analog.AnalogDelete "
		result domain.Result
	)

	result, err := uc.analog.AnalogDelete(ctx, id)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.AnalogDelete", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) AnalogSearch(ctx context.Context, analogID, typeID, modelID, nameID, positionID, fromYear, toYear,
	fuel, fromSpeedometer, toSpeedometer, validity, fromPrice, toPrice, activeValue,
	fromDate, toDate, user, limit, page string) (domain.AnalogListResp, error) {
	var (
		logMsg = "uc.Analog.AnalogSearch "
		result domain.AnalogListResp
	)

	result, err := uc.analog.AnalogSearch(ctx, analogID, typeID, modelID, nameID, positionID, fromYear, toYear, fuel,
		fromSpeedometer, toSpeedometer, validity, fromPrice, toPrice, activeValue, fromDate, toDate, user, limit, page)
	if err != nil {
		uc.log.Error(logMsg+"uc.analog.AnalogSearch", logger.Error(err))

		return result, err
	}

	return result, nil
}

func (uc *UseCase) ChangeAnalogPdfFolder(ctx context.Context) (domain.Result, error) {
	var (
		logMsg = "uc.Analog.ChangeAnalogPdfFolder "
	)

	result, err := uc.analog.ChangeAnalogPdfFolder(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ChangeAnalogPdfFolder", logger.Error(err))

		return domain.Result{}, err
	}

	return result, nil
}

func (uc *UseCase) ChangeAnalogPhotosFolder(ctx context.Context) (domain.Result, error) {
	var (
		logMsg = "uc.Analog.ChangeAnalogPhotosFolder "
	)

	result, err := uc.analog.ChangeAnalogPhotosFolder(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ChangeAnalogPhotosFolder", logger.Error(err))

		return domain.Result{}, err
	}

	return result, nil
}

func (uc *UseCase) ChangeAssessmentPhotoFolder(ctx context.Context) (domain.AssessmentPhotoFolderResponse, error) {
	var (
		logMsg = "uc.Analog.ChangeAssessmentPhotoFolder "
	)

	result, err := uc.analog.ChangeAssessmentPhotoFolder(ctx)
	if err != nil {
		uc.log.Error(logMsg+"uc.service.ChangeAnalogPdfFolder", logger.Error(err))

		return domain.AssessmentPhotoFolderResponse{}, err
	}

	return result, nil
}
