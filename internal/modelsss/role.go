package models

import "time"

type UserRole struct {
	ID       string    `json:"-"`
	Role     string    `json:"role"`
	Code     int       `json:"code"`
	Direct   int       `json:"direct"`
	Active   bool      `json:"active"`
	CreateAt time.Time `json:"-"`
	UpdateAt time.Time `json:"-"`
}

type UserRoleResp struct {
	ID       string    `json:"id"`
	Index    int       `json:"index"`
	Role     string    `json:"role"`
	Code     int       `json:"code"`
	Active   bool      `json:"active"`
	PostCode int       `json:"post_code"`
	CBID     int       `json:"cbid"`
	CreateAt time.Time `json:"created_at"`
	UpdateAt time.Time `json:"updated_at"`
}

type Role struct {
	RoleName string `json:"role_name"`
}

type UpdateRoleStatus struct {
	Active bool `json:"active"`
}
