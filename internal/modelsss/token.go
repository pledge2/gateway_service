package models

type TokenRequest struct {
	Token string `json:"token" example:"valid token"`
}

type TokenResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}
