package models

import (
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/utils"
	"regexp"
)

type SignUp struct {
	Email      string `json:"e_mail" example:"@hamkorbank.uz" binding:"required,email"`
	Password   string `json:"password" example:"password" binding:"required,min=8"`
	RePassword string `json:"re_password" example:"re_password" binding:"required,min=8"`
	CBID       int    `json:"cbid" example:"44444" binding:"required"`
	DeviceType string `json:"device_type" example:"web / ios / android" binding:"required,min=3"`
	DeviceNum  string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265" binding:"required,min=7"`
}

type SignIn struct {
	Password    string `json:"password" example:"password" binding:"required"`
	AccessToken string `json:"-" example:"valid token"`
	CBID        int    `json:"cbid" example:"44444" binding:"required"`
	DeviceType  string `json:"device_type" example:"web / ios / android" binding:"required"`
	DeviceNum   string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265" binding:"required"`
}

type CheckCode struct {
	AccessToken string `json:"-" example:"valid token"`
	Code        string `json:"code" example:"7656" binding:"required"`
	CBID        int    `json:"cbid" example:"44444" binding:"required"`
	DeviceType  string `json:"device_type" example:"web / ios / android" binding:"required"`
	DeviceNum   string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265" binding:"required"`
}
type CheckCbid struct {
	AccessToken string `json:"-" example:"valid token"`
	CBID        int    `json:"cbid" example:"44444" binding:"required"`
	DeviceType  string `json:"device_type" example:"web / ios / android" binding:"required"`
	DeviceNum   string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265" binding:"required"`
}

type ChangePassword struct {
	Password    string `json:"password" example:"44444" binding:"required"`
	RePassword  string `json:"re_password" example:"44444" binding:"required"`
	AccessToken string `json:"-" example:"valid token"`
	CBID        int    `json:"cbid" example:"44444"`
	DeviceType  string `json:"device_type" example:"web / ios / android"`
	DeviceNum   string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265"`
}

type ResendCode struct {
	AccessToken string `json:"-" example:"valid token"`
	Email       string `json:"-" example:"@hamkorbank.uz"`
	Code        string `json:"-" example:"44444"`
	CBID        int    `json:"cbid" example:"44444" binding:"required"`
	DeviceType  string `json:"device_type" example:"web / ios / android"`
	DeviceNum   string `json:"device_num" example:"f43a187c-7969-11ee-b962-0242ac120002 / 423265"`
}
type SendCode struct {
	CBID  string `json:"-" example:"44444"`
	Email string `json:"-" example:"@hamkorbank.uz"`
}

type Result struct {
	Result bool `json:"result"`
}

type Access struct {
	Active int `json:"active"`
}

func (u *SignUp) Validate() error {

	if u.CBID < 1 {
		return errs.ErrCbidFormat
	}
	var validNum = regexp.MustCompile(`[a-z0-9]$`)
	switch u.DeviceType {
	case "web":
		if utils.IsValidUUID(u.DeviceNum) {
			return nil
		} else {
			return errs.ErrDeviceFormat
		}
		// if net.ParseIP(u.DeviceNum) == nil {
		// 	return errs.ErrDeviceFormat
		// }
		// return nil
	case "android":
		if validNum.MatchString(u.DeviceNum) {
			return nil
		} else {
			return errs.ErrDeviceFormat
		}

	case "ios":
		if utils.IsValidUUID(u.DeviceNum) {
			return nil
		} else {
			return errs.ErrDeviceFormat
		}
	default:
		return errs.ErrDeviceFormat
	}
}
