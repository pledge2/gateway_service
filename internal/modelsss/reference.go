package models

// Type
type TypeStore struct {
	Type      string `json:"type"`
	Active    bool   `json:"active"`
	PhotoName string `json:"photo_name"`
}

type TransportTypeResp struct {
	ID          string `json:"id"`
	Index       int    `json:"index"`
	Type        string `json:"type"`
	Active      bool   `json:"active"`
	FastURL     string `json:"fast_url"`
	OfficialURL string `json:"official_url"`
	PhotoName   string `json:"photo_name"`
	PhotoUrl    string `json:"photo_url"`
	CreateAt    string `json:"created_at"`
	UpdateAt    string `json:"updated_at"`
}

type TypeRespWithPagination struct {
	Type       []TransportTypeResp `json:"Type"`
	TotalPages int                 `json:"total_pages"`
}

type TypeUpdate struct {
	Type      string `json:"type"`
	PhotoName string `json:"photo_name"`
	Active    bool   `json:"active"`
}

type UpdateTypePhoto struct {
	PhotoName string `json:"photo_name"`
}

type UpdateTypeStatus struct {
	Active bool `json:"active"`
}

// Model

// Name

// Position

// Fuel

// Validity
type ValidityStore struct {
	Validity string `json:"validity"`
	Code     int    `json:"code"`
	Active   bool   `json:"active"`
}

type ValidityResponse struct {
	ID       string `json:"id"`
	Validity string `json:"validity"`
	Code     int    `json:"code"`
	Active   bool   `json:"active"`
	CreateAt string `json:"created_at"`
}

type TransportValidityResp struct {
	Validity string `json:"validity"`
	Code     int    `json:"code"`
}

type ValdityUpdate struct {
	Validity string `json:"validity"`
	Code     int    `json:"code"`
	Active   bool   `json:"active"`
}

type UpdateValidityStatus struct {
	Active bool `json:"active"`
}

// Discont
type DiscountParam struct {
	Code      int    `json:"code"`
	Percent   int    `json:"percent"`
	CreatedAt string `json:"created_at"`
}

type Discounts struct {
	ID        string          `json:"id"`
	NameID    string          `json:"name_id"`
	Discounts []DiscountParam `json:"discounts"`
}

type DiscountUpdate struct {
	Code    int `json:"code"`
	Percent int `json:"percent"`
}

type DiscountCreate struct {
	NameID string `json:"name_id"`
}
