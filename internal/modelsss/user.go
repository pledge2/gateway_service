package models

type UserList struct {
	ID   string `json:"id"`
	CBID int    `json:"cbid"`
}

type PositionList struct {
	Index        int    `json:"index"`
	PostCode     string `json:"post_code"`
	DepName      string `json:"dep_name"`
	PositionName string `json:"position_name"`
	Direction    string `json:"direction"`
}

type CBIDList struct {
	Index     int    `json:"index"`
	CBID      int    `json:"cbid"`
	BranchID  int    `json:"branch_id"`
	Name      string `json:"name"`
	Direction string `json:"direction"`
	ExpiredAt string `json:"expired_at"`
}

type PositionPostCode struct {
	PostCode string `json:"post_code"`
	Direct   int    `json:"direct"`
}

type DeletePostCode struct {
	PostCode string `json:"post_code"`
}

type PositionCBID struct {
	CBID      int    `json:"cbid"`
	Direct    int    `json:"direct"`
	ExpiredAt string `json:"expired_at"`
}

type DeleteCBID struct {
	CBID int `json:"cbid"`
}

type EmpPostCode struct {
	DepName  string `json:"dep_name"`
	PostName string `json:"post_name"`
}

type EmpCBID struct {
	CBID     int    `json:"cbid"`
	BranchID int    `json:"branch_id"`
	Name     string `json:"name"`
}

type UserInfo struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	CBID     int    `json:"cbid"`
	Direct   int    `json:"direct"`
	Role     int    `json:"role"`
	Position string `json:"position"`
	BranchID int    `json:"branch_id"`
}

type UserName struct {
	CBID int    `json:"cbid"`
	Name string `json:"name"`
}

type UsersList struct {
	ID            string   `json:"id"`
	Index         int      `json:"index"`
	FIO           string   `json:"fio"`
	Email         string   `json:"email"`
	Devices       []string `json:"devices"`
	DevicesStatus []string `json:"devices_status"`
	Role          string   `json:"role"`
	BranchID      int      `json:"branch_id"`
	CreatedAt     string   `json:"created_at"`
}

type UsersPagination struct {
	UsersList  []UsersList
	TotalPages int `json:"total_pages"`
}

type UpdateUserEmail struct {
	ID    string `json:"-"`
	Email string `json:"email"`
}
