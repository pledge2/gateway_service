package models

type FileUrl struct {
	File string `json:"file"`
}

type MultipleFiles struct {
	Files []string `json:"files"`
}
