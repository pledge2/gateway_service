package models

import "time"

type OfficialAssessmentRequest struct {
	ID string `json:"id"`
	TokenRequest
}

type MainInfo struct {
	ID            string    `json:"-"`                                                   // baxolash id
	CBID          int       `json:"-"`                                                   // user id
	Direct        int       `json:"-"`                                                   // user id
	EXTCBID       int       `json:"-"`                                                   // user id
	BranchID      int       `json:"-"`                                                   // user id
	AID           []string  `json:"-"`                                                   // analoglar id
	PID           string    `json:"p_id" example:"a6bfc976-95a6-4891-a5e9-d546e4038d36"` // product id
	GID           string    `json:"-"`                                                   //Analog table bilan transport_deatils table ni reference_key
	IIBInfoID     string    `json:"-"`
	Status        int       `json:"-"` // baxolash statusi, xolati
	Type          string    `json:"type" example:"a49144b8-618b-4195-80ed-224662172683"`
	Model         string    `json:"model" example:"6542a6ff-44e6-490c-b7d6-2527d50e95fe"`
	Name          string    `json:"name" example:"7c33049e-f7e6-4dd0-8abe-2b7d7be13190"`
	Position      string    `json:"position" example:"fad11e6d-a76f-41b4-84f6-23b6ee1e0657"`
	Fuel          string    `json:"fuel" example:"f7f2ef3d-9ac2-42a9-a6ae-c7cff188a60f"`
	Year          int       `json:"year" example:"2022"`
	Speedometer   int       `json:"speedometer" example:"35000"`
	SpeedometerAt time.Time `json:"-"`
	Validity      int       `json:"validity" example:"0"`
	Price         int       `json:"-"`
	PriceAt       time.Time `json:"-"`
	// TokenRequest
}

type AllPhotoSource struct {
	PhotoSource []PhotoSource `json:"photo"`
}

type PhotoSource struct {
	Title    string `json:"title"`
	Order    int    `json:"order"`
	PhotoUrl string `json:"photo_url"`
	LAT      string `json:"lat"`
	LON      string `json:"lon"`
}

type IIBInfo struct {
	ID                  string    `json:"-"` // baxolash id
	OwnerType           int       `json:"owner_type"`
	OwnerInps           string    `json:"owner_inps"`
	Owner               string    `json:"owner"`
	OwnerDateBirth      string    `json:"owner_date_birth"`
	VehicleID           int       `json:"vehicle_id"`
	VehiclePlateNumber  string    `json:"vehicle_plate_number"`
	VehicleModel        string    `json:"vehicle_model"`
	VehicleColor        string    `json:"vehicle_color"`
	IIBRegistrationDate time.Time `json:"iib_registration_date"`
	Division            string    `json:"division"`
	VehicleYear         string    `json:"vehicle_year"`
	VehicleType         string    `json:"vehicle_type"`
	VehicleKuzov        string    `json:"vehicle_kuzov"`
	VehicleFullWeight   string    `json:"vehicle_full_weight"`
	VehicleEmptyWeight  string    `json:"vehicle_empty_weight"`
	VehicleMotor        string    `json:"vehicle_motor"`
	VehicleFuelType     string    `json:"vehicle_fuel_type"`
	VehicleSeats        string    `json:"vehicle_seats"`
	VehicleStands       string    `json:"vehicle_stands"`
	IIBPermitInfo       string    `json:"iib_permit_info"`
	Inspection          string    `json:"inspection"`
	// TokenRequest
}

type FastAssessmentRequest struct {
	Type        string `json:"type" example:"5f743c36-1704-4e43-8549-a0e70b490af3"`
	Model       string `json:"model" example:"dcb20848-2154-4446-b316-4a8339874b30"`
	Name        string `json:"name" example:"5e0cbb08-1082-468f-9c21-c7c3b4637af7"`
	Year        int    `json:"year" example:"2022"`
	Speedometer int    `json:"speedometer" example:"35000"`
	Validity    int    `json:"validity" example:"1"`
	Position    string `json:"position" example:"ddbb8bde-30e2-4419-b6e2-7d092af4355c"`
	Fuel        string `json:"fuel" example:"7fd08594-458d-4062-80ab-ec071e1285d5"`
	// TokenRequest
}

type AssessmentResponse struct {
	MarketPrice int    `json:"market_price"`
	CreditPrice int    `json:"credit_price"`
	Model       string `json:"transport_model"`
	Name        string `json:"transport_name"`
	Position    string `json:"transport_position"`
	Year        int    `json:"transport_year"`
}

type FindAssessment struct {
	ID                 string         `json:"id"`     // baxolash id
	CBID               int            `json:"cbid"`   // user id
	AIDs               []string       `json:"a_ids"`  // analoglar id
	PID                string         `json:"p_id"`   // product id
	GID                string         `json:"-"`      //Analog table bilan transport_deatils table ni reference_key
	Status             int            `json:"status"` // baxolash statusi, xolati
	VehicleKuzov       string         `json:"vehicle_kuzov"`
	OwnerPinfl         string         `json:"owner_pinfl"`
	OwnerFullname      string         `json:"owner_fullname"`
	VehicleModel       string         `json:"vehicle_model"`
	VehicleColor       string         `json:"vehicle_color"`
	VehicleMadeYear    int            `json:"vehicle_year"`
	VehicleMotor       string         `json:"vehicle_motor"`
	VehicleFullWeight  int            `json:"vehicle_full_weight"`
	VehicleEmptyWeight int            `json:"vehicle_empty_weight"`
	VehiclePlateNumber string         `json:"vehicle_plate_number"`
	VehiclePower       int            `json:"vehicle_power"`
	PassportNumber     string         `json:"passport_number"`
	PassportSeria      string         `json:"passport_seria"`
	VinNumber          string         `json:"vin_number"`
	Type               string         `json:"type"`
	Model              string         `json:"model"`
	Name               string         `json:"name"`
	Position           string         `json:"position"`
	Year               int            `json:"year"`
	Speedometer        int            `json:"speedometer"`
	Validity           int            `json:"validity"`
	Fuel               string         `json:"fuel"`
	MarketPrice        int            `json:"market_price"`
	CreditPrice        int            `json:"credit_price"`
	TransportModel     string         `json:"transport_model"`
	TransportName      string         `json:"transport_name"`
	TransportPosition  string         `json:"transport_position"`
	TransportYear      int            `json:"transport_year"`
	Price              int            `json:"price"`
	SellPrice          int            `json:"sell_price"`
	Source             AllPhotoSource `json:"photos"`
	Url                string         `json:"url"`
	IIBPermitInfo      string         `json:"iib_permit_info"`
}

type FindAllAssessment struct {
	ID            string         `json:"id"`     // baxolash id
	CBID          int            `json:"cbid"`   // user id
	AID           []string       `json:"a_id"`   // analoglar id
	PID           string         `json:"p_id"`   // product id
	GID           string         `json:"g_id"`   //Analog table bilan transport_deatils table ni reference_key
	Status        int            `json:"status"` // baxolash statusi, xolati
	Type          string         `json:"type"`
	Model         string         `json:"model"`
	Name          string         `json:"name"`
	Position      string         `json:"position"`
	Year          int            `json:"year"`
	Speedometer   int            `json:"speedometer"`
	SpeedometerAt time.Time      `json:"speedometer_at"`
	Validity      int            `json:"validity"`
	Fuel          string         `json:"fuel"`
	Price         int            `json:"price"`
	PriceAt       time.Time      `json:"price_at"`
	Source        AllPhotoSource `json:"source"`
}

type FindAssessmentByStatus struct {
	ID                 string         `json:"id"`                   // baxolash id
	CBID               int            `json:"cbid"`                 // user id
	GID                string         `json:"g_id"`                 //Analog table bilan transport_deatils table ni reference_key
	Status             int            `json:"-"`                    // baxolash statusi, xolati
	OwnerFullName      string         `json:"owner_fullname"`       //iib_info table
	VehiclePlateNumber string         `json:"vehicle_plate_number"` //iib_info table
	VehicleKuzov       string         `json:"vehicle_kuzov"`        //iib_info table
	EmpName            string         `json:"emp_name"`
	TransportName      string         `json:"transport_name"`
	Photos             AllPhotoSource `json:"photos"`
	CreatedAt          string         `json:"created_at"`
	CardID             string         `json:"card_id"`
}

type AssessmentResult struct {
	Assessments []FindAssessmentByStatus `json:"assessments"`
	TotalPages  int                      `json:"total_pages"`
	StatusName  string                   `json:"status_name"`
}

type StatusCount struct {
	// ID         string `json:"id"`
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
	Count      int    `json:"count"`
	// Url        string `json:"-"`
}

// Official

type GovCarInfo struct {
	AssessmentID   string `json:"assessment_id"`
	PassportNumber string `json:"passport_number"`
	PassportSeria  string `json:"passport_seria"`
	PlateNumber    string `json:"plate_number"`
}

type GovVinNumber struct {
	AssessmentID string `json:"assessment_id"`
	VinNumber    string `json:"vin_number"`
	// TokenRequest
}

type GovResponse struct {
	VinNumber          string `json:"vin_number"`
	PassportNumber     string `json:"passport_number"`
	PassportSeria      string `json:"passport_seria"`
	OwnerFullname      string `json:"owner_fullname"`
	OwnerPinfl         string `json:"owner_pinfl"`
	VehicleModel       string `json:"vehicle_model"`
	VehicleColor       string `json:"vehicle_color"`
	VehicleYear        int    `json:"vehicle_year"`
	VehicleMotor       string `json:"vehicle_motor"`
	VehicleFullWeight  int    `json:"vehicle_full_weight"`
	VehicleEmptyWeight int    `json:"vehicle_empty_weight"`
	VehiclePlateNumber string `json:"vehicle_plate_number"` //iib_info table
	VehiclePower       int    `json:"vehicle_power"`        //iib_info table
	IIBPermitInfo      string `json:"iib_permit_info"`
}

type AssessmentStatus struct {
	AssessmentID string `json:"assessment_id"`
	StatusCode   int    `json:"status_code"`
	Comment      string `json:"comment"`
}

type StatusResponse struct {
	ID         string `json:"id"`
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
	Count      int    `json:"count"`
	CreatedAt  string `json:"created_at"`
}

type StatusStore struct {
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
}

type AssessmentAnalogList struct {
	ID            string `json:"id"`
	Model         string `json:"model"`
	Name          string `json:"name"`
	Position      string `json:"position"`
	Speedometer   int    `json:"speedometer"`
	Year          int    `json:"year"`
	Fuel          string `json:"fuel"`
	SellPrice     int    `json:"sell_price"`
	Price         int    `json:"price"`
	Validity      string `json:"validity"`
	SourceFileUrl string `json:"source_file_url"`
	SourceFile    string `json:"source_file"`
}
