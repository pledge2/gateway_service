package models

type ProductStore struct {
	Product   string `json:"product"`
	PhotoName string `json:"photo_name"`
}

type ProductResponse struct {
	ID        string `json:"id"`
	Product   string `json:"product"`
	PhotoUrl  string `json:"photo_url"`
	Active    bool   `json:"active"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"uplated_at"`
}

type ProductUpdate struct {
	Product string `json:"product"`
	Active  bool   `json:"active"`
}

type UpdateProductPhoto struct {
	PhotoName string `json:"photo_name"`
}

type Source struct {
	LAT   string `json:"lat"`
	LON   string `json:"lon"`
	Title string `json:"title"`
	Order int    `json:"order"`
}

type PhotoName struct {
	PhotoName string `json:"photo_name"`
}

type PhotosName struct {
	PhotoName
	Source
}

type AllPhotosName struct {
	PhotosName []PhotosName `json:"photo"`
}

type AnalogStore struct {
	PID            string        `json:"p_id"`
	UID            string        `json:"u_id"`
	Type           string        `json:"type"`
	Model          string        `json:"model"`
	Name           string        `json:"name"`
	Position       string        `json:"position"`
	Speedometer    int           `json:"speedometer"`
	Year           int           `json:"year"`
	Fuel           string        `json:"fuel"`
	Price          int           `json:"price"`
	Validity       int           `json:"validity"`
	SellPrice      int           `json:"sell_price"`
	AllPhotosName  AllPhotosName `json:"photos"`
	SourceUrl      string        `json:"source_url"`
	Comment        string        `json:"comment"`
	SourceFileName string        `json:"source_file_name"`
	Active         bool          `json:"active"`
}

type IdResponse struct {
	ID string `json:"id"`
}

type Analog struct {
	ID            string       `json:"id"`
	Type          string       `json:"type"`
	Model         string       `json:"model"`
	Name          string       `json:"name"`
	Position      string       `json:"position"`
	Speedometer   int          `json:"speedometer"`
	Year          int          `json:"year"`
	Fuel          string       `json:"fuel"`
	SellPrice     int          `json:"sell_price"`
	Price         int          `json:"price"`
	Validity      int          `json:"validity"`
	Active        bool         `json:"active"`
	Comment       string       `json:"comment"`
	SourceFileUrl string       `json:"source_file_url"`
	SourceFile    string       `json:"source_file"`
	AllPhotosUrl  AllPhotosUrl `json:"photos"`
	Employee      string       `json:"employee"`
	CreatedAt     string       `json:"created_at"`
	UpdatedAt     string       `json:"updated_at"`
}

type AnalogView struct {
	ID            string       `json:"id"`
	Type          string       `json:"type"`
	Model         string       `json:"model"`
	Name          string       `json:"name"`
	Position      string       `json:"position"`
	Speedometer   int          `json:"speedometer"`
	Year          int          `json:"year"`
	Fuel          string       `json:"fuel"`
	SellPrice     int          `json:"sell_price"`
	Price         int          `json:"price"`
	Validity      string       `json:"validity"`
	Active        bool         `json:"active"`
	Comment       string       `json:"comment"`
	SourceFileUrl string       `json:"source_file_url"`
	SourceFile    string       `json:"source_file"`
	AllPhotosUrl  AllPhotosUrl `json:"photos"`
	Employee      string       `json:"employee"`
	CreatedAt     string       `json:"created_at"`
	UpdatedAt     string       `json:"updated_at"`
}

type AllPhotosUrl struct {
	PhotosUrl []PhotosUrl `json:"photo"`
}

type PhotosUrl struct {
	PhotoUrl
	Source
}
type PhotoUrl struct {
	PhotoUrl string `json:"photo_url"`
}

type AnalogListResponse struct {
	TotalPages int          `json:"total_pages"`
	AnalogList []AnalogList `json:"analog_list"`
}
type AnalogList struct {
	Index       int    `json:"index"`
	ID          string `json:"id"`
	Model       string `json:"model"`
	Name        string `json:"name"`
	Position    string `json:"position"`
	Speedometer int    `json:"speedometer"`
	Year        int    `json:"year"`
	Fuel        string `json:"fuel"`
	SellPrice   int    `json:"sell_price"`
	Active      bool   `json:"active"`
	CreatedAt   string `json:"created_at"`
}

type AnalogUpdate struct {
	ID             string        `json:"-"`
	PID            string        `json:"p_id"`
	UID            string        `json:"u_id"`
	Type           string        `json:"type"`
	Model          string        `json:"model"`
	Name           string        `json:"name"`
	Position       string        `json:"position"`
	Speedometer    int           `json:"speedometer"`
	Year           int           `json:"year"`
	Fuel           string        `json:"fuel"`
	Price          int           `json:"price"`
	Validity       int           `json:"validity"`
	SellPrice      int           `json:"sell_price"`
	AllPhotosName  AllPhotosName `json:"photos"`
	SourceName     string        `json:"source_name"`
	Comment        string        `json:"comment"`
	SourceFileName string        `json:"source_file_name"`
	Active         bool          `json:"active"`
}

type AnalogStatus struct {
	ID     string `json:"id"`
	Active bool   `json:"active"`
}

type AnalogUpdateStatus struct {
	Analog []AnalogStatus `json:"data"`
}

type UpdateProductStatus struct {
	Active string `json:"active"`
}
