package nutanix_integration

type fileName struct {
	FileName string `json:"file"`
}

type result struct {
	Result bool `json:"result"`
}
