package minio_integration

import (
	"bytes"
	"context"
	"encoding/json"
	"gateway_service/internal/config"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	service "gateway_service/internal/pkg/http"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/pkg/status"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

const (
	bucket              = "baholash"
	uploadFileEndPoint  = "/minio/upload/file"
	uploadPhotoEndPoint = "/minio/upload/photo"
	getFileEndPoint     = "/minio/get/file"
)

type Client struct {
	cfg    config.Config
	client *http.Client
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		cfg:    cfg,
		client: &http.Client{},
		log:    log,
	}
}

func (c *Client) UploadPhoto(ctx context.Context, fileHeader *multipart.FileHeader, folder, suffix, assessmentID, lat, lon string) (domain.Result, error) {
	var (
		logMsg     = "service.Minio.UploadPhoto "
		archiveUrl = c.cfg.ArchiveService + uploadPhotoEndPoint
		response   service.DBOResponse
		data       result
		result     domain.Result
	)

	fileExt := filepath.Ext(fileHeader.Filename)
	originalFileName := strings.TrimSuffix(filepath.Base(fileHeader.Filename), filepath.Ext(fileHeader.Filename)) + fileExt
	path := filepath.Join(".", "files")

	filePath := path + "/" + originalFileName

	file, _ := os.Open(filePath)

	defer file.Close()

	body := &bytes.Buffer{}

	writer := multipart.NewWriter(body)

	part, _ := writer.CreateFormFile("file", filepath.Base(file.Name()))
	_, err := io.Copy(part, file)
	if err != nil {
		c.log.Error(logMsg+"file io.Copy", logger.Error(err))

		return result, errs.ErrInternal
	}

	bucketName, _ := writer.CreateFormField("bucket")
	_, err = io.Copy(bucketName, bytes.NewBufferString(bucket))
	if err != nil {
		c.log.Error(logMsg+"bucket io.Copy", logger.Error(err))

		return result, errs.ErrInternal
	}

	folderName, _ := writer.CreateFormField("folder")
	_, err = io.Copy(folderName, bytes.NewBufferString(folder))
	if err != nil {
		c.log.Error(logMsg+"folder io.Copy", logger.Error(err))

		return result, errs.ErrInternal
	}

	suffixPicture, _ := writer.CreateFormField("suffix")
	_, err = io.Copy(suffixPicture, bytes.NewBufferString(suffix))
	if err != nil {
		c.log.Error(logMsg+"suffix io.Copy", logger.Error(err))

		return result, errs.ErrInternal
	}

	assessmentKey, _ := writer.CreateFormField("assessmentID")
	_, err = io.Copy(assessmentKey, bytes.NewBufferString(assessmentID))
	if err != nil {
		c.log.Error(logMsg+"assessmentID io.Copy", logger.Error(err))

		return result, errs.ErrInternal
	}

	latNum, _ := writer.CreateFormField("lat")
	_, err = io.Copy(latNum, bytes.NewBufferString(lat))
	if err != nil {
		c.log.Error(logMsg+"lat io.Copy", logger.Error(err))

		return result, errs.ErrInternal
	}

	lonNum, _ := writer.CreateFormField("lon")
	_, err = io.Copy(lonNum, bytes.NewBufferString(lon))
	if err != nil {
		c.log.Error(logMsg+"lon io.Copy", logger.Error(err))

		return result, errs.ErrInternal
	}

	err = writer.Close()
	if err != nil {
		c.log.Error(logMsg+"writer.Close", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, archiveUrl, body)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	request.Header.Add("Content-Type", writer.FormDataContentType())

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) UploadFile(ctx context.Context, fileHeader *multipart.FileHeader, folder string) (domain.FileName, error) {
	var (
		logMsg     = "service.Minio.UploadFile "
		archiveUrl = c.cfg.ArchiveService + uploadFileEndPoint
		response   service.DBOResponse
		data       fileName
		result     domain.FileName
	)

	fileExt := filepath.Ext(fileHeader.Filename)
	originalFileName := strings.TrimSuffix(filepath.Base(fileHeader.Filename), filepath.Ext(fileHeader.Filename)) + fileExt
	path := filepath.Join(".", "files")

	filePath := path + "/" + originalFileName

	file, err := os.Open(filePath)
	if err != nil {
		c.log.Error(logMsg+"os.Open", logger.Error(err))

		return result, errs.ErrInternal
	}

	defer file.Close()

	body := &bytes.Buffer{}

	writer := multipart.NewWriter(body)

	part, _ := writer.CreateFormFile("file", filepath.Base(file.Name()))
	_, err = io.Copy(part, file)
	if err != nil {
		c.log.Error(logMsg+"file io.Copy", logger.Error(err))

		return result, errs.ErrInternal
	}

	bucketName, _ := writer.CreateFormField("bucket")
	_, err = io.Copy(bucketName, bytes.NewBufferString(bucket))
	if err != nil {
		c.log.Error(logMsg+"bucket io.Copy", logger.Error(err))

		return result, errs.ErrInternal
	}

	folderName, _ := writer.CreateFormField("folder")
	_, err = io.Copy(folderName, bytes.NewBufferString(folder))
	if err != nil {
		c.log.Error(logMsg+"folder io.Copy", logger.Error(err))

		return result, errs.ErrInternal
	}

	err = writer.Close()
	if err != nil {
		c.log.Error(logMsg+"writer.Close", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, archiveUrl, body)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}
	request.Header.Add("Content-Type", writer.FormDataContentType())

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.FileName{
		FileName: data.FileName,
	}

	return result, nil
}

func (c *Client) GetFile(ctx context.Context, folder, file string) (domain.FileUrl, error) {
	var (
		logMsg   = "service.Minio.GetFile "
		url      = c.cfg.ArchiveService + getFileEndPoint + "?bucket=" + bucket + "&folder=" + folder + "&file=" + file
		response service.DBOResponse
		data     fileUrl
		result   domain.FileUrl
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Get", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))
		return result, errs.ErrInternal
	}

	result = domain.FileUrl{
		FileUrl: data.FileUrl,
	}

	return result, nil
}
