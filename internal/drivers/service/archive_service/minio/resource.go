package minio_integration

type fileUrl struct {
	FileUrl string `json:"url"`
}

type fileName struct {
	FileName string `json:"file"`
}

type result struct {
	Result bool `json:"result"`
}
