package product

type idResponse struct {
	ID string `json:"id"`
}

type result struct {
	Result bool `json:"result"`
}

type storeProduct struct {
	Product   string `json:"product"`
	PhotoName string `json:"photo_name"`
}

type productResponse struct {
	ID        string `json:"id"`
	Product   string `json:"product"`
	PhotoUrl  string `json:"photo_url"`
	Active    bool   `json:"active"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type productUpdate struct {
	Product string `json:"product"`
	Active  bool   `json:"active"`
}

type updateProductPhoto struct {
	PhotoName string `json:"photo_name"`
}

type updateProductStatus struct {
	Active bool `json:"active"`
}
