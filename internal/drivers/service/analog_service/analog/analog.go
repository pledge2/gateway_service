package analog

import (
	"bytes"
	"context"
	"encoding/json"
	"gateway_service/internal/config"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	service "gateway_service/internal/pkg/http"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/pkg/status"
	"io"
	"net/http"
	"strconv"
)

const (
	analogStoreEndPoint        = "/analog/store"
	analogFindOneEndPoint      = "/analog/find/one"
	analogFindViewEndPoint     = "/analog/find/view"
	analogFindListEndPoint     = "/analog/find/list"
	analogUpdateEndPoint       = "/analog/update"
	analogUpdateStatusEndPoint = "/analog/update/status"
	analogDeleteEndPoint       = "/analog/delete"
	analogSearchEndPoint       = "/analog/search"

	analogChangePdfFolder       = "/analog/change/pdf/folder"
	analogChangePhotosFolder    = "/analog/change/photos/folder"
	assessmentChangePhotoFolder = "/assessment/change/photo/folder"
)

type Client struct {
	cfg    config.Config
	client *http.Client
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		cfg:    cfg,
		client: &http.Client{},
		log:    log,
	}
}

func (c *Client) AnalogStore(ctx context.Context, analog domain.Analog) (domain.IdResponse, error) {
	var (
		logMsg   = "service.Analog.AnalogStore "
		url      = c.cfg.AnalogService + analogStoreEndPoint
		response service.DBOResponse
		req      storeAnalog
		data     idResponse
		result   domain.IdResponse
	)

	req = storeAnalog{
		PID:            analog.PID,
		UID:            analog.UID,
		Type:           analog.Type,
		Model:          analog.Model,
		Name:           analog.Name,
		Position:       analog.Position,
		Speedometer:    analog.Speedometer,
		Year:           analog.Year,
		Fuel:           analog.Fuel,
		Price:          analog.Price,
		Validity:       analog.Validity,
		SellPrice:      analog.SellPrice,
		SourceName:     analog.SourceName,
		Comment:        analog.Comment,
		SourceFileName: analog.SourceFileName,
		Active:         analog.Active,
		NameKey:        analog.NameKey,
	}

	for _, val := range analog.Photos.PhotosName {
		req.Photos.PhotosName = append(req.Photos.PhotosName, photosName{
			source: source{
				Title: val.Source.Title,
				Order: val.Source.Order,
				LAT:   val.Source.LAT,
				LON:   val.Source.LON,
			},
			photoName: photoName{
				PhotoName: val.PhotoName.PhotoName,
			},
		})
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.IdResponse{
		ID: data.ID,
	}

	return result, nil
}

func (c *Client) AnalogFindOne(ctx context.Context, id string) (domain.GetAnalog, error) {
	var (
		logMsg   = "service.Analog.AnalogFindOne "
		url      = c.cfg.AnalogService + analogFindOneEndPoint + "?id=" + id
		response service.DBOResponse
		data     getAnalog
		result   domain.GetAnalog
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.GetAnalog{
		ID:            data.ID,
		Type:          data.Type,
		Model:         data.Model,
		Name:          data.Name,
		Position:      data.Position,
		Speedometer:   data.Speedometer,
		Year:          data.Year,
		Fuel:          data.Fuel,
		SellPrice:     data.SellPrice,
		Price:         data.Price,
		Validity:      data.Validity,
		Active:        data.Active,
		Comment:       data.Comment,
		SourceFileUrl: data.SourceFileUrl,
		SourceFile:    data.SourceFile,
		Employee:      data.Employee,
		CreatedAt:     data.CreatedAt,
		UpdatedAt:     data.CreatedAt,
	}

	for _, val := range data.AllPhotosUrl.PhotosUrl {
		respPhotos := domain.PhotosUrl{
			Source: domain.Source{
				Title: val.source.Title,
				Order: val.source.Order,
				LAT:   val.source.LAT,
				LON:   val.source.LON,
			},
			PhotoUrl: domain.PhotoUrl{
				PhotoUrl: val.photoUrls.PhotoUrl,
			},
		}
		result.AllPhotosUrl.PhotosUrl = append(result.AllPhotosUrl.PhotosUrl, respPhotos)
	}

	return result, nil
}

func (c *Client) AnalogFindView(ctx context.Context, id string) (domain.GetAnalog, error) {
	var (
		logMsg   = "service.Analog.AnalogFindView "
		url      = c.cfg.AnalogService + analogFindViewEndPoint + "?id=" + id
		response service.DBOResponse
		data     viewAnalog
		result   domain.GetAnalog
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.GetAnalog{
		ID:            data.ID,
		Type:          data.Type,
		Model:         data.Model,
		Name:          data.Name,
		Position:      data.Position,
		Speedometer:   data.Speedometer,
		Year:          data.Year,
		Fuel:          data.Fuel,
		SellPrice:     data.SellPrice,
		Price:         data.Price,
		Condition:     data.Validity,
		Active:        data.Active,
		Comment:       data.Comment,
		SourceFileUrl: data.SourceFileUrl,
		SourceFile:    data.SourceFile,
		Employee:      data.Employee,
		CreatedAt:     data.CreatedAt,
		UpdatedAt:     data.CreatedAt,
	}

	for _, val := range data.AllPhotosUrl.PhotosUrl {
		respPhotos := domain.PhotosUrl{
			Source: domain.Source{
				Title: val.source.Title,
				Order: val.source.Order,
				LAT:   val.source.LAT,
				LON:   val.source.LON,
			},
			PhotoUrl: domain.PhotoUrl{
				PhotoUrl: val.photoUrls.PhotoUrl,
			},
		}
		result.AllPhotosUrl.PhotosUrl = append(result.AllPhotosUrl.PhotosUrl, respPhotos)
	}

	return result, nil
}

func (c *Client) AnalogFindList(ctx context.Context, productID, typeID string, limit, page int) (domain.AnalogListResp, error) {
	var (
		logMsg = "service.Analog.AnalogFindList "
		url    = c.cfg.AnalogService + analogFindListEndPoint +
			"?product_id=" + productID + "&type_id=" + typeID +
			"&limit=" + strconv.Itoa(limit) + "&page=" + strconv.Itoa(page)
		response service.DBOResponse
		data     analogListResponse
		result   domain.AnalogListResp
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}
	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data.AnalogList {
		aData := domain.AnalogList{
			Index:       val.Index,
			ID:          val.ID,
			Model:       val.Model,
			Name:        val.Name,
			Position:    val.Position,
			Speedometer: val.Speedometer,
			Year:        val.Year,
			Fuel:        val.Fuel,
			SellPrice:   val.SellPrice,
			Active:      val.Active,
			CreatedAt:   val.CreatedAt,
		}
		result.AnalogList = append(result.AnalogList, aData)
	}

	result.TotalPages = data.TotalPages

	return result, nil
}

func (c *Client) AnalogUpdate(ctx context.Context, id string, analog domain.Analog) (domain.Result, error) {
	var (
		logMsg   = "service.Analog.AnalogUpdate "
		url      = c.cfg.AnalogService + analogUpdateEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := storeAnalog{
		PID:            analog.PID,
		UID:            analog.UID,
		Type:           analog.Type,
		Model:          analog.Model,
		Name:           analog.Name,
		Position:       analog.Position,
		Speedometer:    analog.Speedometer,
		Year:           analog.Year,
		Fuel:           analog.Fuel,
		Price:          analog.Price,
		Validity:       analog.Validity,
		SellPrice:      analog.SellPrice,
		SourceName:     analog.SourceName,
		SourceFileName: analog.SourceFileName,
		Comment:        analog.Comment,
		Active:         analog.Active,
	}

	for _, val := range analog.Photos.PhotosName {
		req.Photos.PhotosName = append(req.Photos.PhotosName, photosName{
			source: source{
				Title: val.Source.Title,
				Order: val.Source.Order,
				LAT:   val.Source.LAT,
				LON:   val.Source.LON,
			},
			photoName: photoName{
				PhotoName: val.PhotoName.PhotoName,
			},
		})
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) AnalogUpdateStatus(ctx context.Context, analog domain.UpdateAnalogStatus) (domain.Result, error) {
	var (
		logMsg   = "service.Analog.AnalogUpdateStatus "
		url      = c.cfg.AnalogService + analogUpdateStatusEndPoint
		response service.DBOResponse
		req      updateAnalogStatus
		data     result
		result   domain.Result
	)

	for _, val := range analog.Analog {
		req.Analog = append(req.Analog, analogStatus{
			ID:     val.ID,
			Active: val.Active,
		})
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) AnalogDelete(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg   = "service.Analog.AnalogDelete "
		url      = c.cfg.AnalogService + analogDeleteEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodDelete, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) AnalogSearch(ctx context.Context, analogID, typeID, modelID, nameID, positionID, fromYear, toYear, fuel,
	fromSpeedometer, toSpeedometer, validity, fromPrice, toPrice, activeValue, fromDate, toDate, user, limit, page string) (domain.AnalogListResp, error) {
	var (
		logMsg = "service.Analog.AnalogSearch "
		url    = c.cfg.AnalogService + analogSearchEndPoint +
			"?id=" + analogID +
			"&type=" + typeID +
			"&model=" + modelID +
			"&name=" + nameID +
			"&position=" + positionID +
			"&fromYear=" + fromYear +
			"&toYear=" + toYear +
			"&fuel=" + fuel +
			"&fromSpeedometer=" + fromSpeedometer +
			"&toSpeedometer=" + toSpeedometer +
			"&validity=" + validity +
			"&fromPrice=" + fromPrice +
			"&toPrice=" + toPrice +
			"&active=" + activeValue +
			"&fromDate=" + fromDate +
			"&toDate=" + toDate +
			"&user=" + user +
			"&limit=" + limit +
			"&page=" + page
		response service.DBOResponse
		data     analogListResponse
		result   domain.AnalogListResp
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}
	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data.AnalogList {
		aData := domain.AnalogList{
			Index:       val.Index,
			ID:          val.ID,
			Model:       val.Model,
			Name:        val.Name,
			Position:    val.Position,
			Speedometer: val.Speedometer,
			Year:        val.Year,
			Fuel:        val.Fuel,
			SellPrice:   val.SellPrice,
			Active:      val.Active,
			CreatedAt:   val.CreatedAt,
		}
		result.AnalogList = append(result.AnalogList, aData)
	}

	result.TotalPages = data.TotalPages

	return result, nil
}

func (c *Client) ChangeAnalogPdfFolder(ctx context.Context) (domain.Result, error) {
	var (
		logMsg   = "service.Analog.ChangeAnalogPdfFolder "
		url      = c.cfg.AnalogService + analogChangePdfFolder
		response service.DBOResponse
		data     result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return domain.Result{}, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return domain.Result{}, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return domain.Result{}, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return domain.Result{}, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return domain.Result{}, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return domain.Result{}, errs.ErrInternal
	}

	finalResult := domain.Result{
		Result: data.Result,
	}

	return finalResult, nil
}

func (c *Client) ChangeAnalogPhotosFolder(ctx context.Context) (domain.Result, error) {
	var (
		logMsg   = "service.Analog.ChangeAnalogPhotosFolder "
		url      = c.cfg.AnalogService + analogChangePhotosFolder
		response service.DBOResponse
		data     result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return domain.Result{}, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return domain.Result{}, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return domain.Result{}, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return domain.Result{}, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return domain.Result{}, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return domain.Result{}, errs.ErrInternal
	}

	finalResult := domain.Result{
		Result: data.Result,
	}

	return finalResult, nil
}

func (c *Client) ChangeAssessmentPhotoFolder(ctx context.Context) (domain.AssessmentPhotoFolderResponse, error) {
	var (
		logMsg   = "service.Analog.ChangeAnalogPhotoFolder "
		url      = c.cfg.AnalogService + assessmentChangePhotoFolder
		response service.DBOResponse
		data     []string
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return domain.AssessmentPhotoFolderResponse{}, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return domain.AssessmentPhotoFolderResponse{}, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return domain.AssessmentPhotoFolderResponse{}, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return domain.AssessmentPhotoFolderResponse{}, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return domain.AssessmentPhotoFolderResponse{}, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return domain.AssessmentPhotoFolderResponse{}, errs.ErrInternal
	}

	return domain.AssessmentPhotoFolderResponse{Result: data}, nil
}
