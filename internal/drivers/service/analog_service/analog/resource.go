package analog

type result struct {
	Result bool `json:"result"`
}

type idResponse struct {
	ID string `json:"id"`
}

type storeAnalog struct {
	PID            string        `json:"p_id"`
	UID            string        `json:"u_id"`
	Type           string        `json:"type"`
	Model          string        `json:"model"`
	Name           string        `json:"name"`
	Position       string        `json:"position"`
	Speedometer    int           `json:"speedometer"`
	Year           int           `json:"year"`
	Fuel           string        `json:"fuel"`
	Price          int           `json:"price"`
	Validity       int           `json:"validity"`
	SellPrice      int           `json:"sell_price"`
	Photos         allPhotosName `json:"photos"`
	SourceName     string        `json:"source_name"`
	Comment        string        `json:"comment"`
	SourceFileName string        `json:"source_file_name"`
	Active         bool          `json:"active"`
	NameKey        string        `json:"name_key"`
}

type allPhotosName struct {
	PhotosName []photosName `json:"photo"`
}

type photosName struct {
	photoName
	source
}

type photoName struct {
	PhotoName string `json:"photo_name"`
}

type source struct {
	LAT   string `json:"lat"`
	LON   string `json:"lon"`
	Title string `json:"title"`
	Order int    `json:"order"`
}

type getAnalog struct {
	ID            string       `json:"id"`
	Type          string       `json:"type"`
	Model         string       `json:"model"`
	Name          string       `json:"name"`
	Position      string       `json:"position"`
	Speedometer   int          `json:"speedometer"`
	Year          int          `json:"year"`
	Fuel          string       `json:"fuel"`
	SellPrice     int          `json:"sell_price"`
	Price         int          `json:"price"`
	Validity      int          `json:"validity"`
	Active        bool         `json:"active"`
	Comment       string       `json:"comment"`
	SourceFileUrl string       `json:"source_file_url"`
	SourceFile    string       `json:"source_file"`
	AllPhotosUrl  allPhotosUrl `json:"photos"`
	Employee      string       `json:"employee"`
	CreatedAt     string       `json:"created_at"`
	UpdatedAt     string       `json:"updated_at"`
}

type viewAnalog struct {
	ID            string       `json:"id"`
	Type          string       `json:"type"`
	Model         string       `json:"model"`
	Name          string       `json:"name"`
	Position      string       `json:"position"`
	Speedometer   int          `json:"speedometer"`
	Year          int          `json:"year"`
	Fuel          string       `json:"fuel"`
	SellPrice     int          `json:"sell_price"`
	Price         int          `json:"price"`
	Validity      string       `json:"validity"`
	Active        bool         `json:"active"`
	Comment       string       `json:"comment"`
	SourceFileUrl string       `json:"source_file_url"`
	SourceFile    string       `json:"source_file"`
	AllPhotosUrl  allPhotosUrl `json:"photos"`
	Employee      string       `json:"employee"`
	CreatedAt     string       `json:"created_at"`
	UpdatedAt     string       `json:"updated_at"`
}

type allPhotosUrl struct {
	PhotosUrl []photosUrl `json:"photo"`
}

type photosUrl struct {
	photoUrls
	source
}
type photoUrls struct {
	PhotoUrl string `json:"photo_url"`
}

type analogListResponse struct {
	TotalPages int          `json:"total_pages"`
	AnalogList []analogList `json:"analog_list"`
}

type analogList struct {
	Index       int    `json:"index"`
	ID          string `json:"id"`
	Model       string `json:"model"`
	Name        string `json:"name"`
	Position    string `json:"position"`
	Speedometer int    `json:"speedometer"`
	Year        int    `json:"year"`
	Fuel        string `json:"fuel"`
	SellPrice   int    `json:"sell_price"`
	Active      bool   `json:"active"`
	CreatedAt   string `json:"created_at"`
}

type analogStatus struct {
	ID     string `json:"id"`
	Active bool   `json:"active"`
}

type updateAnalogStatus struct {
	Analog []analogStatus `json:"data"`
}
