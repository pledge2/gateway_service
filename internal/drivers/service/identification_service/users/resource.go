package users

import "time"

type userList struct {
	ID   string `json:"id"`
	CBID int    `json:"cbid"`
}

type userInfo struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	CBID     int    `json:"cbid"`
	Direct   int    `json:"direct"`
	Role     int    `json:"role"`
	Position string `json:"position"`
	BranchID int    `json:"branch_id"`
}

type userName struct {
	CBID int    `json:"cbid"`
	Name string `json:"name"`
}

type appUsers struct {
	Index         int       `json:"index"`
	ID            string    `json:"id"`
	CBID          int       `json:"cbid"`
	FIO           string    `json:"fio"`
	Email         string    `json:"email"`
	Devices       []string  `json:"devices"`
	DevicesStatus []string  `json:"devices_status"`
	Role          string    `json:"role"`
	Filial        int       `json:"filial"`
	CreatedAt     time.Time `json:"created_at"`
}

type usersPagination struct {
	UsersList  []appUsers
	TotalPages int `json:"total_pages"`
}

type userEmail struct {
	Email string `json:"email"`
}

func timeToString(t time.Time) string {
	return t.Format("2006-01-02 15:04:05")
}
