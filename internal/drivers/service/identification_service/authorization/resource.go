package authorization

type signUp struct {
	Email      string `json:"e_mail"`
	Password   string `json:"password"`
	RePassword string `json:"re_password"`
	CBID       int    `json:"cbid"`
	DeviceType string `json:"device_type"`
	DeviceNum  string `json:"device_num"`
}

type tokenResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type signIn struct {
	Password   string `json:"password"`
	CBID       int    `json:"cbid"`
	DeviceType string `json:"device_type"`
	DeviceNum  string `json:"device_num"`
}

type checkCode struct {
	Code       string `json:"code"`
	CBID       int    `json:"cbid"`
	DeviceType string `json:"device_type"`
	DeviceNum  string `json:"device_num"`
}

type checkCbid struct {
	CBID       int    `json:"cbid"`
	DeviceType string `json:"device_type"`
	DeviceNum  string `json:"device_num"`
}

type changePassword struct {
	Password   string `json:"password"`
	RePassword string `json:"re_password"`
	CBID       int    `json:"cbid"`
	DeviceType string `json:"device_type"`
	DeviceNum  string `json:"device_num"`
}

type resendCode struct {
	CBID       int    `json:"cbid"`
	DeviceType string `json:"device_type"`
	DeviceNum  string `json:"device_num"`
}
