package service

type IdResponse struct {
	ID string `json:"id"`
}

type Result struct {
	Result bool `json:"result"`
}
