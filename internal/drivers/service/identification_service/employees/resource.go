package employees

import "time"

type empInfo struct {
	ID          string    `json:"id"`
	Index       int       `json:"index"`
	BranchID    int       `json:"branchId"`
	CategoryEmp string    `json:"categoryEmp"`
	CBID        int       `json:"cbId"`
	DepCode     string    `json:"depCode"`
	DepName     string    `json:"depName"`
	EmpState    string    `json:"empState"`
	LocalCode   string    `json:"localCode"`
	MailAddress string    `json:"mailAddress"`
	Name        string    `json:"name"`
	PhoneMobil  string    `json:"phoneMobil"`
	PostCode    string    `json:"postCode"`
	PostName    string    `json:"postName"`
	State       string    `json:"state"`
	UserState   string    `json:"userState"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type empPagination struct {
	EmpInfo    []empInfo `json:"employees"`
	TotalPages int       `json:"total_pages"`
}

type updateEmpInfo struct {
	ID          string `json:"-"`
	BranchID    int    `json:"branchId"`
	CategoryEmp string `json:"categoryEmp"`
	CBID        int    `json:"cbId"`
	DepCode     string `json:"depCode"`
	DepName     string `json:"depName"`
	EmpState    string `json:"empState"`
	LocalCode   string `json:"localCode"`
	MailAddress string `json:"mailAddress"`
	Name        string `json:"name"`
	PhoneMobil  string `json:"phoneMobil"`
	PostCode    string `json:"postCode"`
	PostName    string `json:"postName"`
	State       string `json:"state"`
	UserState   string `json:"userState"`
}
