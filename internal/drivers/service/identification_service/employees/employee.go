package employees

import (
	"bytes"
	"context"
	"encoding/json"
	"gateway_service/internal/config"
	"gateway_service/internal/domain"
	identification "gateway_service/internal/drivers/service/identification_service"
	"gateway_service/internal/errs"
	service "gateway_service/internal/pkg/http"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/pkg/status"
	"io"
	"net/http"
	"strconv"
)

const (
	findEmployeeEndpoint     = "/emp/find/one"
	findAllEmployeesEndpoint = "/emp/find/all"
	updateEmpDataEndpoint    = "/emp/update"
	getEmployeeData          = "/iabs/get/all"
)

type Client struct {
	cfg    config.Config
	client *http.Client
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		cfg:    cfg,
		client: &http.Client{},
		log:    log,
	}
}

func (c *Client) FindEmployee(ctx context.Context, cbid int) (domain.EmpInfo, error) {
	var (
		logMsg   = "service.Employees.FindAllEmployees "
		url      = c.cfg.IdentificationService + findEmployeeEndpoint + "?cbid=" + strconv.Itoa(cbid)
		response service.DBOResponse
		data     empInfo
		result   domain.EmpInfo
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.EmpInfo{
		ID:          data.ID,
		Index:       data.Index,
		BranchID:    data.BranchID,
		CategoryEmp: data.CategoryEmp,
		CBID:        data.CBID,
		DepCode:     data.DepCode,
		DepName:     data.DepName,
		EmpState:    data.EmpState,
		LocalCode:   data.LocalCode,
		MailAddress: data.MailAddress,
		Name:        data.Name,
		PhoneMobil:  data.PhoneMobil,
		PostCode:    data.PostCode,
		PostName:    data.PostName,
		State:       data.State,
		UserState:   data.UserState,
		CreatedAt:   data.CreatedAt,
		UpdatedAt:   data.CreatedAt,
	}

	return result, nil
}

func (c *Client) FindAllEmployees(ctx context.Context, limit, page int) (domain.EmpPagination, error) {
	var (
		logMsg = "service.Employees.FindAllEmployees "
		url    = c.cfg.IdentificationService + findAllEmployeesEndpoint +
			"?limit=" + strconv.Itoa(limit) + "&page=" + strconv.Itoa(page)
		response service.DBOResponse
		data     empPagination
		result   domain.EmpPagination
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data.EmpInfo {
		empData := domain.EmpInfo{
			ID:          val.ID,
			Index:       val.Index,
			BranchID:    val.BranchID,
			CategoryEmp: val.CategoryEmp,
			CBID:        val.CBID,
			DepCode:     val.DepCode,
			DepName:     val.DepName,
			EmpState:    val.EmpState,
			LocalCode:   val.LocalCode,
			MailAddress: val.MailAddress,
			Name:        val.Name,
			PhoneMobil:  val.PhoneMobil,
			PostCode:    val.PostCode,
			PostName:    val.PostName,
			State:       val.State,
			UserState:   val.UserState,
			CreatedAt:   val.CreatedAt,
			UpdatedAt:   val.CreatedAt,
		}
		result.EmpInfo = append(result.EmpInfo, empData)
	}
	result.TotalPages = data.TotalPages

	return result, nil
}

func (c *Client) UpdateEmpData(ctx context.Context, id string, empData domain.EmpInfo) (domain.Result, error) {
	var (
		logMsg   = "service.Employees.UpdateEmpData "
		url      = c.cfg.IdentificationService + updateEmpDataEndpoint + "?id=" + id
		response service.DBOResponse
		data     identification.Result
		result   domain.Result
	)

	req := updateEmpInfo{
		ID:          empData.ID,
		BranchID:    empData.BranchID,
		CategoryEmp: empData.CategoryEmp,
		CBID:        empData.CBID,
		DepCode:     empData.DepCode,
		DepName:     empData.DepName,
		EmpState:    empData.EmpState,
		LocalCode:   empData.LocalCode,
		MailAddress: empData.MailAddress,
		Name:        empData.Name,
		PhoneMobil:  empData.PhoneMobil,
		PostCode:    empData.PostCode,
		PostName:    empData.PostName,
		State:       empData.State,
		UserState:   empData.UserState,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) GetEmployeeDataIABS(ctx context.Context) (domain.Result, error) {
	var (
		logMsg   = "service.User.GetEmployeeDataIABS "
		url      = c.cfg.IdentificationService + getEmployeeData
		response service.DBOResponse
		data     identification.Result
		result   domain.Result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))
		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}
