package app_roles

type result struct {
	Result bool `json:"result"`
}

type postCodeList struct {
	Index        int    `json:"index"`
	PostCode     string `json:"post_code"`
	DepName      string `json:"dep_name"`
	PositionName string `json:"position_name"`
	Direction    string `json:"direction"`
}

type cbIDList struct {
	Index     int    `json:"index"`
	CBID      int    `json:"cbid"`
	BranchID  int    `json:"branch_id"`
	Name      string `json:"name"`
	Direction string `json:"direction"`
	ExpiredAt string `json:"expired_at"`
}

type empPostCode struct {
	DepName  string `json:"dep_name"`
	PostName string `json:"post_name"`
}

type rolePostCode struct {
	PostCode string `json:"post_code"`
	Direct   int    `json:"direct"`
}

type empCBID struct {
	CBID     int    `json:"cbid"`
	BranchID int    `json:"branch_id"`
	Name     string `json:"name"`
}

type roleCBID struct {
	CBID      int    `json:"cbid"`
	Direct    int    `json:"direct"`
	ExpiredAt string `json:"expired_at"`
}
