package roles

import "time"

type userRole struct {
	Role   string `json:"role"`
	Code   int    `json:"code"`
	Direct int    `json:"direct"`
	Active bool   `json:"active"`
}

type oneRole struct {
	Role   string `json:"role"`
	Code   int    `json:"code"`
	Active bool   `json:"active"`
}

type userRoles struct {
	ID       string    `json:"id"`
	Index    int       `json:"index"`
	Role     string    `json:"role"`
	Code     int       `json:"code"`
	Active   bool      `json:"active"`
	PostCode int       `json:"post_code"`
	CBID     int       `json:"cbid"`
	CreateAt time.Time `json:"created_at"`
	UpdateAt time.Time `json:"updated_at"`
}

type updateUserRole struct {
	Role   string `json:"role"`
	Code   int    `json:"code"`
	Active bool   `json:"active"`
}

type roleName struct {
	RoleName string `json:"role_name"`
}

type roleStatus struct {
	Active bool `json:"active"`
}
