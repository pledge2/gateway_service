package transport_fuel

type idResponse struct {
	ID string `json:"id"`
}

type result struct {
	Result bool `json:"result"`
}

type fuelStore struct {
	Fuel   string `json:"fuel"`
	Active bool   `json:"active"`
}

type fuelResponse struct {
	ID        string `json:"id"`
	Fuel      string `json:"fuel"`
	Active    bool   `json:"active"`
	CreatedAt string `json:"created_at"`
}

type allFuels struct {
	ID   string `json:"id"`
	Fuel string `json:"fuel"`
}

type fuelStatus struct {
	Active bool `json:"active"`
}
