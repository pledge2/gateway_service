package transport_fuel

import (
	"bytes"
	"context"
	"encoding/json"
	"gateway_service/internal/config"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	service "gateway_service/internal/pkg/http"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/pkg/status"
	"io"
	"net/http"
)

const (
	storeFuelEndPoint        = "/fuel/store"
	findOneFuelEndPoint      = "/fuel/find/one"
	findAllFuelEndPoint      = "/fuel/find/all"
	updateFuelEndPoint       = "/fuel/update"
	updateFuelStatusEndPoint = "/fuel/update/status"
	deleteFuelEndPoint       = "/fuel/delete"
)

type Client struct {
	cfg    config.Config
	client *http.Client
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		cfg:    cfg,
		client: &http.Client{},
		log:    log,
	}
}

func (c *Client) StoreFuel(ctx context.Context, fData domain.Fuel) (domain.IdResponse, error) {
	var (
		logMsg   = "service.Fuel.StoreFuel "
		url      = c.cfg.ReferenceService + storeFuelEndPoint
		response service.DBOResponse
		data     idResponse
		result   domain.IdResponse
	)

	req := fuelStore{
		Fuel:   fData.Fuel,
		Active: fData.Active,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.IdResponse{
		ID: data.ID,
	}

	return result, nil
}

func (c *Client) FindOneFuel(ctx context.Context, id string) (domain.Fuel, error) {
	var (
		logMsg   = "service.Fuel.FindOneFuel "
		url      = c.cfg.ReferenceService + findOneFuelEndPoint + "?id=" + id
		response service.DBOResponse
		data     fuelResponse
		result   domain.Fuel
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Fuel{
		ID:       data.Fuel,
		Fuel:     data.Fuel,
		Active:   data.Active,
		CreateAt: data.CreatedAt,
	}

	return result, nil
}

func (c *Client) FindAllFuel(ctx context.Context) ([]domain.Fuel, error) {
	var (
		logMsg   = "service.Fuel.FindAllFuel "
		url      = c.cfg.ReferenceService + findAllFuelEndPoint
		response service.DBOResponse
		data     []allFuels
		result   []domain.Fuel
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data {
		fuels := domain.Fuel{
			ID:   val.ID,
			Fuel: val.Fuel,
		}
		result = append(result, fuels)
	}

	return result, nil
}

func (c *Client) UpdateFuel(ctx context.Context, id string, fData domain.Fuel) (domain.Result, error) {
	var (
		logMsg   = "service.Fuel.UpdateFuel "
		url      = c.cfg.ReferenceService + updateFuelEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := fuelStore{
		Fuel:   fData.Fuel,
		Active: fData.Active,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) UpdateFuelStatus(ctx context.Context, id string, fData domain.Fuel) (domain.Result, error) {
	var (
		logMsg   = "service.Fuel.UpdateFuelStatus "
		url      = c.cfg.ReferenceService + updateFuelStatusEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := fuelStatus{
		Active: fData.Active,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) DeleteFuel(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg   = "service.Fuel.FuelDelete "
		url      = c.cfg.ReferenceService + deleteFuelEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodDelete, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}
