package transport_discount

type discountParam struct {
	Code      int    `json:"code"`
	Percent   int    `json:"percent"`
	CreatedAt string `json:"created_at"`
}

type updatediscount struct {
	Code    int `json:"code"`
	Percent int `json:"percent"`
}

type discountStore struct {
	NameID string `json:"name_id"`
}

type discounts struct {
	ID        string          `json:"id"`
	NameID    string          `json:"name_id"`
	Discounts []discountParam `json:"discounts"`
}

type result struct {
	Result bool `json:"result"`
}
