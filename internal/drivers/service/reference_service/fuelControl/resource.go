package fuel_control

type fuelControlParam struct {
	FuelID    string `json:"fuel_id"`
	Fuel      string `json:"fuel"`
	Price     int    `json:"price"`
	CreatedAt string `json:"created_at"`
}

type fuelControlUpdate struct {
	FuelID string `json:"fuel_id"`
	Price  int    `json:"price"`
}

type fuelControlStore struct {
	NameID string `json:"name_id"`
}

type fuelControls struct {
	ID           string             `json:"id"`
	NameID       string             `json:"name_id"`
	FuelControls []fuelControlParam `json:"fuel_controls"`
}

type result struct {
	Result bool `json:"result"`
}
