package transport_type

type idResponse struct {
	ID string `json:"id"`
}

type result struct {
	Result bool `json:"result"`
}

type typeStore struct {
	Type      string `json:"type"`
	Active    bool   `json:"active"`
	PhotoName string `json:"photo_name"`
}

type transportTypeResp struct {
	ID          string `json:"id"`
	TypeKey     string `json:"type_key"`
	Index       int    `json:"index"`
	Type        string `json:"type"`
	Active      bool   `json:"active"`
	FastURL     string `json:"fast_url"`
	OfficialURL string `json:"official_url"`
	PhotoName   string `json:"photo_name"`
	PhotoUrl    string `json:"photo_url"`
	CreateAt    string `json:"created_at"`
	UpdateAt    string `json:"updated_at"`
}

type typePagination struct {
	Type       []transportTypeResp `json:"Type"`
	TotalPages int                 `json:"total_pages"`
}

type typeUpdate struct {
	Type      string `json:"type"`
	PhotoName string `json:"photo_name"`
	Active    bool   `json:"active"`
}

type updateTypePhoto struct {
	PhotoName string `json:"photo_name"`
}

type updateTypeStatus struct {
	Active bool `json:"active"`
}
