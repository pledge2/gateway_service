package transport_type

import (
	"bytes"
	"context"
	"encoding/json"
	"gateway_service/internal/config"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	service "gateway_service/internal/pkg/http"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/pkg/status"
	"io"
	"net/http"
	"strconv"
)

const (
	typeStoreEndPoint          = "/type/store"
	typeFindOneEndPoint        = "/type/find/one"
	typeFindAllEndPoint        = "/type/find/all"
	typeFindPaginationEndPoint = "/type/pagination"
	typeUpdateEndPoint         = "/type/update"
	typeDeleteEndPoint         = "/type/delete"
	updateTypePhotoEndPoint    = "/type/update/photo"
	UpdateTypeStatusEndPoint   = "/type/update/status"
	changeTypeFolder           = "/type/change/folder"
)

type Client struct {
	cfg    config.Config
	client *http.Client
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		cfg:    cfg,
		client: &http.Client{},
		log:    log,
	}
}

func (c *Client) StoreType(ctx context.Context, tData domain.TypeStore) (domain.IdResponse, error) {
	var (
		logMsg   = "service.Type.StoreType "
		url      = c.cfg.ReferenceService + typeStoreEndPoint
		response service.DBOResponse
		data     idResponse
		result   domain.IdResponse
	)

	req := typeStore{
		Type:      tData.Type,
		Active:    tData.Active,
		PhotoName: tData.PhotoName,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.IdResponse{
		ID: data.ID,
	}

	return result, nil
}

func (c *Client) FindOneType(ctx context.Context, id string) (domain.TypeStore, error) {
	var (
		logMsg   = "service.Type.FindOneType "
		url      = c.cfg.ReferenceService + typeFindOneEndPoint + "?id=" + id
		response service.DBOResponse
		data     transportTypeResp
		result   domain.TypeStore
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.TypeStore{
		ID:       data.ID,
		Type:     data.Type,
		Active:   data.Active,
		PhotoUrl: data.PhotoUrl,
		CreateAt: data.CreateAt,
	}

	return result, nil
}

func (c *Client) FindAllType(ctx context.Context) ([]domain.TypeStore, error) {
	var (
		logMsg   = "service.Type.FindAllType "
		url      = c.cfg.ReferenceService + typeFindAllEndPoint
		response service.DBOResponse
		data     []transportTypeResp
		result   []domain.TypeStore
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data {
		tData := domain.TypeStore{
			ID:          val.ID,
			Type:        val.Type,
			TypeKey:     val.TypeKey,
			Active:      val.Active,
			FastURL:     val.FastURL,
			OfficialURL: val.OfficialURL,
			PhotoUrl:    val.PhotoUrl,
			CreateAt:    val.CreateAt,
		}
		result = append(result, tData)
	}

	return result, nil
}

func (c *Client) TypePagination(ctx context.Context, limit, page int) (domain.TypePagination, error) {
	var (
		logMsg = "service.Type.TypePagination "
		url    = c.cfg.ReferenceService + typeFindPaginationEndPoint +
			"?limit=" + strconv.Itoa(limit) + "&page=" + strconv.Itoa(page)
		response service.DBOResponse
		data     typePagination
		result   domain.TypePagination
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data.Type {
		tData := domain.TypeStore{
			ID:          val.ID,
			TypeKey:     val.TypeKey,
			Index:       val.Index,
			Type:        val.Type,
			Active:      val.Active,
			FastURL:     val.FastURL,
			OfficialURL: val.OfficialURL,
			PhotoName:   val.PhotoName,
			PhotoUrl:    val.PhotoUrl,
			CreateAt:    val.CreateAt,
			UpdateAt:    val.UpdateAt,
		}
		result.Type = append(result.Type, tData)
	}
	result.TotalPages = data.TotalPages

	return result, nil
}

func (c *Client) UpdateType(ctx context.Context, id string, tData domain.TypeStore) (domain.Result, error) {
	var (
		logMsg   = "service.Type.UpdateType "
		url      = c.cfg.ReferenceService + typeUpdateEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := typeUpdate{
		Type:      tData.Type,
		PhotoName: tData.PhotoName,
		Active:    tData.Active,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))
		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) UpdateTypePhoto(ctx context.Context, id string, tPhoto domain.TypeStore) (domain.Result, error) {
	var (
		logMsg   = "service.Type.UpdateTypePhoto "
		url      = c.cfg.ReferenceService + updateTypePhotoEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := updateTypePhoto{
		PhotoName: tPhoto.PhotoName,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) UpdateTypeStatus(ctx context.Context, id string, tPhoto domain.TypeStore) (domain.Result, error) {
	var (
		logMsg   = "service.Type.UpdateTypeStatus "
		url      = c.cfg.ReferenceService + UpdateTypeStatusEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := updateTypeStatus{
		Active: tPhoto.Active,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) DeleteType(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg   = "service.Type.DeleteType "
		url      = c.cfg.ReferenceService + typeDeleteEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodDelete, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) ChangeTypePhotosFolder(ctx context.Context) (domain.Result, error) {
	var (
		logMsg   = "service.Type.ChangeTypePhotosFolder "
		url      = c.cfg.ReferenceService + changeTypeFolder
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}
	return result, nil
}
