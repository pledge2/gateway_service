package transport_position

type idResponse struct {
	ID string `json:"id"`
}

type result struct {
	Result bool `json:"result"`
}

type positionStore struct {
	NameID   string `json:"name"`
	Position string `json:"position"`
	Active   bool   `json:"active"`
}

type positionResponse struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Position string `json:"position"`
	Active   bool   `json:"active"`
	CreateAt string `json:"created_at"`
}

type position struct {
	ID       string `json:"id"`
	Index    int    `json:"index"`
	Type     string `json:"type"`
	Model    string `json:"model"`
	NameID   string `json:"name_id"`
	Name     string `json:"name"`
	Position string `json:"position"`
	Active   bool   `json:"active"`
	CreateAt string `json:"created_at"`
}

type positionPagination struct {
	Position   []position `json:"position"`
	TotalPages int        `json:"total_pages"`
}

type updatePositionStatus struct {
	Active bool `json:"active"`
}
