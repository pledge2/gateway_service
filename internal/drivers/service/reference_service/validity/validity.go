package transport_validity

import (
	"bytes"
	"context"
	"encoding/json"
	"gateway_service/internal/config"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	service "gateway_service/internal/pkg/http"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/pkg/status"
	"io"
	"net/http"
)

const (
	storeValidityEndPoint        = "/validity/store"
	findOneValidityEndPoint      = "/validity/find/one"
	findAllValidityEndPoint      = "/validity/find/all"
	updateValidityEndPoint       = "/validity/update"
	updateValidityStatusEndPoint = "/validity/update/status"
	deleteValidityEndPoint       = "/validity/delete"
)

type Client struct {
	cfg    config.Config
	client *http.Client
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		cfg:    cfg,
		client: &http.Client{},
		log:    log,
	}
}

func (c *Client) StoreValidity(ctx context.Context, validity domain.Validity) (domain.IdResponse, error) {
	var (
		logMsg   = "service.Validity.StoreValidity "
		url      = c.cfg.ReferenceService + storeValidityEndPoint
		response service.DBOResponse
		data     idResponse
		result   domain.IdResponse
	)

	req := validityStore{
		Validity: validity.Validity,
		Code:     validity.Code,
		Active:   validity.Active,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.IdResponse{
		ID: data.ID,
	}

	return result, nil
}

func (c *Client) FindOneValidity(ctx context.Context, id string) (domain.Validity, error) {
	var (
		logMsg   = "service.Validity.FindOneValidity "
		url      = c.cfg.ReferenceService + findOneValidityEndPoint + "?id=" + id
		response service.DBOResponse
		data     validityResponse
		result   domain.Validity
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Validity{
		ID:       data.ID,
		Validity: data.Validity,
		Code:     data.Code,
		Active:   data.Active,
		CreateAt: data.CreateAt,
	}

	return result, nil
}

func (c *Client) ValidityFindAll(ctx context.Context) ([]domain.Validity, error) {
	var (
		logMsg   = "service.Reference.ValidityFindAll "
		url      = c.cfg.ReferenceService + findAllValidityEndPoint
		response service.DBOResponse
		data     []allValidities
		result   []domain.Validity
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data {
		vData := domain.Validity{
			Validity: val.Validity,
			Code:     val.Code,
		}
		result = append(result, vData)
	}

	return result, nil
}

func (c *Client) ValidityUpdate(ctx context.Context, id string, validity domain.Validity) (domain.Result, error) {
	var (
		logMsg   = "service.Reference.ValidityUpdate "
		url      = c.cfg.ReferenceService + updateValidityEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := validityStore{
		Validity: validity.Validity,
		Code:     validity.Code,
		Active:   validity.Active,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) UpdateValidityStatus(ctx context.Context, id string, validity domain.Validity) (domain.Result, error) {
	var (
		logMsg   = "service.Reference.UpdateValidityStatus "
		url      = c.cfg.ReferenceService + updateValidityStatusEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := updateValidityStatus{
		Active: validity.Active,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) ValidityDelete(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg   = "service.Reference.ValidityDelete "
		url      = c.cfg.ReferenceService + deleteValidityEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodDelete, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}
