package transport_validity

type idResponse struct {
	ID string `json:"id"`
}

type result struct {
	Result bool `json:"result"`
}

type validityStore struct {
	Validity string `json:"validity"`
	Code     int    `json:"code"`
	Active   bool   `json:"active"`
}

type validityResponse struct {
	ID       string `json:"id"`
	Validity string `json:"validity"`
	Code     int    `json:"code"`
	Active   bool   `json:"active"`
	CreateAt string `json:"created_at"`
}

type allValidities struct {
	Validity string `json:"validity"`
	Code     int    `json:"code"`
}

type updateValidityStatus struct {
	Active bool `json:"active"`
}
