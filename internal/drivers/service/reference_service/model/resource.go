package transport_model

type idResponse struct {
	ID string `json:"id"`
}

type result struct {
	Result bool `json:"result"`
}

type modelStore struct {
	TypeID    string `json:"type"`
	Model     string `json:"model"`
	Active    bool   `json:"active"`
	PhotoName string `json:"photo_name"`
}

type modelResp struct {
	ID       string `json:"id"`
	Type     string `json:"type"`
	Model    string `json:"model"`
	Active   bool   `json:"active"`
	PhotoUrl string `json:"photo_url"`
	CreateAt string `json:"created_at"`
}

type allModels struct {
	ID       string `json:"id"`
	Type     string `json:"type"`
	Model    string `json:"model"`
	PhotoUrl string `json:"photo_url"`
}

type model struct {
	ID        string `json:"id"`
	Index     int    `json:"index"`
	TypeID    string `json:"type_id"`
	Type      string `json:"type"`
	Model     string `json:"model"`
	Active    bool   `json:"active"`
	PhotoName string `json:"photo_name"`
	PhotoUrl  string `json:"photo_url"`
	CreateAt  string `json:"created_at"`
}

type modelPagination struct {
	Model      []model `json:"model"`
	TotalPages int     `json:"total_pages"`
}

type updateModelPhoto struct {
	PhotoName string `json:"photo_name"`
}

type updateModelStatus struct {
	Active bool `json:"active"`
}
