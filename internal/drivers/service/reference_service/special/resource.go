package transport_special

type specialStore struct {
	Key  string `json:"key"`
	Name string `json:"name"`
}

type specials struct {
	ID        string `json:"id"`
	Key       string `json:"key"`
	Name      string `json:"name"`
	FL        bool   `json:"fl"`
	KM        bool   `json:"km"`
	MH        bool   `json:"mh"`
	CA        bool   `json:"ca"`
	LE        bool   `json:"le"`
	PO        bool   `json:"po"`
	VO        bool   `json:"vo"`
	WI        bool   `json:"wi"`
	SE        bool   `json:"se"`
	CreatedAt string `json:"created_at"`
}

type result struct {
	Result bool `json:"result"`
}

type idResponse struct {
	ID string `json:"id"`
}
