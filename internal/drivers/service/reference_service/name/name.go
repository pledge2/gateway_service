package transport_name

import (
	"bytes"
	"context"
	"encoding/json"
	"gateway_service/internal/config"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	service "gateway_service/internal/pkg/http"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/pkg/status"
	"io"
	"net/http"
	"strconv"
)

const (
	nameStoreEndPoint            = "/name/store"
	nameFindOneEndPoint          = "/name/find/one"
	nameFindAllEndPoint          = "/name/find/all"
	nameFindPaginationEndPoint   = "/name/pagination"
	nameFindAlByModelIDlEndPoint = "/name/find/all/model"
	nameFilterEndPoint           = "/name/filter"
	nameUpdateEndPoint           = "/name/update"
	updateNamePhotoEndPoint      = "/name/update/photo"
	updateNameStatusEndPoint     = "/name/update/status"
	nameDeleteEndPoint           = "/name/delete"
	changeNamePhotosEndPoint     = "/name/change/folder"
)

type Client struct {
	cfg    config.Config
	client *http.Client
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		cfg:    cfg,
		client: &http.Client{},
		log:    log,
	}
}

func (c *Client) StoreName(ctx context.Context, name domain.Name) (domain.IdResponse, error) {
	var (
		logMsg   = "service.Name.StoreName "
		url      = c.cfg.ReferenceService + nameStoreEndPoint
		response service.DBOResponse
		data     idResponse
		result   domain.IdResponse
	)

	req := nameStore{
		ModelID:   name.ModelID,
		Name:      name.Name,
		TypeKey:   name.TypeKey,
		NameKey:   name.NameKey,
		Active:    name.Active,
		PhotoName: name.PhotoName,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.IdResponse{
		ID: data.ID,
	}

	return result, nil
}

func (c *Client) FindOneName(ctx context.Context, id string) (domain.Name, error) {
	var (
		logMsg   = "service.Name.FindOneName "
		url      = c.cfg.ReferenceService + nameFindOneEndPoint + "?id=" + id
		response service.DBOResponse
		data     nameResp
		result   domain.Name
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Name{
		ID:             data.ID,
		ModelID:        data.Model,
		Name:           data.Name,
		Active:         data.Active,
		PhotoURL:       data.PhotoUrl,
		CreateAt:       data.CreateAt,
		TransportGroup: data.TransportGroup,
	}

	return result, nil
}

func (c *Client) FindAllName(ctx context.Context) ([]domain.Name, error) {
	var (
		logMsg   = "service.Name.FindAllName "
		url      = c.cfg.ReferenceService + nameFindAllEndPoint
		response service.DBOResponse
		data     []allNames
		result   []domain.Name
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data {
		names := domain.Name{
			ID:       val.ID,
			ModelID:  val.Model,
			Name:     val.Name,
			NameKey:  val.NameKey,
			PhotoURL: val.PhotoURL,
		}
		result = append(result, names)
	}

	return result, nil
}

func (c *Client) FindAllNameByModelID(ctx context.Context, modelID string) ([]domain.Name, error) {
	var (
		logMsg   = "service.Name.FindAllNameByModelID "
		url      = c.cfg.ReferenceService + nameFindAlByModelIDlEndPoint + "?model_id=" + modelID
		response service.DBOResponse
		data     []allNames
		result   []domain.Name
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data {
		names := domain.Name{
			ID:       val.ID,
			ModelID:  val.Model,
			Name:     val.Name,
			NameKey:  val.NameKey,
			PhotoURL: val.PhotoURL,
		}
		result = append(result, names)
	}

	return result, nil
}

func (c *Client) NamePagination(ctx context.Context, limit, page int) (domain.NamePagination, error) {
	var (
		logMsg = "service.Name.NamePagination "
		url    = c.cfg.ReferenceService + nameFindPaginationEndPoint +
			"?limit=" + strconv.Itoa(limit) + "&page=" + strconv.Itoa(page)
		response service.DBOResponse
		data     namePagination
		result   domain.NamePagination
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data.Name {
		names := domain.Name{
			ID:        val.ID,
			Index:     val.Index,
			Type:      val.Type,
			ModelID:   val.ModelID,
			Model:     val.Model,
			Name:      val.Name,
			Active:    val.Active,
			PhotoName: val.PhotoName,
			PhotoURL:  val.PhotoURL,
			CreateAt:  val.CreateAt,
		}
		result.Name = append(result.Name, names)
	}

	result.TotalPages = data.TotalPages

	return result, nil
}

func (c *Client) FilterName(ctx context.Context, typeID, modelID, nameID string, limit, page int) (domain.NamePagination, error) {
	var (
		logMsg = "service.FilterName.FilterName "
		url    = c.cfg.ReferenceService + nameFilterEndPoint +
			"?type_id=" + typeID + "&model_id=" + modelID + "&name_id=" + nameID +
			"&limit=" + strconv.Itoa(limit) + "&page=" + strconv.Itoa(page)
		response service.DBOResponse
		data     namePagination
		result   domain.NamePagination
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data.Name {
		names := domain.Name{
			ID:             val.ID,
			Index:          val.Index,
			Type:           val.Type,
			ModelID:        val.ModelID,
			Model:          val.Model,
			Name:           val.Name,
			NameKey:        val.NameKey,
			TypeKey:        val.TypeKey,
			TransportGroup: val.TransportGroup,
			Active:         val.Active,
			PhotoName:      val.PhotoName,
			PhotoURL:       val.PhotoURL,
			CreateAt:       val.CreateAt,
		}
		result.Name = append(result.Name, names)
	}

	result.TotalPages = data.TotalPages

	return result, nil
}

func (c *Client) UpdateName(ctx context.Context, id string, name domain.Name) (domain.Result, error) {
	var (
		logMsg   = "service.Name.UpdateName "
		url      = c.cfg.ReferenceService + nameUpdateEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := nameStore{
		ModelID:   name.ModelID,
		Name:      name.Name,
		TypeKey:   name.TypeKey,
		NameKey:   name.NameKey,
		Active:    name.Active,
		PhotoName: name.PhotoName,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))
		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) UpdateNamePhoto(ctx context.Context, id string, name domain.Name) (domain.Result, error) {
	var (
		logMsg   = "service.Name.UpdateNamePhoto "
		url      = c.cfg.ReferenceService + updateNamePhotoEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := updateNamePhoto{
		PhotoName: name.PhotoName,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))
		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) UpdateNameStatus(ctx context.Context, id string, nStatus domain.Name) (domain.Result, error) {
	var (
		logMsg   = "service.Name.UpdateNameStatus "
		url      = c.cfg.ReferenceService + updateNameStatusEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := updateNameStatus{
		Active: nStatus.Active,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))
		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) DeleteName(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg   = "service.Reference.DeleteName "
		url      = c.cfg.ReferenceService + nameDeleteEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodDelete, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))
		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) ChangeNamePhotosFolder(ctx context.Context) (domain.Result, error) {
	var (
		logMsg   = "service.Type.ChangeNamePhotosFolder "
		url      = c.cfg.ReferenceService + changeNamePhotosEndPoint
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}
	return result, nil
}
