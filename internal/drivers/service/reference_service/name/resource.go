package transport_name

type idResponse struct {
	ID string `json:"id"`
}

type result struct {
	Result bool `json:"result"`
}

type nameStore struct {
	ModelID   string `json:"model"`
	Name      string `json:"name"`
	TypeKey   string `json:"type_key"`
	NameKey   string `json:"name_key"`
	Active    bool   `json:"active"`
	PhotoName string `json:"photo_name"`
}

type nameResp struct {
	ID             string `json:"id"`
	Model          string `json:"model"`
	Name           string `json:"name"`
	Active         bool   `json:"active"`
	PhotoUrl       string `json:"photo_url"`
	CreateAt       string `json:"created_at"`
	TransportGroup string `json:"transport_group"`
}

type allNames struct {
	ID       string `json:"id"`
	Model    string `json:"model"`
	Name     string `json:"name"`
	NameKey  string `json:"name_key"`
	PhotoURL string `json:"photo_url"`
}

type name struct {
	ID             string `json:"id"`
	Index          int    `json:"index"`
	Type           string `json:"type"`
	ModelID        string `json:"model_id"`
	Model          string `json:"model"`
	Name           string `json:"name"`
	TypeKey        string `json:"type_key"`
	NameKey        string `json:"name_key"`
	TransportGroup string `json:"transport_group"`
	PhotoName      string `json:"photo_name"`
	PhotoURL       string `json:"photo_url"`
	Active         bool   `json:"active"`
	CreateAt       string `json:"created_at"`
}

type namePagination struct {
	Name       []name `json:"name"`
	TotalPages int    `json:"total_pages"`
}

type updateNamePhoto struct {
	PhotoName string `json:"photo_name"`
}

type updateNameStatus struct {
	Active bool `json:"active"`
}
