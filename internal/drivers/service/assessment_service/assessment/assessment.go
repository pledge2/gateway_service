package assessment

import (
	"bytes"
	"context"
	"encoding/json"
	"gateway_service/internal/config"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	service "gateway_service/internal/pkg/http"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/pkg/status"
	"io"
	"net/http"
	"strconv"
)

const (
	govCarInfoEndPoint    = "/assessment/govuz/carinfo"
	updateGovDataEndPoint = "/update/gov/data"

	storeMainEndPoint              = "/assessment/store/main"
	getPhotosEndPoint              = "/assessment/get/photos"
	fastAssessmentEndPoint         = "/assessment/fast"
	officialAssessmentEndPoint     = "/assessment/official"
	findAssessmentEndPoint         = "/assessment/find/one"
	findAssessmentByStatusEndPoint = "/assessment/find/status"
	countAllStatusEndPoint         = "/assessment/count/status"
	assessmentAnalogsList          = "/assessment/analogs/list"
	deleteAssessmentEndPoint       = "/assessment/delete"

	changeAssessmentStatus = "/assessment/change/status"

	assessmentDataEndPoint = "/assessment/data"
)

type Client struct {
	cfg    config.Config
	client *http.Client
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		cfg:    cfg,
		client: &http.Client{},
		log:    log,
	}
}

func (c *Client) StoreMainInfo(ctx context.Context, assessment domain.AssessmentStore) (domain.IdResponse, error) {
	var (
		logMsg   = "service.Assessment.StoreMainInfo "
		url      = c.cfg.AssessmentService + storeMainEndPoint
		response service.DBOResponse
		data     idResponse
		result   domain.IdResponse
	)

	req := assessmentStore{
		CBID:        assessment.CBID,
		EXTCBID:     assessment.EXTCBID,
		Direct:      assessment.Direct,
		BranchID:    assessment.BranchID,
		PID:         assessment.PID,
		Type:        assessment.Transport.Type,
		Model:       assessment.Transport.Model,
		Name:        assessment.Transport.Name,
		Position:    assessment.Transport.Position,
		Year:        assessment.Transport.Year,
		Speedometer: assessment.Transport.Speedometer,
		Validity:    assessment.Transport.Validity,
		Fuel:        assessment.Transport.Fuel,
		ExtraParam:  assessment.Transport.ExtraParam,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.IdResponse{
		ID: data.ID,
	}

	return result, nil
}

func (c *Client) GovCarInfo(ctx context.Context, carInfo domain.TechNumberParams) (domain.GovUzResponse, error) {
	var (
		logMsg   = "service.GovUz.GovCarInfo "
		url      = c.cfg.AssessmentService + govCarInfoEndPoint
		response service.DBOResponse
		data     govUzResponse
		result   domain.GovUzResponse
	)

	req := techNumberParams{
		AssessmentID:   carInfo.AssessmentID,
		PassportSeria:  carInfo.PassportSeria,
		PassportNumber: carInfo.PassportNumber,
		PlateNumber:    carInfo.PlateNumber,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.GovUzResponse{
		VehicleKuzov:       data.VehicleKuzov,
		OwnerPinfl:         data.OwnerPinfl,
		OwnerFullname:      data.OwnerFullname,
		VehicleModel:       data.VehicleModel,
		VehicleColor:       data.VehicleColor,
		VehicleMadeYear:    data.VehicleMadeYear,
		VehicleMotor:       data.VehicleMotor,
		VehicleFullWeight:  data.VehicleFullWeight,
		VehicleEmptyWeight: data.VehicleEmptyWeight,
		VehiclePlateNumber: data.VehiclePlateNumber,
		PassportNumber:     data.PassportNumber,
		PassportSeria:      data.PassportSeria,
		VinNumber:          data.VinNumber,
		VehiclePower:       data.VehiclePower,
		Seats:              data.Seats,
		VehicleShassi:      data.VehicleShassi,
	}

	return result, nil
}

func (c *Client) GetPhotos(ctx context.Context, id string) (domain.AssessmentPhotos, error) {
	var (
		logMsg   = "service.Assessment.GetPhotos "
		url      = c.cfg.AssessmentService + getPhotosEndPoint + "?id=" + id
		response service.DBOResponse
		data     allPhotoSource
		result   domain.AssessmentPhotos
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data.PhotoSource {
		assessmentPhoto := domain.PhotoSource{
			Title:    val.Title,
			Order:    val.Order,
			PhotoUrl: val.PhotoUrl,
			LAT:      val.LAT,
			LON:      val.LON,
		}
		result.PhotoSource = append(result.PhotoSource, assessmentPhoto)
	}

	return result, nil
}

func (c *Client) FastAssessment(ctx context.Context, assessment domain.AssessmentStore) (domain.AssessmentResult, error) {
	var (
		logMsg   = "service.Assessment.FastAssessment "
		url      = c.cfg.AssessmentService + fastAssessmentEndPoint
		response service.DBOResponse
		data     assessmentResult
		result   domain.AssessmentResult
	)

	req := fastAssessment{
		Type:        assessment.Transport.Type,
		Model:       assessment.Transport.Model,
		Name:        assessment.Transport.Name,
		Year:        assessment.Transport.Year,
		Speedometer: assessment.Transport.Speedometer,
		Validity:    assessment.Transport.Validity,
		Position:    assessment.Transport.Position,
		Fuel:        assessment.Transport.Fuel,
		NameKey:     assessment.Transport.NameKey,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))
		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.AssessmentResult{
		MarketPrice:       data.MarketPrice,
		CreditPrice:       data.CreditPrice,
		TransportModel:    data.TransportModel,
		TransportName:     data.TransportName,
		TransportPosition: data.TransportPosition,
		TransportYear:     data.TransportYear,
	}

	return result, nil
}

func (c *Client) OfficialAssessment(ctx context.Context, assessment domain.AssessmentStore) (domain.AssessmentResult, error) {
	var (
		logMsg   = "service.Assessment.OfficialAssessment "
		url      = c.cfg.AssessmentService + officialAssessmentEndPoint
		response service.DBOResponse
		data     assessmentResult
		result   domain.AssessmentResult
	)

	req := idResponse{
		ID: assessment.ID,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.AssessmentResult{
		MarketPrice:       data.MarketPrice,
		CreditPrice:       data.CreditPrice,
		TransportModel:    data.TransportModel,
		TransportName:     data.TransportName,
		TransportPosition: data.TransportPosition,
		TransportYear:     data.TransportYear,
	}

	return result, nil
}

func (c *Client) FindAssessment(ctx context.Context, id string) (domain.Assessment, error) {
	var (
		logMsg   = "service.Assessment.FindAssessment "
		url      = c.cfg.AssessmentService + findAssessmentEndPoint + "?id=" + id
		response service.DBOResponse
		data     assessmentResponse
		result   domain.Assessment
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Assessment{
		ID:        data.ID,
		CBID:      data.CBID,
		PID:       data.PID,
		IIBInfoID: data.IIBInfoID,
		GovUz: domain.GovUz{
			VehicleKuzov:       data.VehicleKuzov,
			OwnerPinfl:         data.OwnerPinfl,
			OwnerFullname:      data.OwnerFullname,
			VehicleModel:       data.VehicleModel,
			VehicleColor:       data.VehicleColor,
			VehicleMadeYear:    data.VehicleMadeYear,
			VehicleMotor:       data.VehicleMotor,
			VehicleFullWeight:  data.VehicleFullWeight,
			VehicleEmptyWeight: data.VehicleEmptyWeight,
			VehiclePlateNumber: data.VehiclePlateNumber,
			VehiclePower:       data.VehiclePower,
			PassportNumber:     data.PassportNumber,
			PassportSeria:      data.PassportSeria,
			VehicleShassi:      data.VehicleShassi,
		},
		IIBPermitInfo: data.IIBPermitInfo,
		Transport: domain.Transport{
			ID:          data.ID,
			Type:        data.Type,
			TypeKey:     data.TypeKey,
			Model:       data.Model,
			Name:        data.Name,
			NameKey:     data.NameKey,
			Position:    data.Position,
			Year:        data.Year,
			Speedometer: data.Speedometer,
			Validity:    data.Validity,
			Fuel:        data.Fuel,
			Price:       data.Price,
			SellPrice:   data.SellPrice,
			ExtraParam:  data.ExtraParam,
		},
		MarketPrice: data.MarketPrice,
		CreditPrice: data.CreditPrice,
		TransportView: domain.TransportView{
			TransportModel:    data.TransportModel,
			TransportName:     data.TransportName,
			TransportPosition: data.TransportPosition,
			TransportYear:     data.TransportYear,
		},
		Url:    data.Url,
		Status: data.Status,
	}

	for _, photo := range data.Photos.PhotoUrl {
		value := domain.PhotoSource{
			Title:    photo.Title,
			Order:    photo.Order,
			PhotoUrl: photo.PhotoUrl,
			LAT:      photo.LAT,
			LON:      photo.LON,
		}
		result.AllPhotosUrl.PhotoSource = append(result.AllPhotosUrl.PhotoSource, value)
	}

	return result, nil
}

func (c *Client) FindAssessmentByStatus(ctx context.Context, statusCode, limit, page, cbid, roleCode, branchID, direct int) (domain.AssessmentPagination, error) {
	var (
		logMsg = "service.Assessment.FindAssessmentByStatus "
		url    = c.cfg.AssessmentService + findAssessmentByStatusEndPoint +
			"?cbid=" + strconv.Itoa(cbid) + "&direct=" + strconv.Itoa(direct) +
			"&roleCode=" + strconv.Itoa(roleCode) + "&branchID=" + strconv.Itoa(branchID) +
			"&status=" + strconv.Itoa(statusCode) +
			"&limit=" + strconv.Itoa(limit) + "&page=" + strconv.Itoa(page)

		response service.DBOResponse
		data     assessmentPagination
		result   domain.AssessmentPagination
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Get", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result.TotalPages = data.TotalPages
	result.StatusName = data.StatusName

	for _, val := range data.Assessments {
		aData := domain.Assessment{
			ID:        val.ID,
			CBID:      val.CBID,
			AID:       val.AID,
			PID:       val.PID,
			GID:       val.GID,
			IIBInfoID: val.IIBInfoID,
			Status:    val.Status,

			GovUz: domain.GovUz{
				OwnerFullname:      val.OwnerFullName,
				VehiclePlateNumber: val.VehiclePlateNumber,
				VehicleKuzov:       val.VehicleKuzov,
			},
			CreatedAt: val.CreateAt,
			TransportView: domain.TransportView{
				TransportName: val.TransportName,
			},
			CardID: val.CardID,
		}
		for _, photo := range val.PhotoUrl.PhotoUrl {
			aPhoto := domain.PhotoSource{
				Title:    photo.Title,
				Order:    photo.Order,
				PhotoUrl: photo.PhotoUrl,
				LAT:      photo.LAT,
				LON:      photo.LON,
			}
			aData.AllPhotosUrl.PhotoSource = append(aData.AllPhotosUrl.PhotoSource, aPhoto)
		}
		result.Assessments = append(result.Assessments, aData)
	}

	return result, nil
}

func (c *Client) CountStatusAssessment(ctx context.Context, cbid, branchID, roleCode, direct int) ([]domain.AssessmentStatus, error) {
	var (
		logMsg = "service.Assessment.CountStatusAssessment "
		url    = c.cfg.AssessmentService + countAllStatusEndPoint +
			"?cbid=" + strconv.Itoa(cbid) + "&branchID=" + strconv.Itoa(branchID) +
			"&roleCode=" + strconv.Itoa(roleCode) + "&direct=" + strconv.Itoa(direct)
		response service.DBOResponse
		data     []assessmentStatus
		result   []domain.AssessmentStatus
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Get", logger.Error(err))
		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data {
		value := domain.AssessmentStatus{
			StatusCode:  val.StatusCode,
			StatusName:  val.StatusName,
			StatusCount: val.Count,
		}
		result = append(result, value)
	}

	return result, nil
}

func (c *Client) ChangeAssessmentStatus(ctx context.Context, aStatus domain.AssessmentStatus) (domain.Result, error) {
	var (
		logMsg   = "service.Assessment.ChangeAssessmentStatus "
		url      = c.cfg.AssessmentService + changeAssessmentStatus
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := changeStatus{
		AssessmentID: aStatus.ID,
		StatusCode:   aStatus.StatusCode,
		Comment:      aStatus.Comment,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))
		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) AssessmentAnalogsList(ctx context.Context, id string) ([]domain.GetAnalog, error) {
	var (
		logMsg   = "service.Assessment.AssessmentAnalogsList "
		url      = c.cfg.AssessmentService + assessmentAnalogsList + "?assessmentID=" + id
		response service.DBOResponse
		data     []analogList
		result   []domain.GetAnalog
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+" http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" result.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data {
		aData := domain.GetAnalog{
			ID:            val.ID,
			Model:         val.Model,
			Name:          val.Name,
			Position:      val.Position,
			Speedometer:   val.Speedometer,
			Year:          val.Year,
			Fuel:          val.Fuel,
			SellPrice:     val.SellPrice,
			Price:         val.Price,
			Condition:     val.Validity,
			SourceFile:    val.SourceFile,
			SourceFileUrl: val.SourceFileUrl,
		}
		result = append(result, aData)
	}

	return result, err
}

func (c *Client) AssessmentData(ctx context.Context, cbid, texPassSeria, texPassNum string) (domain.GovInfoWithCBID, error) {
	var (
		logMsg = "service.Assessment.AssessmentData "
		url    = c.cfg.AssessmentService + assessmentDataEndPoint +
			"?cbid=" + cbid + "&tex_passport_seria=" + texPassSeria + "&tex_passport_number=" + texPassNum
		response service.DBOResponse
		data     assessmentDataWithGovInfo
		result   domain.GovInfoWithCBID
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+" http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" result.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.GovInfoWithCBID{
		AssessmentID: data.AssessmentID,
		CBID:         data.CBID,
		Transport: domain.Transport{
			Model:       data.Transport.TransportModel,
			Name:        data.Transport.TransportName,
			Position:    data.Transport.TransportPosition,
			Year:        data.Transport.TransportYear,
			Speedometer: data.Transport.TransportSpeedometer,
			Condition:   data.Transport.TransportCondition,
			Fuel:        data.Transport.TransportFuel,
			Price:       data.Transport.TransportPrice,
			SellPrice:   data.Transport.TransportSellPrice,
		},
		GovUzStore: domain.GovUzStore{
			OwnerPinfl:               data.GovUzData.OwnerPinfl,
			OwnerFullname:            data.GovUzData.OwnerFullname,
			OwnerType:                data.GovUzData.OwnerType,
			OwnerDateBirth:           data.GovUzData.OwnerDateBirth,
			VehicleID:                data.GovUzData.VehicleID,
			VehiclePlateNumber:       data.GovUzData.VehiclePlateNumber,
			VehicleModel:             data.GovUzData.VehicleModel,
			VehicleColor:             data.GovUzData.VehicleColor,
			VehicleRegistrationDate:  data.GovUzData.VehicleRegistrationDate,
			IibPermitInfo:            data.GovUzData.IibPermitInfo,
			Division:                 data.GovUzData.Division,
			GarovPermitInfo:          data.GovUzData.GarovPermitInfo,
			VehicleMadeYear:          data.GovUzData.VehicleMadeYear,
			VehicleType:              data.GovUzData.VehicleType,
			VehicleKuzov:             data.GovUzData.VehicleKuzov,
			VehicleFullWeight:        data.GovUzData.VehicleFullWeight,
			VehicleEmptyWeight:       data.GovUzData.VehicleEmptyWeight,
			VehicleMotor:             data.GovUzData.VehicleMotor,
			VehicleFuelType:          data.GovUzData.VehicleFuelType,
			VehicleSeats:             data.GovUzData.VehicleSeats,
			VehicleStands:            data.GovUzData.VehicleStands,
			Inspection:               data.GovUzData.Inspection,
			VehicleTexpassportSeria:  data.GovUzData.VehicleTexpassportSeria,
			VehicleTexpassportNumber: data.GovUzData.VehicleTexpassportNumber,
			BodyTypeName:             data.GovUzData.BodyTypeName,
			VehicleShassi:            data.GovUzData.VehicleShassi,
			VehiclePower:             data.GovUzData.VehiclePower,
			DateSchetSpravka:         data.GovUzData.DateSchetSpravka,
			TuningPermit:             data.GovUzData.TuningPermit,
			TuningGivenDate:          data.GovUzData.TuningGivenDate,
			TuningIssueDate:          data.GovUzData.TuningIssueDate,
			PrevPnfl:                 data.GovUzData.PrevPnfl,
			PrevOwner:                data.GovUzData.PrevOwner,
			PrevOwnerType:            data.GovUzData.PrevOwnerType,
			PrevPlateNumber:          data.GovUzData.PrevPlateNumber,
			PrevTexpasportSery:       data.GovUzData.PrevTexpasportSery,
			PrevTexpasportNumber:     data.GovUzData.PrevTexpasportNumber,
			State:                    data.GovUzData.State,
		},
	}
	return result, err
}

func (c *Client) UpdateGovData(ctx context.Context, assessmentID string, info domain.GovUzStore) (domain.Result, error) {
	var (
		logMsg   = "service.GovUz.UpdateGovData "
		url      = c.cfg.AssessmentService + updateGovDataEndPoint + "?assessment_id=" + assessmentID
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	req := govUzResponse{
		OwnerFullname:      info.OwnerFullname,
		OwnerPinfl:         info.OwnerPinfl,
		VehicleMadeYear:    info.VehicleMadeYear,
		VehiclePlateNumber: info.VehiclePlateNumber,
		VehicleModel:       info.VehicleModel,
		VehicleColor:       info.VehicleColor,
		VehicleKuzov:       info.VehicleKuzov,
		VehicleFullWeight:  info.VehicleFullWeight,
		VehicleEmptyWeight: info.VehicleEmptyWeight,
		VehicleMotor:       info.VehicleMotor,
		PassportNumber:     info.VehicleTexpassportNumber,
		PassportSeria:      info.VehicleTexpassportSeria,
		VehiclePower:       info.VehiclePower,
		VehicleShassi:      info.VehicleShassi,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))
		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

func (c *Client) DeleteAssessment(ctx context.Context, id string) (domain.Result, error) {
	var (
		logMsg   = "service.Assessment.DeleteAssessment "
		url      = c.cfg.AssessmentService + deleteAssessmentEndPoint + "?id=" + id
		response service.DBOResponse
		data     result
		result   domain.Result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodDelete, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}
