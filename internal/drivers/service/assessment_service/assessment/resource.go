package assessment

type OfficialAssessment struct {
	ID string `json:"id"` // baxolash id
}

type idResponse struct {
	ID string `json:"id"`
}

type assessmentStore struct {
	CBID        int    `json:"cbid"`
	EXTCBID     int    `json:"ext_cbid"`
	Direct      int    `json:"direct"`
	BranchID    int    `json:"branch_id"`
	PID         string `json:"p_id"`
	Type        string `json:"type"`
	Model       string `json:"model"`
	Name        string `json:"name"`
	Position    string `json:"position"`
	Year        int    `json:"year" `
	Speedometer int    `json:"speedometer"`
	Validity    int    `json:"validity"`
	Fuel        string `json:"fuel"`
	ExtraParam  string `json:"extra_param"`
}

type allPhotoSource struct {
	PhotoSource []photoData `json:"photo_source"`
}

type photoData struct {
	Title    string `json:"title"`
	Order    int    `json:"order"`
	PhotoUrl string `json:"photo_url"`
	LAT      string `json:"lat"`
	LON      string `json:"lon"`
}

type techNumberParams struct {
	AssessmentID   string `json:"assessment_id"`
	PassportNumber string `json:"passport_number"`
	PassportSeria  string `json:"passport_seria"`
	PlateNumber    string `json:"plate_number"`
}

type govUzResponse struct {
	VehicleKuzov       string `json:"vehicle_kuzov"`
	OwnerPinfl         string `json:"owner_pinfl"`
	OwnerFullname      string `json:"owner_fullname"`
	VehicleModel       string `json:"vehicle_model"`
	VehicleColor       string `json:"vehicle_color"`
	VehicleMadeYear    int    `json:"vehicle_made_year"`
	VehicleMotor       string `json:"vehicle_motor"`
	VehicleFullWeight  int    `json:"vehicle_full_weight"`
	VehicleEmptyWeight int    `json:"vehicle_empty_weight"`
	VehiclePlateNumber string `json:"vehicle_plate_number"`
	PassportNumber     string `json:"passport_number"`
	PassportSeria      string `json:"passport_seria"`
	VinNumber          string `json:"vin_number"`
	VehiclePower       int    `json:"vehicle_power"`
	Seats              int    `json:"seats"`
	VehicleShassi      string `json:"vehicle_shassi"`
}

type fastAssessment struct {
	Type        string `json:"type"`
	Model       string `json:"model"`
	Name        string `json:"name"`
	Year        int    `json:"year"`
	Speedometer int    `json:"speedometer"`
	Validity    int    `json:"validity"`
	Position    string `json:"position"`
	Fuel        string `json:"fuel"`
	NameKey     string `json:"name_key"`
}

type assessmentResult struct {
	MarketPrice       int    `json:"market_price"`
	CreditPrice       int    `json:"credit_price"`
	TransportModel    string `json:"transport_model"`
	TransportName     string `json:"transport_name"`
	TransportPosition string `json:"transport_position"`
	TransportYear     int    `json:"transport_year"`
}

type result struct {
	Result bool `json:"result"`
}

type IdResponse struct {
	ID string `json:"id"`
}

type assessmentResponse struct {
	ID                 string      `json:"id"`
	CBID               int         `json:"cbid"`
	PID                string      `json:"p_id"`
	IIBInfoID          string      `json:"iib_info_id"`
	Status             int         `json:"status"`
	VehicleKuzov       string      `json:"vehicle_kuzov"`
	OwnerPinfl         string      `json:"owner_pinfl"`
	OwnerFullname      string      `json:"owner_fullname"`
	VehicleModel       string      `json:"vehicle_model"`
	VehicleColor       string      `json:"vehicle_color"`
	VehicleMadeYear    int         `json:"vehicle_year"`
	VehicleMotor       string      `json:"vehicle_motor"`
	VehicleFullWeight  int         `json:"vehicle_full_weight"`
	VehicleEmptyWeight int         `json:"vehicle_empty_weight"`
	VehiclePlateNumber string      `json:"vehicle_plate_number"`
	VehiclePower       int         `json:"vehicle_power"`
	PassportNumber     string      `json:"passport_number"`
	PassportSeria      string      `json:"passport_seria"`
	VinNumber          string      `json:"vin_number"`
	VehicleShassi      string      `json:"vehicle_shassi"`
	Type               string      `json:"type"`
	TypeKey            string      `json:"type_key"`
	Model              string      `json:"model"`
	Name               string      `json:"name"`
	NameKey            string      `json:"name_key"`
	Position           string      `json:"position"`
	Year               int         `json:"year"`
	Speedometer        int         `json:"speedometer"`
	Validity           int         `json:"validity"`
	Fuel               string      `json:"fuel"`
	Price              int         `json:"price"`
	SellPrice          int         `json:"sell_price"`
	MarketPrice        int         `json:"market_price"`
	CreditPrice        int         `json:"credit_price"`
	TransportModel     string      `json:"transport_model"`
	TransportName      string      `json:"transport_name"`
	TransportPosition  string      `json:"transport_position"`
	TransportYear      int         `json:"transport_year"`
	Photos             allPhotoUrl `json:"photos"`
	Url                string      `json:"url"`
	ExtraParam         string      `json:"extra_param"`
	IIBPermitInfo      string      `json:"iib_permit_info"`
}

type allPhotoUrl struct {
	PhotoUrl []photoData `json:"photo"`
}

type assessmentPagination struct {
	Assessments []assessment `json:"assessments"`
	TotalPages  int          `json:"total_pages"`
	StatusName  string       `json:"status_name"`
}

type assessment struct {
	ID                 string      `json:"id"`
	CBID               int         `json:"cbid"`
	AID                string      `json:"a_id"`
	PID                string      `json:"p_id"`
	GID                string      `json:"g_id"`
	IIBInfoID          string      `json:"iib_info_id"`
	Status             int         `json:"status"`
	OwnerFullName      string      `json:"owner_fullname"`
	VehiclePlateNumber string      `json:"vehicle_plate_number"`
	VehicleKuzov       string      `json:"vehicle_kuzov"`
	PhotoUrl           allPhotoUrl `json:"photos"`
	CreateAt           string      `json:"created_at"`
	TransportName      string      `json:"transport_name"`
	CardID             string      `json:"card_id"`
}

type assessmentStatus struct {
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
	Count      int    `json:"count"`
}

type changeStatus struct {
	AssessmentID string `json:"assessment_id"`
	StatusCode   int    `json:"status_code"`
	Comment      string `json:"comment"`
}

type analogList struct {
	ID            string `json:"id"`
	Model         string `json:"model"`
	Name          string `json:"name"`
	Position      string `json:"position"`
	Speedometer   int    `json:"speedometer"`
	Year          int    `json:"year"`
	Fuel          string `json:"fuel"`
	SellPrice     int    `json:"sell_price"`
	Price         int    `json:"price"`
	Validity      string `json:"validity"`
	SourceFileUrl string `json:"source_file_url"`
	SourceFile    string `json:"source_file"`
}

type appTransportInfo struct {
	TransportModel       string `json:"transport_model"`
	TransportName        string `json:"transport_name"`
	TransportPosition    string `json:"transport_position"`
	TransportYear        int    `json:"transport_year"`
	TransportSpeedometer int    `json:"transport_speedometer"`
	TransportCondition   string `json:"transport_condition"`
	TransportFuel        string `json:"transport_fuel"`
	TransportPrice       int    `json:"market_price"`
	TransportSellPrice   int    `json:"credit_price"`
}

type govUzData struct {
	OwnerPinfl               string `json:"owner_pinfl"`
	OwnerFullname            string `json:"owner_fullname"`
	OwnerType                int    `json:"owner_type"`
	OwnerDateBirth           string `json:"owner_date_birth"`
	VehicleID                int    `json:"vehicle_id"`
	VehiclePlateNumber       string `json:"vehicle_plate_number"`
	VehicleModel             string `json:"vehicle_model"`
	VehicleColor             string `json:"vehicle_color"`
	VehicleRegistrationDate  string `json:"vehicle_registration_date"`
	IibPermitInfo            string `json:"iib_permit_info"`
	Division                 string `json:"division"`
	GarovPermitInfo          string `json:"garov_permit_info"`
	VehicleMadeYear          int    `json:"vehicle_made_year"`
	VehicleType              int    `json:"vehicle_type"`
	VehicleKuzov             string `json:"vehicle_kuzov"`
	VehicleFullWeight        int    `json:"vehicle_full_weight"`
	VehicleEmptyWeight       int    `json:"vehicle_empty_weight"`
	VehicleMotor             string `json:"vehicle_motor"`
	VehicleFuelType          int    `json:"vehicle_fuel_type"`
	VehicleSeats             int    `json:"vehicle_seats"`
	VehicleStands            int    `json:"vehicle_stands"`
	Inspection               string `json:"inspection"`
	VehicleTexpassportSeria  string `json:"vehicle_texpassport_seria"`
	VehicleTexpassportNumber string `json:"vehicle_texpassport_number"`
	BodyTypeName             string `json:"body_type_name"`
	VehicleShassi            string `json:"vehicle_shassi"`
	VehiclePower             int    `json:"vehicle_power"`
	DateSchetSpravka         string `json:"date_schet_spravka"`
	TuningPermit             string `json:"tuning_permit"`
	TuningGivenDate          string `json:"tuning_given_date"`
	TuningIssueDate          string `json:"tuning_issue_date"`
	PrevPnfl                 string `json:"prev_pnfl"`
	PrevOwner                string `json:"prev_owner"`
	PrevOwnerType            string `json:"prev_owner_type"`
	PrevPlateNumber          string `json:"prev_plate_number"`
	PrevTexpasportSery       string `json:"prev_texpasport_seria"`
	PrevTexpasportNumber     string `json:"prev_texpasport_number"`
	State                    string `json:"state"`
}

type assessmentDataWithGovInfo struct {
	AssessmentID string           `json:"assessment_id"`
	CBID         int              `json:"cbid"`
	Transport    appTransportInfo `json:"transport_info"`
	GovUzData    govUzData        `json:"gov_uz_data"`
}
