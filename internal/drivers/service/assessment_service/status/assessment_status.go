package status

import (
	"bytes"
	"context"
	"encoding/json"
	"gateway_service/internal/config"
	"gateway_service/internal/domain"
	"gateway_service/internal/errs"
	service "gateway_service/internal/pkg/http"
	"gateway_service/internal/pkg/logger"
	"gateway_service/internal/pkg/status"
	"io"
	"net/http"
)

const (
	statusStoreEndPoint     = "/status/store"
	findAllAssessmentStatus = "/status/find/all"
)

type Client struct {
	cfg    config.Config
	client *http.Client
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		cfg:    cfg,
		client: &http.Client{},
		log:    log,
	}
}

func (c *Client) StatusStore(ctx context.Context, aStatus domain.AssessmentStatus) (domain.IdResponse, error) {
	var (
		logMsg   = "service.Assessment_Status.StatusStore "
		url      = c.cfg.AssessmentService + statusStoreEndPoint
		response service.DBOResponse
		data     IdResponse
		result   domain.IdResponse
	)

	req := statusStore{
		StatusName: aStatus.StatusName,
		StatusCode: aStatus.StatusCode,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.IdResponse{
		ID: data.ID,
	}

	return result, nil
}

func (c *Client) FindAllStatus(ctx context.Context) ([]domain.AssessmentStatus, error) {
	var (
		logMsg   = "service.Assessment_Status.FindAllStatus "
		url      = c.cfg.AssessmentService + findAllAssessmentStatus
		response service.DBOResponse
		data     []statusResponse
		result   []domain.AssessmentStatus
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Get", logger.Error(err))
		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data {
		aStatus := domain.AssessmentStatus{
			ID:          val.ID,
			StatusCode:  val.StatusCode,
			StatusName:  val.StatusName,
			StatusCount: val.Count,
			CreatedAt:   val.CreatedAt,
		}
		result = append(result, aStatus)
	}

	return result, nil
}
