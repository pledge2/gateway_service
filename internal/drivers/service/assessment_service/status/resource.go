package status

type statusStore struct {
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
}

type statusResponse struct {
	ID         string `json:"id"`
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
	Count      int    `json:"count"`
	CreatedAt  string `json:"created_at"`
}

type IdResponse struct {
	ID string `json:"id"`
}
