package report

import (
	"context"
	"gateway_service/internal/config"
	"gateway_service/internal/errs"
	"gateway_service/internal/pkg/logger"
	"io"
	"net/http"
)

const assessmentReport = "/generate/pdf"

type Client struct {
	client *http.Client
	cfg    config.Config
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		client: &http.Client{},
		cfg:    cfg,
		log:    log,
	}
}

func (c *Client) AssessmentReport(ctx context.Context, assessmentID string) ([]byte, error) {
	var (
		logMsg = "service.Assessment.AssessmentReport "
		url    = c.cfg.ReportService + assessmentReport + "?assessmentID=" + assessmentID
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return nil, errs.ErrInternal
	}

	resp, err := c.client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return nil, errs.ErrInternal
	}
	defer resp.Body.Close()

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return nil, errs.ErrInternal
	}

	if resp.StatusCode != http.StatusOK {
		c.log.Error(logMsg+"unexpected status code", logger.Int("status_code", resp.StatusCode))
		c.log.Info(logMsg, logger.String("Response", string(res)))

		return nil, errs.ErrInternal
	}

	return res, nil
}
